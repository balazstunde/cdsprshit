1. You must have Java JDK (https://www.oracle.com/technetwork/java/javase/downloads/index.html) and add it to the PATH
2. Download STS from https://spring.io/tools3/sts/all
3. Download Android Studio from https://developer.android.com/studio/ (with SDK not just the IDE)
4. Download MySQL from https://dev.mysql.com/downloads/
5. Download MongoDB from https://www.mongodb.com/download-center
6. Make a user for the project: username root and password admin123 then run the localhost.sql and bicycle.sql in MySQL
7. Add the images and bicycleimages folder in the root (C:/)
8. Run the MongoDB server (MongoDB/server/bin/mongod.exe or from command line: mongod (if exists in PATH))
9. Open STS from the downloaded folder (sts-bundle/sts-3.9.5.RELEASE/STS.exe)
10. Import merx folder (check the merx-server and merx-simo importer and merx bicycle importer) to STS as gradle project
11. Right click on merx-server and Configure than click on Add gradle nature
12. RIght click on merx-server click on Gradle and Refresh Project
13. Right click on merx-server and Run as Spring Boot Project
14. The same with the importers
15. Import merx-android to Android Studio and run it on your Android Phone or emulator



->Branching strategy:
	- We will make a new branch from develop, and you will make new branches from our branch and send merge requests to it.
	- After we aprove it will go to Gulesz or Karesz to aprove it again and to be marged in develop.
	- Naming convension feature/[Your name's initials (like BT)]_#[number of task]
	- It's done something when Gulesz or Karesz merged into develope
	- Commit messages starts with the number of task then a short description about you made