package edu.codespring.merx.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Brand;
import edu.codespring.merx.model.Car;
import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.model.Type;
import edu.codespring.merx.repository.CarRespository;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

	@Mock
	private CarRespository carRespository;
	
	@InjectMocks
	private CarService carService;
	
	@Test
	public void testGetCar() {
		Type type = new Type(1, "Dacia", "dacia");
		EmissionClass emissionClass = new EmissionClass(1, "EURO 3");
		Brand brand = new Brand(1, "Logan", "logan");
		Car car = new Car(1, "purple", 12000, 120000, "", 5, type, brand, emissionClass, "fs", "gfdgr", "sdgr", "reg", "gd", "chgtf", "loip", "kurek");
		when(carRespository.findById(1)).thenReturn(Optional.of(car));
		
		Car car2 = carService.getCar(1);
		assertThat(car2, samePropertyValuesAs(car));
	}
}
