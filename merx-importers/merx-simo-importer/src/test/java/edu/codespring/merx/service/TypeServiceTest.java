package edu.codespring.merx.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Type;
import edu.codespring.merx.repository.TypeRepository;

@RunWith(MockitoJUnitRunner.class)
public class TypeServiceTest {

	@Mock
	private TypeRepository typeRepository;
	
	@InjectMocks
	private TypeService typeService;
	
	@Test
	public void testGetType() {
		Type type = new Type(1, "Dacia", "dacia");
		when(typeRepository.findById(1)).thenReturn(Optional.of(type));
		
		Type type2 = typeService.getType(1);
		assertThat(type2, samePropertyValuesAs(type));
	}
}
