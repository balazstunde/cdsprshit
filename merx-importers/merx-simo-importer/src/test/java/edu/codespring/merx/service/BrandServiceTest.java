package edu.codespring.merx.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Brand;
import edu.codespring.merx.repository.BrandRepository;

@RunWith(MockitoJUnitRunner.class)
public class BrandServiceTest {

	@Mock
	private BrandRepository brandRepository;
	
	@InjectMocks
	private BrandService brandService;
	
	@Test
	public void testGetBrand() {
		Brand brand = new Brand(1, "Logan", "logan");
		when(brandRepository.findById(1)).thenReturn(Optional.of(brand));
		
		Brand brand2 = brandService.getBrand(1);
		assertThat(brand2, samePropertyValuesAs(brand));
	}
}
