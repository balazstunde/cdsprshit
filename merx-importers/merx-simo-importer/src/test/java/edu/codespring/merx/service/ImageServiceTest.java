package edu.codespring.merx.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Image;
import edu.codespring.merx.repository.ImageRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceTest {
	
	@Mock
	private ImageRepository imageRepository;
	
	@InjectMocks
	private ImageService imageService;
	
	@Test
	public void testGetImage() {
		Image image = new Image(1, "images/1", 2);
		when(imageRepository.findById(1)).thenReturn(Optional.of(image));
		
		Image image2 = imageService.getImage(1);
		assertThat(image2, samePropertyValuesAs(image));
	}

}
