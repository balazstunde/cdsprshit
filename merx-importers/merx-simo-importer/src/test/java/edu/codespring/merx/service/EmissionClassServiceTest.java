package edu.codespring.merx.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.repository.EmissionClassRepository;

@RunWith(MockitoJUnitRunner.class)
public class EmissionClassServiceTest {

	@Mock
	private EmissionClassRepository emissionClassRepository;
	
	@InjectMocks
	private EmissionClassService emissionClassService;
	
	@Test
	public void testGetEmissionClass() {
		EmissionClass emissionClass = new EmissionClass(1, "EURO 3");
		when(emissionClassRepository.findById(1)).thenReturn(Optional.of(emissionClass));
		
		EmissionClass emissionClass2 = emissionClassService.getEmissionClass(1);
		assertThat(emissionClass, samePropertyValuesAs(emissionClass2));
	}
}
