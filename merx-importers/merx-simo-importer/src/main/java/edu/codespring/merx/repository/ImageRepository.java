package edu.codespring.merx.repository;

import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Image;

@Repository
public interface ImageRepository extends ReadOnlyRepository<Image, Integer>{

}
