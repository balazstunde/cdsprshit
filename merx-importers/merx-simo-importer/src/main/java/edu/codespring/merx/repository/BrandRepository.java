package edu.codespring.merx.repository;

import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Brand;

@Repository
public interface BrandRepository extends ReadOnlyRepository<Brand, Integer>{

}
