package edu.codespring.merx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Type;
import edu.codespring.merx.repository.TypeRepository;

@Service
public class TypeService {

	@Autowired
	private TypeRepository typeRepository;
	
	public Type getType(Integer id) {
		return typeRepository.findById(id).get();
	}
}
