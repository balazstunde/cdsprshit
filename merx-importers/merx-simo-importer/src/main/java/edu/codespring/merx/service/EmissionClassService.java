package edu.codespring.merx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.repository.EmissionClassRepository;

@Service
public class EmissionClassService {
	
	@Autowired
	private EmissionClassRepository emissionClassRepository;
	
	public EmissionClass getEmissionClass(Integer id) {
		return emissionClassRepository.findById(id).get();
	}

}
