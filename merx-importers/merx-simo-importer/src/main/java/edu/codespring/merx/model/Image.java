package edu.codespring.merx.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "kepek")
@Immutable
public class Image {

	@Id
	private Integer id;
	
	@Column(name = "file")
	private String file;
	
	@Column(name = "auto_id")
	private Integer carId;
	
	public Image() {
		
	}

	public Image(Integer id, String file, Integer carId) {
		super();
		this.id = id;
		this.file = file;
		this.carId = carId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Integer getCarId() {
		return carId;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}
	
}
