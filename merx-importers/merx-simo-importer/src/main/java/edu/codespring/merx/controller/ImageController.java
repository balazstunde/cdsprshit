package edu.codespring.merx.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;


@Controller
public class ImageController {
	
	@RequestMapping(value="{product}/{productId}/images/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getImage(@PathVariable String product, @PathVariable Integer productId, @PathVariable String imageId) {
		RestTemplate restTemplate = new RestTemplate();
		
	    ResponseEntity<byte[]> responseEntity = restTemplate.getForEntity("http://vo2.skytransauto.eu/public/autokepek/" + imageId, byte[].class);
	    return responseEntity;
	}
	
}
