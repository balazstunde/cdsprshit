package edu.codespring.merx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Image;
import edu.codespring.merx.repository.ImageRepository;

@Service
public class ImageService {
	
	@Autowired
	private ImageRepository imageRepository;
	
	public Image getImage(Integer id) {
		return imageRepository.findById(id).get();
	}
}
