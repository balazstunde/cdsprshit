package edu.codespring.merx.repository;

import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Car;

@Repository
public interface CarRespository extends ReadOnlyRepository<Car, Integer>{

}
