package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Car;
import edu.codespring.merx.repository.CarRespository;

@Service
public class CarService {
	
	@Autowired
	private CarRespository carRespository;
	
	public Car getCar(Integer id) {
		return carRespository.findById(id).get();
	}
	
	public List<Car> getAllCars() {
		List<Car> cars = new ArrayList<Car>();
		carRespository.findAll().forEach(cars::add);
		
		return cars;
	}

}
