package edu.codespring.merx.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "euro_szabvany")
@Immutable
public class EmissionClass {
	
	@Id
	private int id;
	
	@Column(name = "megnevezes")
	private String name;
	
	public EmissionClass() {
		
	}

	public EmissionClass(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
