package edu.codespring.merx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Brand;
import edu.codespring.merx.repository.BrandRepository;

@Service
public class BrandService {

	@Autowired
	private BrandRepository brandRepository;
	
	public Brand getBrand(Integer id) {
		return brandRepository.findById(id).get();
	}
}
