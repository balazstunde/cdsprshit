package edu.codespring.merx.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@Entity
@Table(name = "auto")
@Immutable
public class Car {
	@Id
	private Integer id;
	
	@Column(name = "szin")
	private String color;
	
	@Column(name = "net_ar")
	private Integer price;
	
	@Column(name = "km")
	private Integer kilometers;
	
	@Column(name = "leiras")
	private String description;
	
	@Column(name = "ulohely")
	private Integer numberOfSeats;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="auto_id")
	private List<Image> images;
	
	@ManyToOne
	@JoinColumn(name = "tipus_id")
	private Type type;
	
	@ManyToOne
	@JoinColumn(name = "marka_id")
	private Brand brand;
	
	@ManyToOne
	@JoinColumn(name = "euro_szabvany_id")
	private EmissionClass emissionClass;
	
	@Column(name = "felszereltseg")
	private String facilities;
	
	@Column(name = "motorizacio")
	private String motorization;
	
	@Column(name = "evjarat")
	private String vintage;
	
	@Column(name = "akku")
	private String battery;
	
	@Column(name = "potkerek")
	private String spareWheel;
	
	@Column(name = "gumimeret")
	private String wheelSize;
	
	@Column(name = "karosszeria")
	private String body;
	
	@Column(name = "megjegyzes")
	private String remark;
	
	public Car() {
		
	}

	public Car(Integer id, String color, Integer price, Integer kilometers, String description, Integer numberOfSeats,
			 Type type, Brand brand, EmissionClass emissionClass, String facilities,
			String motorization, String vintage, String battery, String spareWheel, String wheelSize, String body,
			String remark) {
		super();
		this.id = id;
		this.color = color;
		this.price = price;
		this.kilometers = kilometers;
		this.description = description;
		this.numberOfSeats = numberOfSeats;
		this.type = type;
		this.brand = brand;
		this.emissionClass = emissionClass;
		this.facilities = facilities;
		this.motorization = motorization;
		this.vintage = vintage;
		this.battery = battery;
		this.spareWheel = spareWheel;
		this.wheelSize = wheelSize;
		this.body = body;
		this.remark = remark;
	}



	public EmissionClass getEmissionClass() {
		return emissionClass;
	}

	public void setEmissionClass(EmissionClass emissionClass) {
		this.emissionClass = emissionClass;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getKilometers() {
		return kilometers;
	}

	public void setKilometers(Integer kilometers) {
		this.kilometers = kilometers;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Integer numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getFacilities() {
		return facilities;
	}

	public void setFacilities(String facilities) {
		this.facilities = facilities;
	}

	public String getMotorization() {
		return motorization;
	}

	public void setMotorization(String motorization) {
		this.motorization = motorization;
	}

	public String getVintage() {
		return vintage;
	}

	public void setVintage(String vintage) {
		this.vintage = vintage;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

	public String getSpareWheel() {
		return spareWheel;
	}

	public void setSpareWheel(String spareWheel) {
		this.spareWheel = spareWheel;
	}

	public String getWheelSize() {
		return wheelSize;
	}

	public void setWheelSize(String wheelSize) {
		this.wheelSize = wheelSize;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
