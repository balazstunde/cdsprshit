package edu.codespring.merx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.codespring.merx.model.Car;
//import edu.codespring.merx.model.Auto;
import edu.codespring.merx.model.Brand;
import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.model.Image;
import edu.codespring.merx.model.Type;
//import edu.codespring.merx.service.CarService;
import edu.codespring.merx.service.BrandService;
import edu.codespring.merx.service.CarService;
import edu.codespring.merx.service.EmissionClassService;
import edu.codespring.merx.service.ImageService;
import edu.codespring.merx.service.TypeService;

@Controller
public class CarController {

	@Autowired
	private CarService carService;
	
	@Autowired
	private BrandService brandService;
	
	@Autowired
	private EmissionClassService emissionClassService;
	
	@Autowired
	private TypeService typeService;
	
	@Autowired
	private ImageService imageService;
	
	@RequestMapping(method = RequestMethod.GET, value = "car/{id}")
	@ResponseBody
    public Car getCar(@PathVariable Integer id) {
		Car car = null;
		try {
			car = carService.getCar(id);
		}catch(DataAccessException ex) {
			System.out.println("CarService error.");
			System.out.println(ex.toString());
		}
		return car;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@ResponseBody
	public String hello() {
		return "hello";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "brand/{id}")
	@ResponseBody
    public Brand getBrand(@PathVariable Integer id) {
		Brand brand = null;
		try {
			brand = brandService.getBrand(id);
		}catch(DataAccessException ex) {
			System.out.println("MarkaSevrvice error.");
			System.out.println(ex.toString());
		}
		return brand;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "emissionclass/{id}")
	@ResponseBody
    public EmissionClass getEmissionClass(@PathVariable Integer id) {
		EmissionClass emissionClass = null;
		try {
			emissionClass = emissionClassService.getEmissionClass(id);
		}catch(DataAccessException ex) {
			System.out.println("MarkaSevrvice error.");
			System.out.println(ex.toString());
		}
		return emissionClass;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "type/{id}")
	@ResponseBody
    public Type getType(@PathVariable Integer id) {
		Type type = null;
		try {
			type = typeService.getType(id);
		}catch(DataAccessException ex) {
			System.out.println("MarkaSevrvice error.");
			System.out.println(ex.toString());
		}
		return type;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "image/{id}")
	@ResponseBody
    public Image getImage(@PathVariable Integer id) {
		Image image = null;
		try {
			image = imageService.getImage(id);
		}catch(DataAccessException ex) {
			System.out.println("MarkaSevrvice error.");
			System.out.println(ex.toString());
		}
		return image;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/car")
	@ResponseBody
	public List<Car> getAllTopics() {
		List<Car> cars = null;
		try {
			cars = carService.getAllCars();
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		return cars;
	}
}
