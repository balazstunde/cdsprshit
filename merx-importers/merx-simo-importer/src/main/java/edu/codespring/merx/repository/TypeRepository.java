package edu.codespring.merx.repository;

import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Type;

@Repository
public interface TypeRepository extends ReadOnlyRepository<Type, Integer>{

}
