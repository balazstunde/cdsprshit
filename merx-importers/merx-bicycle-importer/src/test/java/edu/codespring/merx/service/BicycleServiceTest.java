package edu.codespring.merx.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Bicycle;
import edu.codespring.merx.model.Model;
import edu.codespring.merx.repository.BicycleRepository;

@RunWith(MockitoJUnitRunner.class)
public class BicycleServiceTest {
	
	@Mock
	private BicycleRepository bicycleRepository;
	
	@InjectMocks
	private BicycleService bicycleService;
	
	@Test
	public void testGetBicycle() {
		Model model = new Model(1, "BMX");
		Bicycle bicycle = new Bicycle(1, 123f, "DHS", model, 10f, "haskjfhksjd", "safhkjdgfk", 1.2f, "red", "20");
		when(bicycleRepository.findById(1)).thenReturn(Optional.of(new Bicycle(1, 123f, "DHS", model, 10f, "haskjfhksjd", "safhkjdgfk", 1.2f, "red", "20")));
		Bicycle bicycle2 = bicycleService.getBicycle(1);
		assertThat(bicycle2, samePropertyValuesAs(bicycle));
	}
	
}
