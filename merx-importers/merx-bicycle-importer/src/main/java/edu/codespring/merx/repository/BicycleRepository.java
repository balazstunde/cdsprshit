package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Bicycle;

@Repository
public interface BicycleRepository extends CrudRepository<Bicycle, Integer>{

}
