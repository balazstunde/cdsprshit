package edu.codespring.merx.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.codespring.merx.model.Bicycle;
import edu.codespring.merx.service.BicycleService;

@Controller
public class BicycleController {
	@Autowired
	BicycleService bicycleService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/bicycle/{id}")
	@ResponseBody
    public Bicycle getBicycle(@PathVariable Integer id) {
		Bicycle bicycle = null;
		try {
			bicycle = bicycleService.getBicycle(id);
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		return bicycle;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/bicycle")
	@ResponseBody
    public List<Bicycle> getBicycles() {
		List<Bicycle> bicycles = null;
		try {
			bicycles = bicycleService.getAllBicycles();
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		return bicycles;
    }
	
	@RequestMapping(value="/bicycle/{bicycleId}/bicycleimages/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getImage(@PathVariable Integer bicycleId, @PathVariable String imageId) {
		File serverFile = new File("C:/bicycleimages/" + bicycleId + "/" + imageId);
	    byte[] bytes = null;
		try {
			bytes = Files.readAllBytes(serverFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
	}
}
