package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Bicycle;
import edu.codespring.merx.repository.BicycleRepository;

@Service
public class BicycleService {

	@Autowired
	private BicycleRepository bicycleRepository;
	
	public Bicycle getBicycle(int id) throws DataAccessException{
		return bicycleRepository.findById(id).get();
		
	}
	
	public List<Bicycle> getAllBicycles() throws DataAccessException {
		List<Bicycle> bicycles = new ArrayList<>();
		bicycleRepository.findAll().forEach(bicycles::add);
		
		return bicycles;
	}
}
