package edu.codespring.merx.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "images")
public class Image {
	
	@Id
	private Integer id;

	private Integer bicycleId;
	
	@Column(name="image_path")
	private String path;
	
	public Image() {
		
	}

	public Image(Integer id, Integer bicycleId, String path) {
		super();
		this.id = id;
		this.bicycleId = bicycleId;
		this.path = path;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBicycleId() {
		return bicycleId;
	}

	public void setBicycleId(Integer bicycleId) {
		this.bicycleId = bicycleId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
}
