package edu.codespring.merx.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.codespring.merx.model.Image;

@JsonInclude(Include.NON_NULL)
@Entity(name = "bicycles")
public class Bicycle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "price")
	private Float price;
	
	@Column(name = "bicycle_name")
	private String bicycleName;
	
	@ManyToOne
	private Model model;
	
	@Column(name = "weight")
	private Float weight;
	
	@Column(name = "fork")
	private String fork;
	
	@Column(name = "rotors")
	private String rotors;
	
	@Column(name = "rating")
	private Float rating;
	
	@Column(name = "color")
	private String color;
	
	@Column(name = "wheel_size")
	private String wheelSize;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="bicycleId")
	private List<Image> images;
	
	public Bicycle() {
		
	}

	public Bicycle(Integer id, Float price, String bicycleName, Model model, Float weight, String fork, String rotors,
			Float rating, String color, String wheelSize) {
		super();
		this.id = id;
		this.price = price;
		this.bicycleName = bicycleName;
		this.model = model;
		this.weight = weight;
		this.fork = fork;
		this.rotors = rotors;
		this.rating = rating;
		this.color = color;
		this.wheelSize = wheelSize;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getBicycleName() {
		return bicycleName;
	}

	public void setBicycleName(String bicycleName) {
		this.bicycleName = bicycleName;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public String getFork() {
		return fork;
	}

	public void setFork(String fork) {
		this.fork = fork;
	}

	public String getRotors() {
		return rotors;
	}

	public void setRotors(String rotors) {
		this.rotors = rotors;
	}

	public Float getRating() {
		return rating;
	}

	public void setRating(Float rating) {
		this.rating = rating;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getWheelSize() {
		return wheelSize;
	}

	public void setWheelSize(String wheelSize) {
		this.wheelSize = wheelSize;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
	
}
