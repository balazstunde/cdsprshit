package edu.codespring.merx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={"edu.codespring.merx"})
public class BicycleApiDataApplication {
	public static void main(String[] args) {
		
		SpringApplication.run(BicycleApiDataApplication.class, args);
	}
}