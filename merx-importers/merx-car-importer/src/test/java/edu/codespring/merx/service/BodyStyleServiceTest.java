package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.BodyStyle;
import edu.codespring.merx.repository.BodyStyleRepository;

@RunWith(MockitoJUnitRunner.class)
public class BodyStyleServiceTest {
	
	@Mock
	private BodyStyleRepository bodyStyleRepository;
	
	@InjectMocks
	private BodyStyleService bodyStyleService;
	
	private List<BodyStyle> bodyStyles;
	
	@Before
    public void setBodyStyles() {
		bodyStyles = new ArrayList<>();
		bodyStyles.add(new BodyStyle(1, "Bogar"));
		bodyStyles.add(new BodyStyle(2, "Kabrio"));
    }
	
	@Test
	public void testGetBodyStyle() {
		when(bodyStyleRepository.findById(1)).thenReturn(Optional.of(bodyStyles.get(0)));
		assertEquals(bodyStyleService.getBodyStyle(1), bodyStyles.get(0));
	}
	
	@Test
	public void testAddBodyStyle() {
		BodyStyle bodyStyle = new BodyStyle(3, "Kombi");
		when(bodyStyleRepository.save(bodyStyle)).thenAnswer(invocation -> {
			bodyStyles.add(bodyStyle);
		     return bodyStyle;
		});
		
		int db = bodyStyles.size();
		bodyStyleService.addBodyStyle(bodyStyle);
		
        assertTrue(bodyStyles.contains(bodyStyle));
        assertEquals(db + 1, bodyStyles.size());
	}
	
	@Test
	public void testGetAllBodyStyles() {
		when(bodyStyleRepository.findAll()).thenReturn(bodyStyles);
		
		List<BodyStyle> bodyStyles1 = bodyStyleService.getAllBodyStyles();
		assertArrayEquals(bodyStyles.toArray(), bodyStyles1.toArray());
		
	}

}
