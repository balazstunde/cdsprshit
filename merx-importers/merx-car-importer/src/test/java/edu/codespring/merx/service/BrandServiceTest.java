package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Brand;
import edu.codespring.merx.repository.BrandRepository;

@RunWith(MockitoJUnitRunner.class)
public class BrandServiceTest {

	@Mock 
	private BrandRepository brandRepository;
	
	@InjectMocks
	private BrandService brandService;
	
	private List<Brand> brands;
	
	@Before
    public void setBrands() {
		brands = new ArrayList<>();
		brands.add(new Brand(1, "Bentley"));
		brands.add(new Brand(2, "Opel"));
    }
	
	@Test
	public void testGetBrand() {
		when(brandRepository.findById(1)).thenReturn(Optional.of(brands.get(0)));
		assertEquals(brandService.getBrand(1), brands.get(0));
	}
	
	@Test
	public void testAddBrand() {
		Brand brand = new Brand(3, "WolksVagen");
		when(brandRepository.save(brand)).thenAnswer(invocation -> {
			brands.add(brand);
		     return brand;
		});
		
		int db = brands.size();
		brandService.addBrand(brand);
		
        assertTrue(brands.contains(brand));
        assertEquals(db + 1, brands.size());
	}
	
	@Test
	public void testGetAllBrands() {
		when(brandRepository.findAll()).thenReturn(brands);
		
		List<Brand> brands1 = brandService.getAllBrands();
		assertArrayEquals(brands.toArray(), brands1.toArray());
		
	}
}
