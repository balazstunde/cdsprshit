package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Engine;
import edu.codespring.merx.repository.EngineRepository;

@RunWith(MockitoJUnitRunner.class)
public class EngineServiceTest {

	@Mock
	private EngineRepository engineRepository;
	
	@InjectMocks
	private EngineService engineService;
	
	private List<Engine> engines;
	
	@Before
    public void setEngines() {
		engines = new ArrayList<>();
		engines.add(new Engine(1, "abc", 150, 1700));
		engines.add(new Engine(2, "def", 120, 1600));
    }
	
	@Test
	public void testGetEngine() {
		when(engineRepository.findById(1)).thenReturn(Optional.of(engines.get(0)));
		assertEquals(engineService.getEngine(1), engines.get(0));
	}
	
	@Test
	public void testAddEngine() {
		Engine engine = new Engine(3, "ghi", 160, 2000);
		when(engineRepository.save(engine)).thenAnswer(invocation -> {
			engines.add(engine);
		     return engine;
		});
		
		int db = engines.size();
		engineService.addEngine(engine);
		
        assertTrue(engines.contains(engine));
        assertEquals(db + 1, engines.size());
	}
	
	@Test
	public void testGetAllEngines() {
		when(engineRepository.findAll()).thenReturn(engines);
		
		List<Engine> engines1 = engineService.getAllEngins();
		assertArrayEquals(engines.toArray(), engines1.toArray());
		
	}
}
