package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.repository.EmissionClassRepository;

@RunWith(MockitoJUnitRunner.class)
public class EmissionClassServiceTest {

	@Mock
	private EmissionClassRepository emissionClassRepository;
	
	@InjectMocks
	private EmissionClassService emissionClassService;
	
private List<EmissionClass> emissionClasses;
	
	@Before
    public void setEmissionClasses() {
		emissionClasses = new ArrayList<>();
		emissionClasses.add(new EmissionClass(1, "EURO 4"));
		emissionClasses.add(new EmissionClass(2, "EURO 5"));
    }
	
	@Test
	public void testGetEmissionClass() {
		when(emissionClassRepository.findById(1)).thenReturn(Optional.of(emissionClasses.get(0)));
		assertEquals(emissionClassService.getEmissionClass(1), emissionClasses.get(0));
	}
	
	@Test
	public void testAddEmissionClass() {
		EmissionClass emissionClass = new EmissionClass(3, "EURO 6");
		when(emissionClassRepository.save(emissionClass)).thenAnswer(invocation -> {
			emissionClasses.add(emissionClass);
		     return emissionClass;
		});
		
		int db = emissionClasses.size();
		emissionClassService.addEmissionClass(emissionClass);
		
        assertTrue(emissionClasses.contains(emissionClass));
        assertEquals(db + 1, emissionClasses.size());
	}
	
	@Test
	public void testGetAllEmissionClases() {
		when(emissionClassRepository.findAll()).thenReturn(emissionClasses);
		
		List<EmissionClass> emissionClasses1 = emissionClassService.getAllEmissionClasses();
		assertArrayEquals(emissionClasses.toArray(), emissionClasses1.toArray());
		
	}
}
