package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Color;
import edu.codespring.merx.repository.ColorRepository;

@RunWith(MockitoJUnitRunner.class)
public class ColorServiceTest {
	
	@Mock
	private ColorRepository colorRepository;
	
	@InjectMocks
	private ColorService colorService;
	
	private List<Color> colors;
	
	@Before
    public void setColors() {
		colors = new ArrayList<>();
		colors.add(new Color(1, "black"));
		colors.add(new Color(2, "red"));
    }
	
	@Test
	public void testGetColor() {
		when(colorRepository.findById(1)).thenReturn(Optional.of(colors.get(0)));
		assertEquals(colorService.getColor(1), colors.get(0));
	}
	
	@Test
	public void testAddColor() {
		Color color = new Color(3, "purple");
		when(colorRepository.save(color)).thenAnswer(invocation -> {
			colors.add(color);
		     return color;
		});
		
		int db = colors.size();
		colorService.addColor(color);
		
        assertTrue(colors.contains(color));
        assertEquals(db + 1, colors.size());
	}
	
	@Test
	public void testGetAllColors() {
		when(colorRepository.findAll()).thenReturn(colors);
		
		List<Color> colors1 = colorService.getAllColors();
		assertArrayEquals(colors.toArray(), colors1.toArray());
		
	}
}
