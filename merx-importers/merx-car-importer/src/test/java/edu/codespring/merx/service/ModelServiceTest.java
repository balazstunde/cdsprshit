package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Brand;
import edu.codespring.merx.model.Color;
import edu.codespring.merx.model.Model;
import edu.codespring.merx.repository.ModelRepository;

@RunWith(MockitoJUnitRunner.class)
public class ModelServiceTest {

	@Mock
	private ModelRepository modelRepository;
	
	@InjectMocks
	private ModelService modelService;
	
	private List<Model> models;
	
	@Before
    public void setModels() {
		models = new ArrayList<>();
		Brand brand = new Brand(1, "Opel");
		models.add(new Model(1, "Zafira", brand));
		models.add(new Model(2, "Astra", brand));
    }
	
	@Test
	public void testGetModel() {
		when(modelRepository.findById(1)).thenReturn(Optional.of(models.get(0)));
		assertEquals(modelService.getModel(1), models.get(0));
	}
	
	@Test
	public void testAddModel() {
		Brand brand = new Brand(2, "VolksWagen");
		Model model = new Model(3, "Passat", brand);
		when(modelRepository.save(model)).thenAnswer(invocation -> {
			models.add(model);
		     return model;
		});
		
		int db = models.size();
		modelService.addModel(model);
		
        assertTrue(models.contains(model));
        assertEquals(db + 1, models.size());
	}
	
	@Test
	public void testGetAllModels() {
		when(modelRepository.findAll()).thenReturn(models);
		
		List<Model> models1 = modelService.getAllModels();
		assertArrayEquals(models.toArray(), models1.toArray());
		
	}
}
