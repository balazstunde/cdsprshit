package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.Image;
import edu.codespring.merx.model.ImageIdentity;
import edu.codespring.merx.repository.ImageRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceTest {

	@Mock
	private ImageRepository imageRepository;
	
	@InjectMocks
	private ImageService imageService;
	
	private List<Image> images;
	
	@Before
    public void setImages() {
		images = new ArrayList<>();
		images.add(new Image(new ImageIdentity("2.jpg", 1), "images/1/2.jpg"));
		images.add(new Image(new ImageIdentity("3.jpg", 1), "images/1/3.jpg"));
    }
	
	@Test
	public void testGetImage() {
		ImageIdentity imageIdentity = new ImageIdentity("2.jpg", 1);
		when(imageRepository.findById(imageIdentity)).thenReturn(Optional.of(images.get(0)));
		assertEquals(imageService.getImage(imageIdentity), images.get(0));
	}
	
	@Test
	public void testAddImage() {
		ImageIdentity imageIdentity = new ImageIdentity("1.jpg",1);
		Image image = new Image(imageIdentity, "images/1/1.jpg");
		when(imageRepository.save(image)).thenAnswer(invocation -> {
			images.add(image);
		     return image;
		});
		
		int db = images.size();
		imageService.addImage(image);
		
        assertTrue(images.contains(image));
        assertEquals(db + 1, images.size());
	}
	
	@Test
	public void testGetAllImages() {
		when(imageRepository.findAll()).thenReturn(images);
		
		List<Image> images1 = imageService.getAllImages();
		assertArrayEquals(images.toArray(), images1.toArray());
		
	}
}
