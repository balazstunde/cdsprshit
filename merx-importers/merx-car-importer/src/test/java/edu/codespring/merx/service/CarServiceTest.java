package edu.codespring.merx.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.model.BodyStyle;
import edu.codespring.merx.model.Brand;
import edu.codespring.merx.model.Car;
import edu.codespring.merx.model.Color;
import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.model.Engine;
import edu.codespring.merx.model.Model;
import edu.codespring.merx.repository.CarRepository;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {
	
	@Mock
	private CarRepository carRepository;
	
	@InjectMocks
	private  CarService carService;
	
	private List<Car> cars;
	
	@Before
    public void setCars() {
		cars = new ArrayList<>();
		Engine engine = new Engine(1, "fogalmam sincs", 1560, 1978);
		EmissionClass emissionClass = new EmissionClass(1, "EURO 5");
		BodyStyle bodyStyle = new BodyStyle(1, "Kombi");
		Brand brand = new Brand(3, "VolksWagen");
		Model model = new Model(1, "Passat 2013", brand);
		Color color = new Color(1, "purple");
		cars.add(new Car(1, color, engine, emissionClass, bodyStyle, model, 5, 10000, 123.5f, 1203, true, true, 5.6f, 7.0f, 4.8f, " sfa ", "ABS"));
		cars.add(new Car(2, color, engine, emissionClass, bodyStyle, model, 2, 10000, 123.5f, 1203, true, true, 5.6f, 7.0f, 4.8f, " sfa ", "nothing"));
    }
	
	@Test
	public void testGetCar() {
		when(carRepository.findById(1)).thenReturn(Optional.of(cars.get(0)));
		assertEquals(carService.getCar(1), cars.get(0));
	}
	
	@Test
	public void testAddCar() {
		Engine engine = new Engine(1, "fogalmam sincs", 1560, 1978);
		EmissionClass emissionClass = new EmissionClass(1, "EURO 5");
		BodyStyle bodyStyle = new BodyStyle(1, "Kombi");
		Brand brand = new Brand(3, "Opel");
		Model model = new Model(1, "Zafira", brand);
		Color color = new Color(1, "green");
		Car car = new Car(3, color, engine, emissionClass, bodyStyle, model, 5, 10000, 123.5f, 1203, true, true, 5.6f, 7.0f, 4.8f, " sfa ", "ABS");
		when(carRepository.save(car)).thenAnswer(invocation -> {
			cars.add(car);
		     return car;
		});
		
		int db = cars.size();
		carService.addCar(car);
		
        assertTrue(cars.contains(car));
        assertEquals(db + 1, cars.size());
	}
	
	@Test
	public void testGetAllCars() {
		when(carRepository.findAll()).thenReturn(cars);
		
		List<Car> cars1 = carService.getAllCars();
		assertArrayEquals(cars.toArray(), cars1.toArray());
		
	}
}
