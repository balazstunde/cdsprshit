<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h3>${brand} ${model} </h3>
	
	<table>
	
		<tr>
			<td>Price: </td>
			<td>${price}</td>
		</tr>
	
		<tr>
			<td>Color: </td>
			<td>${color}</td>
		</tr>
		
		<tr>
			<td>Engine name: </td>
			<td>${engine_name}</td>
		</tr>
		
		<tr>
			<td>Engine power: </td>
			<td>${engine_power}</td>
		</tr>
		
		<tr>
			<td>Cubic capacity: </td>
			<td>${cubic_capacity}</td>
		</tr>
		
		<tr>
			<td>CO2 emission: </td>
			<td>${co2_emission}</td>
		</tr>
		
		<tr>
			<td>Mileage: </td>
			<td>${mileage}</td>
		</tr>
		
		<tr>
			<td>Automatic transmission: </td>
			<td>${automatic_transmission}</td>
		</tr>
		
		<tr>
			<td>Emission Class: </td>
			<td>${emission_class}</td>
		</tr>
		
		<tr>
			<td>Air Conditioning: </td>
			<td>${air_conditioning}</td>
		</tr>
		
		<tr>
			<td>Fuel Consumption (City): </td>
			<td>${fuel_consumption_city}</td>
		</tr>
		
		<tr>
			<td>Fuel Consumption (Highway)): </td>
			<td>${fuel_consumption_highway}</td>
		</tr>
		
		<tr>
			<td>Fuel Consumption (Combined): </td>
			<td>${fuel_consumption_combined}</td>
		</tr>
		
		<tr>
			<td>Body Style: </td>
			<td>${body_style}</td>
		</tr>
		
		<tr>
			<td>Number of doors: </td>
			<td>${number_of_doors}</td>
		</tr>
		
		<tr>
			<td>Description: </td>
			<td>${description}</td>
		</tr>
		
		<tr>
			<td>Features: </td>
			<td>${features}</td>
		</tr>
		
	</table>
</body>
</html>