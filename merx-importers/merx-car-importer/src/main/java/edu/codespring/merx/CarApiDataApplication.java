package edu.codespring.merx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages={"edu.codespring.merx"})
public class CarApiDataApplication extends SpringBootServletInitializer{
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CarApiDataApplication.class);
    }
	
	public static void main(String[] args) {
		
		SpringApplication.run(CarApiDataApplication.class, args);
	}
}
