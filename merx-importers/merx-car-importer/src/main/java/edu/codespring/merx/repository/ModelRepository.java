package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Model;

@Repository
public interface ModelRepository extends CrudRepository<Model, Integer>{
	
}
