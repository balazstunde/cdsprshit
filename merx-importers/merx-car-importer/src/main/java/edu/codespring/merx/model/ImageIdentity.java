package edu.codespring.merx.model;

import java.io.Serializable;

public class ImageIdentity implements Serializable{
	
	private String imageId;
	private Integer carId;
	
	public ImageIdentity() {
		
	}

	public ImageIdentity(String imageId, Integer car) {
		super();
		this.imageId = imageId;
		this.carId = car;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public Integer getCar() {
		return carId;
	}

	public void setCar(Integer carId) {
		this.carId = carId;
	}
	
	@Override
	public boolean equals(Object o) {
		 if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        ImageIdentity that = (ImageIdentity) o;

	        if (!imageId.equals(that.imageId)) return false;
	        return carId.equals(that.carId);
	}
	
	@Override
    public int hashCode() {
        int result = imageId.hashCode();
        result = 31 * result + carId.hashCode();
        return result;
    }
	
}
