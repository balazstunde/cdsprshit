package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Color;
import edu.codespring.merx.repository.ColorRepository;

@Service
public class ColorService{

	@Autowired
	private ColorRepository colorRepository;
	
	public Color getColor(int id) throws DataAccessException {
		return colorRepository.findById(id).get();
	}
	
	public void addColor(Color color) throws DataAccessException {
		colorRepository.save(color);
	}
	
	public List<Color> getAllColors() throws DataAccessException {
		List<Color> colors = new ArrayList<>();
		colorRepository.findAll().forEach(colors::add);;
		return colors;
	}
	
}
