package edu.codespring.merx.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmissionClass {
	
	@Id
	private int id;
	private String name;
	
	public EmissionClass(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public EmissionClass() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
