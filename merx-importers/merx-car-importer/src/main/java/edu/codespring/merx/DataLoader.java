package edu.codespring.merx;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import edu.codespring.merx.model.BodyStyle;
import edu.codespring.merx.model.Brand;
import edu.codespring.merx.model.Car;
import edu.codespring.merx.model.Color;
import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.model.Engine;
import edu.codespring.merx.model.Image;
import edu.codespring.merx.model.ImageIdentity;
import edu.codespring.merx.model.Model;
import edu.codespring.merx.service.BodyStyleService;
import edu.codespring.merx.service.BrandService;
import edu.codespring.merx.service.CarService;
import edu.codespring.merx.service.ColorService;
import edu.codespring.merx.service.EmissionClassService;
import edu.codespring.merx.service.EngineService;
import edu.codespring.merx.service.ImageService;
import edu.codespring.merx.service.ModelService;

@Component
public class DataLoader implements ApplicationRunner {
	
	private CarService carService;
	private ColorService colorService;
	private EngineService engineService;
	private EmissionClassService emissionClassService;
	private BodyStyleService bodyStyleService;
	private ModelService modelService;
	private BrandService brandService;
	private ImageService imageService;

 	@Autowired
    public DataLoader(CarService carService, ColorService colorService, EngineService engineService, EmissionClassService emissionClassService, BodyStyleService bodyStyleService, ModelService modelService, BrandService brandService, ImageService imageService) {
      this.carService = carService;
      this.colorService = colorService;
      this.engineService = engineService;
      this.emissionClassService = emissionClassService;
      this.bodyStyleService = bodyStyleService;
      this.modelService = modelService;
      this.brandService = brandService;
      this.imageService = imageService;
    }

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		try{
			Color color = new Color(1, "#f12345");
			colorService.addColor(color);
			Color color1 = new Color(2, "#387eed");
			colorService.addColor(color1);
			Color color2 = new Color(3, "#18202d");
			colorService.addColor(color2);
			Color color3 = new Color(4, "#5e9166");
			colorService.addColor(color3);
			
			Engine engine = new Engine(1, "asdfghjkl", 221, 2993);
			engineService.addEngine(engine);
			Engine engine1 = new Engine(2, "asdfghjkl", 55, 1496);
			engineService.addEngine(engine1);
			Engine engine2 = new Engine(3, "asdfghjkl", 423, 5998);
			engineService.addEngine(engine2);
			Engine engine3 = new Engine(4, "asdfghjkl", 285, 2993);
			engineService.addEngine(engine3);
			
			EmissionClass emissionClass = new EmissionClass(1, "Euro 6");
			emissionClassService.addEmissionClass(emissionClass);
			EmissionClass emissionClass1 = new EmissionClass(2, "Euro 5");
			emissionClassService.addEmissionClass(emissionClass1);
			EmissionClass emissionClass2 = new EmissionClass(3, "Euro 4");
			emissionClassService.addEmissionClass(emissionClass2);
			EmissionClass emissionClass3 = new EmissionClass(4, "Euro 3");
			emissionClassService.addEmissionClass(emissionClass3);
			
			BodyStyle bodyStyle = new BodyStyle(1, "Sedan/Hatchback");
			bodyStyleService.addBodyStyle(bodyStyle);
			BodyStyle bodyStyle1 = new BodyStyle(2, "Small car/Other");
			bodyStyleService.addBodyStyle(bodyStyle1);
			BodyStyle bodyStyle2 = new BodyStyle(3, "Coupe/Sport");
			bodyStyleService.addBodyStyle(bodyStyle2);
			
			Brand brand = new Brand(1, "Jaguar");
			brandService.addBrand(brand);
			Brand brand1 = new Brand(2, "Mazda");
			brandService.addBrand(brand1);
			Brand brand2 = new Brand(3, "Bentley");
			brandService.addBrand(brand2);
			Brand brand3 = new Brand(4, "Tesla");
			brandService.addBrand(brand3);
			
			Model model = new Model(1, "XF 30d Aut. R-Sport", brand);
			modelService.addModel(model);
			Model model1 = new Model(2, "Mazda 2 (75 PS)", brand1);
			modelService.addModel(model1);
			Model model2 = new Model(3, "Continental GT , 1. Besitzer", brand2);
			modelService.addModel(model2);
			Model model3 = new Model(4, "Model S 85 Pano-Leder-Navi", brand3);
			modelService.addModel(model3);
			
			int numberOfDoors = 5;
			int numberOfDoors1 = 3;
			
			int price = 31084;
			int price1 = 11672;
			int price2 = 74379;
			int price3 = 47639;
			
			int co2Emission = 144;
			int co2Emission1 = 110;
			int co2Emission2 = 330;
			int co2Emission3 = 0;
			
			int mileage = 31281;
			int mileage1 = 8;
			int mileage2 = 59595;
			int mileage3 = 89500;
			
			boolean automaticTransmission = true;
			boolean automaticTransmission1 = false;
			
			boolean airConditioning = true;
			boolean airConditioning1 = false;
					
			float fuelConsumptionCombined = (float) 5.5;
			float fuelConsumptionCombined1 = (float) 4.7;
			float fuelConsumptionCombined2 = (float) 14.4;
			float fuelConsumptionCombined3 = (float) 0;
			
			float fuelConsumptionCity = (float) 6.6;
			float fuelConsumptionCity1 = (float) 6.1;
			float fuelConsumptionCity2 = (float) 22.1;
			float fuelConsumptionCity3 = (float) 0;
			
			float fuelConsumptionHighway = (float) 4.5;
			float fuelConsumptionHighway1 = (float) 3.9;
			float fuelConsumptionHighway2 = (float) 9.9;
			float fuelConsumptionHighway3 = (float) 0;
			
			String description = "nothing";
			String description1 = "";
			String description2 = "nothing";
			String description3 = "nothing";
			
			String features = "ABS:Front and side airbags,Xenon headlights:Navigation system,Heated front seats:ESP,Parking sensors:Central lock";
			String features1 = "ABS:Front and side airbags,Heated front seats:ESP,Parking sensors:Central lock,Electric windows:Cruise control";
			String features2 = "ABS:Xenon head,lights:Navigation system,Heated front seats:ESP,Parking sensors:Central lock,Electric windows:Sunroof,Cruise control:Electric mirrors,";
			String features3 = "ABS:Front side and other airbags,ESP:Parking sensors,Cruise control:Handsfree system,Daytime running lights:Parking distance control - rear,Rear parking camera:Adaptive cruise control";
			
			Image image = new Image(new ImageIdentity("1", 1), "images/1.jpg");
			imageService.addImage(image);
			
			Image image1 = new Image(new ImageIdentity("2", 1), "images/2.jpg");
			imageService.addImage(image1);
			
			Image image2 = new Image(new ImageIdentity("3", 1), "images/3.jpg");
			imageService.addImage(image2);
			
			List<Image> images = new ArrayList<>();
			images.add(image);
			images.add(image1);
			images.add(image2);
			
			
			Car car = new Car(1, color, engine, emissionClass, bodyStyle, model, numberOfDoors, price, co2Emission, mileage, automaticTransmission, airConditioning, fuelConsumptionCombined, fuelConsumptionCity, fuelConsumptionHighway,  description,  features);
			car.setImages(images);
			carService.addCar(car);
			
			
			Image image3 = new Image(new ImageIdentity("1", 2), "images/1.jpg");
			imageService.addImage(image3);
			
			Image image4 = new Image(new ImageIdentity("2", 2), "images/2.jpg");
			imageService.addImage(image4);
			
			Image image5 = new Image(new ImageIdentity("3", 2), "images/3.jpg");
			imageService.addImage(image5);
			
			List<Image> images1 = new ArrayList<>();
			images1.add(image3);
			images1.add(image4);
			images1.add(image5);
			
			Car car1 = new Car(2, color1, engine1, emissionClass, bodyStyle1, model1, numberOfDoors, price1, co2Emission1, mileage1, automaticTransmission, airConditioning, fuelConsumptionCombined1, fuelConsumptionCity1, fuelConsumptionHighway1,  description1,  features1);
			car1.setImages(images1);
			carService.addCar(car1);
			
			Image image6 = new Image(new ImageIdentity("1", 3), "images/1.jpg");
			imageService.addImage(image6);
			
			Image image7 = new Image(new ImageIdentity("2", 3), "images/2.jpg");
			imageService.addImage(image7);
			
			Image image8 = new Image(new ImageIdentity("3", 3), "images/3.jpg");
			imageService.addImage(image8);
			
			Image image12 = new Image(new ImageIdentity("4", 3), "images/4.jpg");
			imageService.addImage(image12);
			
			List<Image> images2 = new ArrayList<>();
			images2.add(image6);
			images2.add(image7);
			images2.add(image8);
			images2.add(image12);
			Car car2 = new Car(3, color2, engine2, emissionClass1, bodyStyle2, model2, numberOfDoors1, price2, co2Emission2, mileage2, automaticTransmission, airConditioning, fuelConsumptionCombined2, fuelConsumptionCity2, fuelConsumptionHighway2,  description2,  features2);
			car2.setImages(images2);
			carService.addCar(car2);
			
			Image image9 = new Image(new ImageIdentity("1", 4), "images/1.jpg");
			imageService.addImage(image9);
			
			Image image10 = new Image(new ImageIdentity("2", 4), "images/2.jpg");
			imageService.addImage(image10);
			
			Image image11 = new Image(new ImageIdentity("3", 4), "images/3.jpg");
			imageService.addImage(image11);
			
			List<Image> images3 = new ArrayList<>();
			images3.add(image9);
			images3.add(image10);
			images3.add(image11);
			
			Car car3 = new Car(4, color3, engine3, emissionClass, bodyStyle, model3, numberOfDoors, price3, co2Emission3, mileage3, automaticTransmission, airConditioning, fuelConsumptionCombined3, fuelConsumptionCity3, fuelConsumptionHighway3,  description3,  features3);
			car3.setImages(images3);
			carService.addCar(car3);
			
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		
	}
}