package edu.codespring.merx.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Engine {
	
	@Id
	private int id;
	private String name;
	private int power;
	private int cubicCapacity;
	
	public Engine() {
		
	}
	
	public Engine(int id, String name, int power, int cubicCapacity) {
		super();
		this.id = id;
		this.name = name;
		this.power = power;
		this.cubicCapacity = cubicCapacity;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getCubicCapacity() {
		return cubicCapacity;
	}
	public void setCubicCapacity(int cubicCapapcity) {
		this.cubicCapacity = cubicCapapcity;
	}
	
}
