
package edu.codespring.merx.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.codespring.merx.model.Car;
import edu.codespring.merx.service.CarService;

@Controller
public class CarController {
	
	@Autowired
	private CarService carService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/html/car/{id}")
    public String getCarHTML(Map<String, Object> model, @PathVariable int id, HttpServletResponse response, HttpServletRequest request) {
		Car car = null;
		try {
			car = carService.getCar(id);
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		model.put("brand", car.getModel().getBrand().getName());
		model.put("model", car.getModel().getName());
		model.put("price", car.getPrice());
		model.put("color", car.getColor().getColor());
		model.put("engine_name", car.getEngine().getName());
		model.put("engine_power", car.getEngine().getPower());
		model.put("cubic_capacity", car.getEngine().getCubicCapacity());
		model.put("co2_emission", car.getCo2Emission());
		model.put("mileage", car.getMileage());
		model.put("automatic_transmission", car.isAutomaticTransmission());
		model.put("emission_class", car.getEmissionClass().getName());
		model.put("air_conditioning", car.isAirConditioning());
		model.put("fuel_consumption_city", car.getFuelConsumptionCity());
		model.put("fuel_consumption_highway", car.getFuelConsumptionHighway());
		model.put("fuel_consumption_combined", car.getFuelConsumptionCombined());
		model.put("body_style", car.getBodyStyle().getName());
		model.put("number_of_doors", car.getNumberOfDoors());
		model.put("description", car.getDescription());
		model.put("features", car.getFeatures());
		return "car";
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/car/{id}")
	@ResponseBody
    public Car getCarJSON(Map<String, Object> model, @PathVariable Integer id, HttpServletResponse response, HttpServletRequest request) {
		Car car = null;
		try {
			car = carService.getCar(id);
		}catch(DataAccessException ex) {
			System.out.println("ENDOF");
			System.out.println(ex.toString());
		}
		return car;
    }
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/car")
	@ResponseBody
	public List<Car> getAllTopics() {
		List<Car> cars = null;
		try {
			cars = carService.getAllCars();
		}catch(DataAccessException ex) {
			System.out.println(ex.toString());
		}
		return cars;
	}
	
	@RequestMapping(value="/car/{carId}/images/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getImage(@PathVariable Integer carId, @PathVariable String imageId) {
		/*ImageIdentity imageIdentity = new ImageIdentity(imageId, carId);
		Image image = imageService.getImage(imageIdentity);*/
		File serverFile = new File("C:/images/" + carId + "/" + imageId);
	    byte[] bytes = null;
		try {
			bytes = Files.readAllBytes(serverFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	@ResponseBody
    public String getSomething() {
        return "Welcome!";
    }
}
