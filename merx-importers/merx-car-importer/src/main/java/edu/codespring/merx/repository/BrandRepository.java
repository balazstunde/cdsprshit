package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Brand;

@Repository
public interface BrandRepository extends CrudRepository<Brand, Integer>{
	
}