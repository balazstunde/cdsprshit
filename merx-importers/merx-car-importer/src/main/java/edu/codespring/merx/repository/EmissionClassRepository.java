package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.EmissionClass;

@Repository
public interface EmissionClassRepository extends CrudRepository<EmissionClass, Integer>{
	
}