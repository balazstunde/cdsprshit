package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Engine;
import edu.codespring.merx.repository.EngineRepository;

@Service
public class EngineService {

	@Autowired
	private EngineRepository engineRepository;
	
	public Engine getEngine(int id) throws DataAccessException {
		return engineRepository.findById(id).get();
	}
	
	public void addEngine(Engine engine) throws DataAccessException {
		engineRepository.save(engine);
	}
	
	public List<Engine> getAllEngins() throws DataAccessException {
		List<Engine> engins = new ArrayList<>();
		engineRepository.findAll().forEach(engins::add);;
		return engins;
	}
	
}
