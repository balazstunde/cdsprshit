package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Image;
import edu.codespring.merx.model.ImageIdentity;

@Repository
public interface ImageRepository extends CrudRepository<Image, ImageIdentity>{

}
