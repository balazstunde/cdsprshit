package edu.codespring.merx.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Car;
import edu.codespring.merx.repository.CarRepository;

@Service
public class CarService{

	@Autowired
	private CarRepository carRepository;
	
	public Car getCar(int id) throws DataAccessException{
		return carRepository.findById(id).get();
		
	}
	
	public void addCar(Car car) throws DataAccessException{
		carRepository.save(car);
		
	}
	
	public List<Car> getAllCars() throws DataAccessException{
		List<Car> cars = new ArrayList<>();
		carRepository.findAll().forEach(cars::add);
		
		return cars;
	}
	
}
