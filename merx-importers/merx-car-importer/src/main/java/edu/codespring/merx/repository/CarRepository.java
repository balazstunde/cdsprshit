package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Integer>{
	public Car findByModelId(int id);
}
