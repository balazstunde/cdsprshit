package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Color;

@Repository
public interface ColorRepository extends CrudRepository<Color, Integer>{
	
}