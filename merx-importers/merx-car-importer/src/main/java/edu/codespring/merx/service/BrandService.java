package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Brand;
import edu.codespring.merx.repository.BrandRepository;

@Service
public class BrandService {

	@Autowired
	private BrandRepository brandRepository;
	
	public Brand getBrand(int id) throws DataAccessException {
		return brandRepository.findById(id).get();
	}
	
	public void addBrand(Brand brand) throws DataAccessException {
		brandRepository.save(brand);
	}
	
	public List<Brand> getAllBrands() throws DataAccessException {
		List<Brand> brands = new ArrayList<>();
		brandRepository.findAll().forEach(brands::add);;
		return brands;
	}
}
