package edu.codespring.merx.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Image {
	
	@Id
	private ImageIdentity imageIdentity;
	private String path;
	
	public Image() {
		
	}
	
	public Image(ImageIdentity imageIdentity, String path) {
		super();
		this.imageIdentity = imageIdentity;
		this.path = path;
	}
	
	public ImageIdentity getImageIdentity() {
		return imageIdentity;
	}
	
	public void setImageIdentity(ImageIdentity imageIdentity) {
		this.imageIdentity = imageIdentity;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
}
