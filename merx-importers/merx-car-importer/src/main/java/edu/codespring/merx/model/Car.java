package edu.codespring.merx.model;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

@JsonInclude(Include.NON_NULL)
@Entity
public class Car {
	@Id
	private Integer id;
	@ManyToOne
	private Color color;
	@ManyToOne
	private Engine engine;
	@ManyToOne
	private EmissionClass emissionClass;
	@ManyToOne
	private BodyStyle bodyStyle;
	@ManyToOne
	private Model model;
	
	private Number numberOfDoors;
	private Number price;
	private Number co2Emission;
	private Number mileage;
	private boolean automaticTransmission;
	private boolean airConditioning;
	private Number fuelConsumptionCombined;
	private Number fuelConsumptionCity;
	private Number fuelConsumptionHighway;
	private String description;
	private String features;
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="carId")
	private List<Image> images;

	public Car(Integer id, Color color, Engine engine, EmissionClass emissionClass, BodyStyle bodyStyle, Model model,
			Number numberOfDoors, Number price, Number co2Emission, Number mileage, boolean automaticTransmission,
			boolean airConditioning, Number fuelConsumptionCombined, Number fuelConsumptionCity,
			Number fuelConsumptionHighway, String description, String features) {
		super();
		this.id = id;
		this.color = color;
		this.engine = engine;
		this.emissionClass = emissionClass;
		this.bodyStyle = bodyStyle;
		this.model = model;
		this.numberOfDoors = numberOfDoors;
		this.price = price;
		this.co2Emission = co2Emission;
		this.mileage = mileage;
		this.automaticTransmission = automaticTransmission;
		this.airConditioning = airConditioning;
		this.fuelConsumptionCombined = fuelConsumptionCombined;
		this.fuelConsumptionCity = fuelConsumptionCity;
		this.fuelConsumptionHighway = fuelConsumptionHighway;
		this.description = description;
		this.features = features;
	}

	public Car() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public EmissionClass getEmissionClass() {
		return emissionClass;
	}

	public void setEmissionClass(EmissionClass emissionClass) {
		this.emissionClass = emissionClass;
	}

	public BodyStyle getBodyStyle() {
		return bodyStyle;
	}

	public void setBodyStyle(BodyStyle bodyStyle) {
		this.bodyStyle = bodyStyle;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
	
	public Number getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	public Number getCo2Emission() {
		return co2Emission;
	}
	public void setCo2Emission(int co2Emission) {
		this.co2Emission = co2Emission;
	}
	
	public Number getMileage() {
		return mileage;
	}
	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
	
	public boolean isAutomaticTransmission() {
		return automaticTransmission;
	}
	public void setAutomaticTransmission(boolean automaticTransmission) {
		this.automaticTransmission = automaticTransmission;
	}
	
	public boolean isAirConditioning() {
		return airConditioning;
	}
	public void setAirConditioning(boolean airConditioning) {
		this.airConditioning = airConditioning;
	}
	
	public Number getNumberOfDoors() {
		return numberOfDoors;
	}
	public void setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public Number getFuelConsumptionCombined() {
		return fuelConsumptionCombined;
	}

	public void setFuelConsumptionCombined(float fuelConsumptionCombined) {
		this.fuelConsumptionCombined = fuelConsumptionCombined;
	}

	public Number getFuelConsumptionCity() {
		return fuelConsumptionCity;
	}

	public void setFuelConsumptionCity(float fuelConsumptionCity) {
		this.fuelConsumptionCity = fuelConsumptionCity;
	}

	public Number getFuelConsumptionHighway() {
		return fuelConsumptionHighway;
	}

	public void setFuelConsumptionHighway(float fuelConsumptionHighway) {
		this.fuelConsumptionHighway = fuelConsumptionHighway;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getFeatures() {
		return features;
	}
	public void setFeatures(String features) {
		this.features = features;
	}
	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
	
}
