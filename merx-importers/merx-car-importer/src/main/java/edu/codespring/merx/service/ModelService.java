package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Model;
import edu.codespring.merx.repository.ModelRepository;

@Service
public class ModelService {

	@Autowired
	private ModelRepository modelRepository;
	
	public Model getModel(int id) throws DataAccessException {
		return modelRepository.findById(id).get();
	}
	
	public void addModel(Model model) throws DataAccessException {
		modelRepository.save(model);
	}
	
	public List<Model> getAllModels() throws DataAccessException {
		List<Model> models = new ArrayList<>();
		modelRepository.findAll().forEach(models::add);;
		return models;
	}
	
}
