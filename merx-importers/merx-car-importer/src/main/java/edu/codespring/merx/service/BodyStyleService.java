package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.BodyStyle;
import edu.codespring.merx.repository.BodyStyleRepository;

@Service
public class BodyStyleService {

	@Autowired
	private BodyStyleRepository bodyStyleRepository;
	
	public BodyStyle getBodyStyle(int id) throws DataAccessException {
		return bodyStyleRepository.findById(id).get();
	}
	
	public void addBodyStyle(BodyStyle bodyStyle) throws DataAccessException {
		bodyStyleRepository.save(bodyStyle);
	}
	
	public List<BodyStyle> getAllBodyStyles() throws DataAccessException {
		List<BodyStyle> bodyStyles = new ArrayList<>();
		bodyStyleRepository.findAll().forEach(bodyStyles::add);;
		return bodyStyles;
	}
}
