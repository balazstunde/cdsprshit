package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.Image;
import edu.codespring.merx.model.ImageIdentity;
import edu.codespring.merx.repository.ImageRepository;

@Service
public class ImageService {
	
	@Autowired
	private ImageRepository imageRepository;

	public Image getImage(ImageIdentity id) throws DataAccessException{
		return imageRepository.findById(id).get();
	}
	
	public void addImage(Image image) throws DataAccessException{
		imageRepository.save(image);
		
	}
	
	public List<Image> getAllImages() throws DataAccessException{
		List<Image> images = new ArrayList<>();
		imageRepository.findAll().forEach(images::add);
		
		return images;
	}
	
}
