package edu.codespring.merx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.model.EmissionClass;
import edu.codespring.merx.repository.EmissionClassRepository;

@Service
public class EmissionClassService {
	
	@Autowired
	private EmissionClassRepository emissionClassRepository;
	
	public EmissionClass getEmissionClass(int id) throws DataAccessException {
		return emissionClassRepository.findById(id).get();
	}
	
	public void addEmissionClass(EmissionClass emissionClass) throws DataAccessException {
		emissionClassRepository.save(emissionClass);
	}
	
	public List<EmissionClass> getAllEmissionClasses() throws DataAccessException {
		List<EmissionClass> emissionClasses = new ArrayList<>();
		emissionClassRepository.findAll().forEach(emissionClasses::add);;
		return emissionClasses;
	}

}
