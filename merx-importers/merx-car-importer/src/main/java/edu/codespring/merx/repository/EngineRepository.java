package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.Engine;

@Repository
public interface EngineRepository extends CrudRepository<Engine, Integer>{
	
}
