package edu.codespring.merx.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.model.BodyStyle;

@Repository
public interface BodyStyleRepository extends CrudRepository<BodyStyle, Integer>{
	
}
