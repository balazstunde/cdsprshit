package edu.codespring.merx.server.backend.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplateTextField;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateTextFieldRepository;
import edu.codespring.merx.server.service.DisplayTemplateTextFieldService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateTextFieldServiceTest {
	
	@Mock
    private DisplayTemplateTextFieldRepository displayTemplateTextFieldRepository;
	
    @InjectMocks
    private DisplayTemplateTextFieldService displayTemplateTextFieldService;
    
    List<DisplayTemplateTextField> displayTemplateTextFields;
    
    @Before
    public void setDisplayTemplateTextFields() {
    	displayTemplateTextFields = new ArrayList<>();
    	displayTemplateTextFields.add(new DisplayTemplateTextField(2, new ProductTemplateElement(2, "color", "string", "blablabla", "color color"), new LinearLayoutParams("LinearLayout"), 3, "color", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"));
    	displayTemplateTextFields.add(new DisplayTemplateTextField(5, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "engine_name", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"));
    }
	
	@Test
	public void testGetDisplayTemplateTextField() {
		when(displayTemplateTextFieldRepository.findById(2)).thenReturn(Optional.of(displayTemplateTextFields.get(0)));
        
		assertEquals(displayTemplateTextFieldService.getDisplayTemplateTextField(2), displayTemplateTextFields.get(0));
	}
	
	@Test
	public void testAddDisplayTemplateTextField() {
		DisplayTemplateTextField displayTemplateTextField = new DisplayTemplateTextField(7, new ProductTemplateElement(7, "body_style", "string", "blablabla", "bodyStyle name"), new LinearLayoutParams("LinearLayout"), 3, "body_style", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000");
		when(displayTemplateTextFieldRepository.save(displayTemplateTextField)).thenAnswer(invocation -> {
			displayTemplateTextFields.add(displayTemplateTextField);
		     return displayTemplateTextField;
		});
		
		int db = displayTemplateTextFields.size();
		displayTemplateTextFieldService.addDisplayTemplateTextField(displayTemplateTextField);
		
        assertTrue(displayTemplateTextFields.contains(displayTemplateTextField));
        assertEquals(db + 1, displayTemplateTextFields.size());
	}
	
	@Test
	public void testGetAllDisplayTemplateTextFields() {
		when(displayTemplateTextFieldRepository.findAll()).thenReturn(displayTemplateTextFields);
		
		List<DisplayTemplateTextField> displayTemplateTextFields1 = displayTemplateTextFieldService.getAllDisplayTemplateTextFields();
		assertArrayEquals(displayTemplateTextFields.toArray(), displayTemplateTextFields1.toArray());
		
	}
}
