package edu.codespring.merx.server.backend.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplateWebDisplay;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateWebDisplayRepository;
import edu.codespring.merx.server.service.DisplayTemplateWebDisplayService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateWebDisplayTest {

	@Mock
    private DisplayTemplateWebDisplayRepository displayTemplateWebDisplayRepository;
	
    @InjectMocks
    private DisplayTemplateWebDisplayService displayTemplateWebDisplayService;
    
    List<DisplayTemplateWebDisplay> displayTemplateWebDisplays;
    
    @Before
    public void setDisplayTemplateTextFields() {
    	displayTemplateWebDisplays = new ArrayList<>();
    	displayTemplateWebDisplays.add(new DisplayTemplateWebDisplay(2, new ProductTemplateElement(2, "color", "string", "blablabla", "color color"), new LinearLayoutParams("LinearLayout"), 3, "color", "TextField", 12, "Arial", "#42acdd"));
    	displayTemplateWebDisplays.add(new DisplayTemplateWebDisplay(5, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "engine_name", "TextField", 12, "Arial", "#42acdd"));
    }
	
	@Test
	public void testGetDisplayTemplateWebDisplay() {
		when(displayTemplateWebDisplayRepository.findById(2)).thenReturn(Optional.of(displayTemplateWebDisplays.get(0)));
        
		assertEquals(displayTemplateWebDisplayService.getDisplayTemplateWebDisplay(2), displayTemplateWebDisplays.get(0));
	}
	
	@Test
	public void testAddDisplayTemplateWebDisplay() {
		DisplayTemplateWebDisplay displayTemplateWebDisplay = new DisplayTemplateWebDisplay(7, new ProductTemplateElement(7, "body_style", "string", "blablabla", "bodyStyle name"), new LinearLayoutParams("LinearLayout"), 3, "body_style", "TextField", 16, "Arial", "#42acdd");
		when(displayTemplateWebDisplayRepository.save(displayTemplateWebDisplay)).thenAnswer(invocation -> {
			displayTemplateWebDisplays.add(displayTemplateWebDisplay);
		     return displayTemplateWebDisplay;
		});
		
		int db = displayTemplateWebDisplays.size();
		displayTemplateWebDisplayService.addDisplayTemplateWebDisplay(displayTemplateWebDisplay);
		
        assertTrue(displayTemplateWebDisplays.contains(displayTemplateWebDisplay));
        assertEquals(db + 1, displayTemplateWebDisplays.size());
	}
	
	@Test
	public void testGetAllDisplayTemplateWebDisplays() {
		when(displayTemplateWebDisplayRepository.findAll()).thenReturn(displayTemplateWebDisplays);
		
		List<DisplayTemplateWebDisplay> displayTemplateWebDisplays1 = displayTemplateWebDisplayService.getAllDisplayTemplateWebDisplays();
		assertArrayEquals(displayTemplateWebDisplays.toArray(), displayTemplateWebDisplays1.toArray());
		
	}
}
