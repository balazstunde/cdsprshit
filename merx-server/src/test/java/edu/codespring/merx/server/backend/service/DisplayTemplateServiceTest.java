package edu.codespring.merx.server.backend.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplate;
import edu.codespring.merx.server.backend.model.DisplayTemplateBooleanField;
import edu.codespring.merx.server.backend.model.DisplayTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayTemplateFloatField;
import edu.codespring.merx.server.backend.model.DisplayTemplateImageList;
import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;
import edu.codespring.merx.server.backend.model.DisplayTemplateTextField;
import edu.codespring.merx.server.backend.model.GridLayout;
import edu.codespring.merx.server.backend.model.GridLayoutParams;
import edu.codespring.merx.server.backend.model.Layout;
import edu.codespring.merx.server.backend.model.LinearLayout;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateRepository;
import edu.codespring.merx.server.service.DisplayTemplateService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateServiceTest {
	
	@Mock
    private DisplayTemplateRepository displayTemplateRepository;
	
    @InjectMocks
    private DisplayTemplateService displayTemplateService;
    
    List<DisplayTemplate> displayTemplates;
    List<DisplayTemplateElement> elements;
    List<Layout> layouts;
    
    @Before
    public void setDisplayTemplates() {
    	displayTemplates = new ArrayList<>();
    	elements = Arrays.asList(
    			new DisplayTemplateFloatField(1, new ProductTemplateElement(1, "price", "number", "blablabla", "price"), new LinearLayoutParams("LinearLayout"), 3, "price", "FloatField", 16, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(2, new ProductTemplateElement(2, "color", "string", "blablabla", "color color"), new LinearLayoutParams("LinearLayout"), 3, "color", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateNumberField(3, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "engine_power", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateFloatField(4, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "cubic_capacity", "FloatField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(5, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "engine_name", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(6, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "emission_class", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(7, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "body_style", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(8, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "model_name", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(9, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "brand_name", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateNumberField(10, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "number_of_doors", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateFloatField(11, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "co2_emission", "FloatField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateNumberField(12, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "mileage", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateBooleanField(13, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "automatic_transmission", "BooleanField", 16, "Arial", "#42acdd"),
				new DisplayTemplateBooleanField(14, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "air_conditioning", "BooleanField", 16, "Arial", "#42acdd"),
				new DisplayTemplateFloatField(15, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "fuel_consumption_combined", "FloatField", 12, "Times New Roman", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateFloatField(16, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "fuel_consumption_city", "FloatField", 12, "Times New Roman", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateFloatField(17, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "fuel_consumption_highway", "FloatField", 12, "Times New Roman", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(18, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "description", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateTextField(19, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "features", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"),
				new DisplayTemplateImageList(20, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 2, "images", "ImageList", 200, 300, 16, "Arial", "#42acdd")
		);
    	
    	layouts = Arrays.asList(
				new GridLayout("GridLayout", 1, 0, new LinearLayoutParams("LinearLayout"), 2, 1),
				new LinearLayout("LinearLayout", 2, 1, new GridLayoutParams("GridLayout", 1, 1), "horizontal"),
				new LinearLayout("LinearLayout", 3, 1, new GridLayoutParams("GridLayout", 2, 1), "vertical")
		);
    	displayTemplates.add(new DisplayTemplate(1, "car", 1, elements, layouts));
    	displayTemplates.add(new DisplayTemplate(2, "bike", 1, elements, layouts));
    }
	
	@Test
	public void testGetDisplayTemplate() {
		DisplayTemplate DisplayTemplate1 = new DisplayTemplate(1, "car", 1, elements, layouts);
		when(displayTemplateRepository.findById(1)).thenReturn(Optional.of(displayTemplates.get(0)));
        
        DisplayTemplate DisplayTemplate = displayTemplateService.getDisplayTemplate(1);
		assertThat(DisplayTemplate, samePropertyValuesAs(DisplayTemplate1));
		assertEquals(DisplayTemplate, displayTemplates.get(0));
	}
	
	@Test
	public void testAddDisplayTemplate() {
		DisplayTemplate DisplayTemplate1 = new DisplayTemplate(3, "roller",1, elements, layouts);
		when(displayTemplateRepository.save(DisplayTemplate1)).thenAnswer(invocation -> {
			displayTemplates.add(DisplayTemplate1);
		     return DisplayTemplate1;
		});
		
		int db = displayTemplates.size();
		displayTemplateService.addDisplayTemplate(DisplayTemplate1);
		
        assertTrue(displayTemplates.contains(DisplayTemplate1));
        assertEquals(db + 1, displayTemplates.size());
	}
	
	@Test
	public void testGetAllDisplayTemplates() {
		when(displayTemplateRepository.findAll()).thenReturn(displayTemplates);
		
		List<DisplayTemplate> DisplayTemplates1 = displayTemplateService.getAllDisplayTemplates();
		assertArrayEquals(displayTemplates.toArray(), DisplayTemplates1.toArray());
		
	}
	
	@Test
	public void testGetByName() {
		when(displayTemplateRepository.findByName("car")).thenReturn(displayTemplates.get(0));
		assertEquals(displayTemplateService.getByName("car"), displayTemplates.get(0));
	}
	
	@Test
	public void testGetByVendorIdAndName() {
		DisplayTemplate DisplayTemplate1 = new DisplayTemplate(1, "car", 1, elements, layouts);
		when(displayTemplateRepository.findByVendorIdAndName(1, "car")).thenReturn(displayTemplates.get(0));
        
        DisplayTemplate DisplayTemplate = displayTemplateService.getByVendorIdAndName(1, "car");
		assertThat(DisplayTemplate, samePropertyValuesAs(DisplayTemplate1));
		assertEquals(DisplayTemplate, displayTemplates.get(0));
	}
}
