package edu.codespring.merx.server.backend.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplateImageField;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateImageFieldRepository;
import edu.codespring.merx.server.service.DisplayTemplateImageFieldService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateImageFieldServiceTest {
	@Mock
    private DisplayTemplateImageFieldRepository displayTemplateImageFieldRepository;
	
    @InjectMocks
    private DisplayTemplateImageFieldService displayTemplateImageFieldService;
    
    List<DisplayTemplateImageField> displayTemplateImageFields;
    
    @Before
    public void setDisplayTemplateImageFields() {
    	displayTemplateImageFields = new ArrayList<>();
    	displayTemplateImageFields.add(new DisplayTemplateImageField(2, new ProductTemplateElement(20, "images", "string", "blablabla", "images path"), new LinearLayoutParams("LinearLayout"), 3, "image", "ImageField", 200, 300, 16, "Arial", "#42acdd"));
    	displayTemplateImageFields.add(new DisplayTemplateImageField(3, new ProductTemplateElement(20, "images", "string", "blablabla", "images path"), new LinearLayoutParams("LinearLayout"), 3, "image", "ImageField", 300, 400, 16, "Arial", "#42acdd"));
    }
	
	@Test
	public void testGetDisplayTemplateImageField() {
		when(displayTemplateImageFieldRepository.findById(2)).thenReturn(Optional.of(displayTemplateImageFields.get(0)));
        
		assertEquals(displayTemplateImageFieldService.getDisplayTemplateImageField(2), displayTemplateImageFields.get(0));
	}
	
	@Test
	public void testAddDisplayTemplateImageField() {
		DisplayTemplateImageField displayTemplateImageField = new DisplayTemplateImageField(4, new ProductTemplateElement(20, "images", "string", "blablabla", "images path"), new LinearLayoutParams("LinearLayout"), 3, "image", "ImageField", 300, 400, 16, "Arial", "#42acdd");
		when(displayTemplateImageFieldRepository.save(displayTemplateImageField)).thenAnswer(invocation -> {
			displayTemplateImageFields.add(displayTemplateImageField);
		     return displayTemplateImageField;
		});
		
		int db = displayTemplateImageFields.size();
		displayTemplateImageFieldService.addDisplayTemplateImageField(displayTemplateImageField);
		
        assertTrue(displayTemplateImageFields.contains(displayTemplateImageField));
        assertEquals(db + 1, displayTemplateImageFields.size());
	}
	
	@Test
	public void testGetAllDisplayTemplateImageFields() {
		when(displayTemplateImageFieldRepository.findAll()).thenReturn(displayTemplateImageFields);
		
		List<DisplayTemplateImageField> displayTemplateImageFields1 = displayTemplateImageFieldService.getAllDisplayTemplateImageFields();
		assertArrayEquals(displayTemplateImageFields.toArray(), displayTemplateImageFields1.toArray());
		
	}
}
