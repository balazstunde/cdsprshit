package edu.codespring.merx.server.backend.service;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import edu.codespring.merx.server.backend.model.Address;
import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.backend.repository.VendorRepository;
import edu.codespring.merx.server.service.VendorService;


@RunWith(MockitoJUnitRunner.class)
public class VendorServiceTest {
	
	@Mock
    private VendorRepository vendorRepository;
	
    @InjectMocks
    private VendorService vendorService;
    
    List<Vendor> vendors;
    List<Address> addresses = new ArrayList<>();
    
    @Before
    public void setVendors() {
    	vendors = new ArrayList<>();
		addresses.add(new Address("Romania, Odorheiu Secuiesc, strada Bethlen Gabor, nr. 1"));
    	vendors.add(new Vendor(1, "Simo", "simo", "http://localhost:8080/", "1.png", addresses));
    	vendors.add(new Vendor(2, "Cars", "cars", "http://localhost:8081/", "2.png", addresses));
    }
	
	@Test
	public void testGetVendor() {
		Vendor vendor = new Vendor();
        vendor.setId(1);
        vendor.setName("Simo");
        vendor.setUsername("simo");
        vendor.setUrl("http://localhost:8080/");
        vendor.setImage("1.png");
       vendor.setAddresses(addresses);
		when(vendorRepository.findById(1)).thenReturn(Optional.of(vendors.get(0)));
        
        Vendor vendor1 = vendorService.getVendor(1);
		assertThat(vendor1, samePropertyValuesAs(vendor));
	}
	
	@Test
	public void testAddVendor() {
		Vendor vendor = new Vendor();
        vendor.setId(3);
        vendor.setName("Simo1");
        vendor.setUsername("simo1");
        vendor.setUrl("http://localhost:8082/");
        vendor.setImage("3.png");
        vendor.setAddresses(addresses);
		when(vendorRepository.save(vendor)).thenAnswer(invocation -> {
		    vendors.add(vendor);
		     return vendor;
		});
		
		int db = vendors.size();
		vendorService.addVendor(vendor);
		
        assertTrue(vendors.contains(vendor));
        assertEquals(db + 1, vendors.size());
	}
	
	@Test
	public void testGetAllVendors() {
		when(vendorRepository.findAll()).thenReturn(vendors);
		
		List<Vendor> vendors1 = vendorService.getAllVendors();
		assertArrayEquals(vendors.toArray(), vendors1.toArray());
		
	}
}
