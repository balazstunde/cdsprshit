package edu.codespring.merx.server.backend.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateNumberFieldRepository;
import edu.codespring.merx.server.service.DisplayTemplateNumberFieldService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateNumberFieldServiceTest {
	@Mock
    private DisplayTemplateNumberFieldRepository displayTemplateNumberFieldRepository;
	
    @InjectMocks
    private DisplayTemplateNumberFieldService displayTemplateNumberFieldService;
    
    List<DisplayTemplateNumberField> displayTemplateNumberFields;
    
    @Before
    public void setDisplayTemplateNumberFields() {
    	displayTemplateNumberFields = new ArrayList<>();
    	displayTemplateNumberFields.add(new DisplayTemplateNumberField(3, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "engine_power", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"));
    	displayTemplateNumberFields.add(new DisplayTemplateNumberField(10, new ProductTemplateElement(10, "number_of_doors", "number", "blablabla", "numberOfDoors"), new LinearLayoutParams("LinearLayout"), 3, "number_of_doors", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"));
    }
	
	@Test
	public void testGetDisplayTemplateNumberField() {
		when(displayTemplateNumberFieldRepository.findById(3)).thenReturn(Optional.of(displayTemplateNumberFields.get(0)));
        
		assertEquals(displayTemplateNumberFieldService.getDisplayTemplateNumberField(3), displayTemplateNumberFields.get(0));
	}
	
	@Test
	public void testAddDisplayTemplateNumberField() {
		DisplayTemplateNumberField displayTemplateNumberField = new DisplayTemplateNumberField(12, new ProductTemplateElement(12, "mileage", "number", "blablabla", "mileage"), new LinearLayoutParams("LinearLayout"), 3, "mileage", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000");
		when(displayTemplateNumberFieldRepository.save(displayTemplateNumberField)).thenAnswer(invocation -> {
			displayTemplateNumberFields.add(displayTemplateNumberField);
		     return displayTemplateNumberField;
		});
		
		int db = displayTemplateNumberFields.size();
		displayTemplateNumberFieldService.addDisplayTemplateNumberField(displayTemplateNumberField);
		
        assertTrue(displayTemplateNumberFields.contains(displayTemplateNumberField));
        assertEquals(db + 1, displayTemplateNumberFields.size());
	}
	
	@Test
	public void testGetAllDisplayTemplateNumberFields() {
		when(displayTemplateNumberFieldRepository.findAll()).thenReturn(displayTemplateNumberFields);
		
		List<DisplayTemplateNumberField> displayTemplateNumberFields1 = displayTemplateNumberFieldService.getAllDisplayTemplateNumberFields();
		assertArrayEquals(displayTemplateNumberFields.toArray(), displayTemplateNumberFields1.toArray());
		
	}
}
