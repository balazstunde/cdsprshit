package edu.codespring.merx.server.backend.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplateImageList;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateImageListRepository;
import edu.codespring.merx.server.service.DisplayTemplateImageListService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateImageListServiceTest {
	
	@Mock
    private DisplayTemplateImageListRepository displayTemplateImageListRepository;
	
    @InjectMocks
    private DisplayTemplateImageListService displayTemplateImageListService;
    
    List<DisplayTemplateImageList> displayTemplateImageLists;
    
    @Before
    public void setDisplayTemplateImageLists() {
    	displayTemplateImageLists = new ArrayList<>();
    	displayTemplateImageLists.add(new DisplayTemplateImageList(20, new ProductTemplateElement(1, "images", "array", "blablabla", "images path"), new LinearLayoutParams("LinearLayout"), 2, "images", "ImageList", 200, 300, 16, "Arial", "#42acdd"));
    	displayTemplateImageLists.add(new DisplayTemplateImageList(20, new ProductTemplateElement(2, "images", "array", "blablabla", "images path"), new LinearLayoutParams("LinearLayout"), 2, "images", "ImageList", 200, 300, 16, "Arial", "#42acdd"));
    }
	
	@Test
	public void testGetDisplayTemplateImageList() {
		when(displayTemplateImageListRepository.findById(2)).thenReturn(Optional.of(displayTemplateImageLists.get(0)));
        
		assertEquals(displayTemplateImageListService.getDisplayTemplateImageList(2), displayTemplateImageLists.get(0));
	}
	
	@Test
	public void testAddDisplayTemplateImageList() {
		DisplayTemplateImageList displayTemplateImageList = new DisplayTemplateImageList(3, new ProductTemplateElement(20, "images", "array", "blablabla", "images path"), new LinearLayoutParams("LinearLayout"), 2, "images", "ImageList", 200, 300, 16, "Arial", "#42acdd");
		when(displayTemplateImageListRepository.save(displayTemplateImageList)).thenAnswer(invocation -> {
			displayTemplateImageLists.add(displayTemplateImageList);
		     return displayTemplateImageList;
		});
		
		int db = displayTemplateImageLists.size();
		displayTemplateImageListService.addDisplayTemplateImageList(displayTemplateImageList);
		
        assertTrue(displayTemplateImageLists.contains(displayTemplateImageList));
        assertEquals(db + 1, displayTemplateImageLists.size());
	}
	
	@Test
	public void testGetAllDisplayTemplateImageLists() {
		when(displayTemplateImageListRepository.findAll()).thenReturn(displayTemplateImageLists);
		
		List<DisplayTemplateImageList> displayTemplateImageLists1 = displayTemplateImageListService.getAllDisplayTemplateImageLists();
		assertArrayEquals(displayTemplateImageLists.toArray(), displayTemplateImageLists1.toArray());
		
	}
}
