package edu.codespring.merx.server.backend.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.ProductTemplateElementRepository;
import edu.codespring.merx.server.service.ProductTemplateElementService;

@RunWith(MockitoJUnitRunner.class)
public class ProductTemplateElementServiceTest {
	@Mock
    private ProductTemplateElementRepository productTemplateElementRepository;
	
    @InjectMocks
    private ProductTemplateElementService productTemplateElementService;
    
    List<ProductTemplateElement> productTemplateElements;
    
    @Before
    public void setProductTemplateElements() {
    	productTemplateElements = new ArrayList<>();
    	productTemplateElements.add(new ProductTemplateElement(1, "price", "number", "blablabla", "price"));
    	productTemplateElements.add(new ProductTemplateElement(5, "engine_name", "string", "blablabla", "engine name"));
    }
	
	@Test
	public void testGetProductTemplateElement() {
		ProductTemplateElement productTemplateElement = new ProductTemplateElement(1, "price", "number", "blablabla", "price");
		when(productTemplateElementRepository.findById(1)).thenReturn(Optional.of(productTemplateElements.get(0)));
        
        ProductTemplateElement productTemplateElement1 = productTemplateElementService.getProductTemplateElement(1);
		assertThat(productTemplateElement1, samePropertyValuesAs(productTemplateElement));
		assertEquals(productTemplateElement1, productTemplateElements.get(0));
	}
	
	@Test
	public void testAddProductTemplateElement() {
		ProductTemplateElement productTemplateElement = new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power");
		
		when(productTemplateElementRepository.save(productTemplateElement)).thenAnswer(invocation -> {
					productTemplateElements.add(productTemplateElement);
				     return productTemplateElement;
				});
		
		int db = productTemplateElements.size();
		productTemplateElementService.addProductTemplateElement(productTemplateElement);
		
        assertTrue(productTemplateElements.contains(productTemplateElement));
        assertEquals(db + 1, productTemplateElements.size());
	}
	
	@Test
	public void testGetAllProductTemplates() {
		when(productTemplateElementRepository.findAll()).thenReturn(productTemplateElements);
		
		List<ProductTemplateElement> productTemplateElements1 = productTemplateElementService.getAllProductTemplateElements();
		assertArrayEquals(productTemplateElements.toArray(), productTemplateElements1.toArray());
		
	}
	
	@Test
	public void testGetByName() {
		when(productTemplateElementRepository.findByName("price")).thenReturn(productTemplateElements.get(0));
		
		assertEquals(productTemplateElementService.getByName("price"), productTemplateElements.get(0));
	}
}
