package edu.codespring.merx.server.backend.service;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.DisplayTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayTemplateFloatField;
import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;
import edu.codespring.merx.server.backend.model.DisplayTemplateTextField;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateElementRepository;
import edu.codespring.merx.server.service.DisplayTemplateElementService;

@RunWith(MockitoJUnitRunner.class)
public class DisplayTemplateElementServiceTest {
	@Mock
    private DisplayTemplateElementRepository displayTemplateElementRepository;
	
    @InjectMocks
    private DisplayTemplateElementService displayTemplateElementService;
    
    List<DisplayTemplateElement> displayTemplateElements;
    
    @Before
    public void setDisplayTemplateElements() {
    	displayTemplateElements = new ArrayList<>();
    	displayTemplateElements.add(new DisplayTemplateFloatField(1, new ProductTemplateElement(1, "price", "number", "blablabla", "price"), new LinearLayoutParams("LinearLayout"), 3, "price", "FloatField", 16, "Arial", 16, "Arial", "#42acdd", "#000000"));
    	displayTemplateElements.add(new DisplayTemplateTextField(2, new ProductTemplateElement(2, "color", "string", "blablabla", "color color"), new LinearLayoutParams("LinearLayout"), 3, "color", "TextField", 12, "Arial", 16, "Arial", "#42acdd", "#000000"));
    }
	
	@Test
	public void testGetDisplayTemplateElement() {
		when(displayTemplateElementRepository.findById(1)).thenReturn(Optional.of(displayTemplateElements.get(0)));
		assertEquals(displayTemplateElementService.getDisplayTemplateElement(1), displayTemplateElements.get(0));
	}
	
	@Test
	public void testAddDisplayTemplateElement() {
		DisplayTemplateElement displayTemplateElement = new DisplayTemplateNumberField(3, new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"), new LinearLayoutParams("LinearLayout"), 3, "engine_power", "NumberField", 12, "Arial", 16, "Arial", "#42acdd", "#000000");
		when(displayTemplateElementRepository.save(displayTemplateElement)).thenAnswer(invocation -> {
			displayTemplateElements.add(displayTemplateElement);
		     return displayTemplateElement;
		});
		
		int db = displayTemplateElements.size();
		displayTemplateElementService.addDisplayTemplateElement(displayTemplateElement);
		
        assertTrue(displayTemplateElements.contains(displayTemplateElement));
        assertEquals(db + 1, displayTemplateElements.size());
	}
	
	@Test
	public void testGetAllProductTemplates() {
		when(displayTemplateElementRepository.findAll()).thenReturn(displayTemplateElements);
		
		List<DisplayTemplateElement> displayTemplateElements1 = displayTemplateElementService.getAllDisplayTemplateElements();
		assertArrayEquals(displayTemplateElements.toArray(), displayTemplateElements1.toArray());
		
	}
	
}
