package edu.codespring.merx.server.backend.service;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.codespring.merx.server.backend.model.ProductTemplate;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.ProductTemplateRepository;
import edu.codespring.merx.server.service.ProductTemplateService;

@RunWith(MockitoJUnitRunner.class)
public class ProductTemplateServiceTest {
	@Mock
    private ProductTemplateRepository productTemplateRepository;
	
    @InjectMocks
    private ProductTemplateService productTemplateService;
    
    List<ProductTemplate> productTemplates;
    List<ProductTemplateElement> elements;
    
    @Before
    public void setProductTemplates() {
    	productTemplates = new ArrayList<>();
    	elements = Arrays.asList(
				new ProductTemplateElement(1, "price", "number", "blablabla", "price"),
				new ProductTemplateElement(2, "color", "string", "blablabla", "color color"),
				new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"),
				new ProductTemplateElement(4, "cubic_capacity", "number", "blablabla", "engine cubicCapacity"),
				new ProductTemplateElement(5, "engine_name", "string", "blablabla", "engine name"),
				new ProductTemplateElement(6, "emission_class", "string", "blablabla", "emissionClass name"),
				new ProductTemplateElement(7, "body_style", "string", "blablabla", "bodyStyle name"),
				new ProductTemplateElement(8, "model_name", "string", "blablabla", "model name"),
				new ProductTemplateElement(9, "brand_name", "string", "blablabla", "model brand name"),
				new ProductTemplateElement(10, "number_of_doors", "number", "blablabla", "numberOfDoors"),
				new ProductTemplateElement(11, "co2_emission", "number", "blablabla", "co2Emission"),
				new ProductTemplateElement(12, "mileage", "number", "blablabla", "mileage"),
				new ProductTemplateElement(13, "automatic_transmission", "boolean", "blablabla", "automaticTransmission"),
				new ProductTemplateElement(14, "air_conditioning", "boolean", "blablabla", "airConditioning"),
				new ProductTemplateElement(15, "fuel_consumption_combined", "number", "blablabla", "fuelConsumptionCombined"),
				new ProductTemplateElement(16, "fuel_consumption_city", "number", "blablabla", "fuelConsumptionCity"),
				new ProductTemplateElement(17, "fuel_consumption_highway", "number", "blablabla", "fuelConsumptionHighway"),
				new ProductTemplateElement(18, "description", "string", "blablabla", "description"),
				new ProductTemplateElement(19, "features", "string", "blablabla", "features"),
				new ProductTemplateElement(20, "images", "array", "blablabla", "images path")
		);
    	productTemplates.add(new ProductTemplate(1, 1, "car", elements));
    	productTemplates.add(new ProductTemplate(2, 1, "bike", elements));
    }
	
	@Test
	public void testGetProductTemplate() {
		ProductTemplate productTemplate1 = new ProductTemplate(1, 1, "car", elements);
		when(productTemplateRepository.findById(1)).thenReturn(Optional.of(productTemplates.get(0)));
        
        ProductTemplate productTemplate = productTemplateService.getProductTemplate(1);
		assertThat(productTemplate, samePropertyValuesAs(productTemplate1));
		assertEquals(productTemplate, productTemplates.get(0));
	}
	
	@Test
	public void testAddProductTemplate() {
		ProductTemplate productTemplate1 = new ProductTemplate(3, 1, "roller", elements);
		when(productTemplateRepository.save(productTemplate1)).thenAnswer(invocation -> {
			productTemplates.add(productTemplate1);
		     return productTemplate1;
		});
		
		int db = productTemplates.size();
		productTemplateService.addProductTemplate(productTemplate1);
		
        assertTrue(productTemplates.contains(productTemplate1));
        assertEquals(db + 1, productTemplates.size());
	}
	
	@Test
	public void testGetAllProductTemplates() {
		when(productTemplateRepository.findAll()).thenReturn(productTemplates);
		
		List<ProductTemplate> productTemplates1 = productTemplateService.getAllProductTemplates();
		assertArrayEquals(productTemplates.toArray(), productTemplates1.toArray());
		
	}
	
	@Test
	public void testGetByVendorIdAndName() {
		ProductTemplate productTemplate1 = new ProductTemplate(1, 1, "car", elements);
		when(productTemplateRepository.findByVendorIdAndName(1, "car")).thenReturn(productTemplates.get(0));
        
        ProductTemplate productTemplate = productTemplateService.getByVendorIdAndName(1, "car");
		assertThat(productTemplate, samePropertyValuesAs(productTemplate1));
		assertEquals(productTemplate, productTemplates.get(0));
	}
}
