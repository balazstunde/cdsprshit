package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.DisplayTemplateElement;
import edu.codespring.merx.server.backend.repository.DisplayTemplateElementRepository;

@Service
public class DisplayTemplateElementService {
	@Autowired
	private DisplayTemplateElementRepository displayTemplateElementRepository;
	
	public DisplayTemplateElement getDisplayTemplateElement(int id) {
		return displayTemplateElementRepository.findById(id).get();
	}
	
	public DisplayTemplateElement addDisplayTemplateElement(DisplayTemplateElement displayTemplateElement) {
		return displayTemplateElementRepository.save(displayTemplateElement);
	}
	
	public List<DisplayTemplateElement> getAllDisplayTemplateElements() {
		List<DisplayTemplateElement> displayTemplateElements = new ArrayList<>();
		displayTemplateElementRepository.findAll().forEach(displayTemplateElements::add);
		
		return displayTemplateElements;
	}
}
