package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import edu.codespring.merx.server.backend.model.DisplayTemplateImageList;
import edu.codespring.merx.server.backend.repository.DisplayTemplateImageListRepository;

public class DisplayTemplateImageListService {
	@Autowired
	private DisplayTemplateImageListRepository displayTemplateImageListRepository;
	
	public DisplayTemplateImageList getDisplayTemplateImageList(int id) {
		return displayTemplateImageListRepository.findById(id).get();
	}
	
	public DisplayTemplateImageList addDisplayTemplateImageList(DisplayTemplateImageList displayTemplateImageList) {
		return displayTemplateImageListRepository.save(displayTemplateImageList);
	}
	
	public List<DisplayTemplateImageList> getAllDisplayTemplateImageLists() {
		List<DisplayTemplateImageList> displayTemplateImageLists = new ArrayList<>();
		displayTemplateImageListRepository.findAll().forEach(displayTemplateImageLists::add);
		
		return displayTemplateImageLists;
	}
}
