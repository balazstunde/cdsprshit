package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.ProductTemplateElement;

@Repository
public interface ProductTemplateElementRepository extends MongoRepository<ProductTemplateElement, Integer>{
	public ProductTemplateElement findByName(String name);
}
