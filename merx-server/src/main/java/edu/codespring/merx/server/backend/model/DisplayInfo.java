package edu.codespring.merx.server.backend.model;

import java.util.List;

public class DisplayInfo {
	private Integer id;
	private String name;
	private List<DisplayInfoElement> elements;
	private List<Layout> layoutElements;

	public DisplayInfo() {
		
	}
	
	public DisplayInfo(Integer id, String name, List<DisplayInfoElement> elements, List<Layout> layoutElements) {
		super();
		this.id = id;
		this.name = name;
		this.elements = elements;
		this.layoutElements = layoutElements;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DisplayInfoElement> getElements() {
		return elements;
	}

	public void setElements(List<DisplayInfoElement> elements) {
		this.elements = elements;
	}

	public List<Layout> getLayoutElements() {
		return layoutElements;
	}

	public void setLayoutElements(List<Layout> layoutElements) {
		this.layoutElements = layoutElements;
	}
	
}
