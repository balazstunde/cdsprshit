package edu.codespring.merx.server.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProductTemplateElements")
public class ProductTemplateElement {
	
	@Id
	private Integer id;
	private String name;
	private String type;
	private String constraints;
	private String source;
	
	public ProductTemplateElement() {
		
	}
	
	public ProductTemplateElement(Integer id, String name, String type, String constraints, String source) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.constraints = constraints;
		this.source = source;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getConstraints() {
		return constraints;
	}
	public void setConstraints(String constraints) {
		this.constraints = constraints;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
}
