package edu.codespring.merx.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import edu.codespring.merx.server.backend.model.Address;
import edu.codespring.merx.server.backend.model.BookmarkTemplate;
import edu.codespring.merx.server.backend.model.BookmarkTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayTemplate;
import edu.codespring.merx.server.backend.model.DisplayTemplateBooleanField;
import edu.codespring.merx.server.backend.model.DisplayTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayTemplateFloatField;
import edu.codespring.merx.server.backend.model.DisplayTemplateImageList;
import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;
import edu.codespring.merx.server.backend.model.DisplayTemplateTextField;
import edu.codespring.merx.server.backend.model.DisplayTemplateWebDisplay;
import edu.codespring.merx.server.backend.model.GridLayout;
import edu.codespring.merx.server.backend.model.GridLayoutParams;
import edu.codespring.merx.server.backend.model.Layout;
import edu.codespring.merx.server.backend.model.LinearLayout;
import edu.codespring.merx.server.backend.model.LinearLayoutParams;
import edu.codespring.merx.server.backend.model.ProductTemplate;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.service.BookmarkTemplateElementService;
import edu.codespring.merx.server.service.BookmarkTemplateService;
import edu.codespring.merx.server.service.DisplayTemplateElementService;
import edu.codespring.merx.server.service.DisplayTemplateService;
import edu.codespring.merx.server.service.ProductTemplateElementService;
import edu.codespring.merx.server.service.ProductTemplateService;
import edu.codespring.merx.server.service.VendorService;

@Component
public class DBSeeder implements ApplicationRunner{

	@Autowired
	private ProductTemplateElementService productTemplateElementService;
	@Autowired
	private ProductTemplateService productTemplateService;
	@Autowired
	private VendorService vendorService;
	@Autowired
	private DisplayTemplateElementService displayTemplateElementService;
	@Autowired
	private DisplayTemplateService displayTemplateService;
	@Autowired
	private BookmarkTemplateElementService bookmarkTemplateElementService;
	@Autowired
	private BookmarkTemplateService bookmarkTemplateService;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		//add a Vendor (Cars)
		List<Address> addresses1 = new ArrayList<>();
		addresses1.add(new Address("Romania, Odorheiu Secuiesc, strada Bethlen Gabor, nr. 1"));
		addresses1.add(new Address("Romania, Odorheiu Secuiesc, strada Bethlen Gabor, nr. 2"));
		vendorService.addVendor(new Vendor(1, "Auto Kereskedes", "autokereskedes", "http://localhost:8081", "1.png", addresses1));	
		
		//create an elements list with ProductTemplateElements (Product = Car)
		List<ProductTemplateElement> elements = Arrays.asList(
				new ProductTemplateElement(1, "price", "number", "blablabla", "price"),
				new ProductTemplateElement(2, "color", "string", "blablabla", "color color"),
				new ProductTemplateElement(3, "engine_power", "number", "blablabla", "engine power"),
				new ProductTemplateElement(4, "cubic_capacity", "number", "blablabla", "engine cubicCapacity"),
				new ProductTemplateElement(5, "engine_name", "string", "blablabla", "engine name"),
				new ProductTemplateElement(6, "emission_class", "string", "blablabla", "emissionClass name"),
				new ProductTemplateElement(7, "body_style", "string", "blablabla", "bodyStyle name"),
				new ProductTemplateElement(8, "model_name", "string", "blablabla", "model name"),
				new ProductTemplateElement(9, "brand_name", "string", "blablabla", "model brand name"),
				new ProductTemplateElement(10, "number_of_doors", "number", "blablabla", "numberOfDoors"),
				new ProductTemplateElement(11, "co2_emission", "number", "blablabla", "co2Emission"),
				new ProductTemplateElement(12, "mileage", "number", "blablabla", "mileage"),
				new ProductTemplateElement(13, "automatic_transmission", "boolean", "blablabla", "automaticTransmission"),
				new ProductTemplateElement(14, "air_conditioning", "boolean", "blablabla", "airConditioning"),
				new ProductTemplateElement(15, "fuel_consumption_combined", "number", "blablabla", "fuelConsumptionCombined"),
				new ProductTemplateElement(16, "fuel_consumption_city", "number", "blablabla", "fuelConsumptionCity"),
				new ProductTemplateElement(17, "fuel_consumption_highway", "number", "blablabla", "fuelConsumptionHighway"),
				new ProductTemplateElement(18, "description", "string", "blablabla", "description"),
				new ProductTemplateElement(19, "features", "string", "blablabla", "features"),
				new ProductTemplateElement(20, "images", "array", "blablabla", "images path")
				);
		
		for(int i = 0; i < elements.size(); i++) {
			productTemplateElementService.addProductTemplateElement(elements.get(i));
		}
		
		//create ProductTemplate with elements array (Product = Car)
		productTemplateService.addProductTemplate(new ProductTemplate(1, 1, "car", elements));
		
		//add some DisplayTemplateElement (Car)
		List<DisplayTemplateElement> elements1 = Arrays.asList(
				new DisplayTemplateFloatField(1, productTemplateElementService.getByName("price"),new LinearLayoutParams("LinearLayout"), 3, "price", "FloatField", 18, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(2, productTemplateElementService.getByName("color"), new LinearLayoutParams("LinearLayout"), 3, "color", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateNumberField(3, productTemplateElementService.getByName("engine_power"), new LinearLayoutParams("LinearLayout"), 3, "engine_power", "NumberField", 12, "Roboto regular",18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateFloatField(4, productTemplateElementService.getByName("cubic_capacity"), new LinearLayoutParams("LinearLayout"), 3, "cubic_capacity", "FloatField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(5, productTemplateElementService.getByName("engine_name"), new LinearLayoutParams("LinearLayout"), 3, "engine_name", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(6, productTemplateElementService.getByName("emission_class"), new LinearLayoutParams("LinearLayout"), 3, "emission_class", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(7, productTemplateElementService.getByName("body_style"), new LinearLayoutParams("LinearLayout"), 3, "body_style", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(8, productTemplateElementService.getByName("model_name"), new LinearLayoutParams("LinearLayout"), 3, "model_name", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(9, productTemplateElementService.getByName("brand_name"), new LinearLayoutParams("LinearLayout"), 3, "brand_name", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateNumberField(10, productTemplateElementService.getByName("number_of_doors"), new LinearLayoutParams("LinearLayout"), 3, "number_of_doors", "NumberField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateFloatField(11, productTemplateElementService.getByName("co2_emission"), new LinearLayoutParams("LinearLayout"), 3, "co2_emission", "FloatField", 12, "Roboto regular",18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateNumberField(12, productTemplateElementService.getByName("mileage"), new LinearLayoutParams("LinearLayout"), 3, "mileage", "NumberField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateBooleanField(13, productTemplateElementService.getByName("automatic_transmission"), new LinearLayoutParams("LinearLayout"), 3, "automatic_transmission", "BooleanField", 18, "Roboto regular", "#000000"),
				new DisplayTemplateBooleanField(14, productTemplateElementService.getByName("air_conditioning"), new LinearLayoutParams("LinearLayout"), 3, "air_conditioning", "BooleanField", 18, "Roboto regular", "#000000"),
				new DisplayTemplateFloatField(15, productTemplateElementService.getByName("fuel_consumption_combined"), new LinearLayoutParams("LinearLayout"), 3, "fuel_consumption_combined", "FloatField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateFloatField(16, productTemplateElementService.getByName("fuel_consumption_city"), new LinearLayoutParams("LinearLayout"), 3, "fuel_consumption_city", "FloatField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateFloatField(17, productTemplateElementService.getByName("fuel_consumption_highway"), new LinearLayoutParams("LinearLayout"), 3, "fuel_consumption_highway", "FloatField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(18, productTemplateElementService.getByName("description"), new LinearLayoutParams("LinearLayout"), 3, "description", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateTextField(19, productTemplateElementService.getByName("features"), new LinearLayoutParams("LinearLayout"), 3, "features", "TextField", 12, "Roboto regular", 18, "Roboto regular", "#000000", "#000000"),
				new DisplayTemplateImageList(20, productTemplateElementService.getByName("images"), new LinearLayoutParams("LinearLayout"), 2, "images", "ImageList", 200, 300,18, "Roboto regular", "#000000")
		);
		
		for(int i = 0; i < elements1.size(); i++) {
			displayTemplateElementService.addDisplayTemplateElement(elements1.get(i));
		}
		
		List<Layout> layouts = Arrays.asList(
				new GridLayout("GridLayout", 1, 0, new LinearLayoutParams("LinearLayout"), 2, 1),
				new LinearLayout("LinearLayout", 2, 1, new GridLayoutParams("GridLayout", 0, 0), "horizontal"),
				new LinearLayout("LinearLayout", 3, 1, new GridLayoutParams("GridLayout", 1, 0), "vertical")
		);
		
		displayTemplateService.addDisplayTemplate(new DisplayTemplate(1, "car", 1, elements1, layouts));
		
		List<BookmarkTemplateElement> bookmarkTemplateElements1 = Arrays.asList(
				new BookmarkTemplateElement(1, "car", "model brand name"),
				new BookmarkTemplateElement(2, "car", "model name")
		);
		
		for(int i = 0; i < bookmarkTemplateElements1.size(); i++) {
			bookmarkTemplateElementService.addBookmarkTemplateElement(bookmarkTemplateElements1.get(i));
		}
		
		bookmarkTemplateService.addBookmarkTemplate(new BookmarkTemplate(1, "car", 1, bookmarkTemplateElements1));
		
		//add a Vendor (Cycle)
		List<Address> addresses2 = new ArrayList<>();
		addresses2.add(new Address("Romania, Cluj Napoca, strada Mihail Kogalniceanu nr. 1"));
		vendorService.addVendor(new Vendor(2, "chainreactioncycles", "chainreactioncycles", "http://localhost:8082", "2.jpg", addresses2));	
		
		List<ProductTemplateElement> elements2 = Arrays.asList(
				new ProductTemplateElement(21, "bicycle_price", "number", "blablabla", "price"),
				new ProductTemplateElement(22, "bicycle_name", "string", "blablabla", "bicycleName"),
				new ProductTemplateElement(23, "bicycle_model_name", "string", "blablabla", "model modelName"),
				new ProductTemplateElement(24, "weight", "number", "blablabla", "weight"),
				new ProductTemplateElement(25, "fork", "string", "blablabla", "fork"),
				new ProductTemplateElement(26, "rotors", "string", "blablabla", "rotors"),
				new ProductTemplateElement(27, "rating", "number", "blablabla", "rating"),
				new ProductTemplateElement(28, "bicycle_color", "string", "blablabla", "color"),
				new ProductTemplateElement(29, "wheel_size", "string", "blablabla", "wheelSize"),
				new ProductTemplateElement(30, "bicycle_images", "array", "blablabla", "images path")
				);
		
		for(int i = 0; i < elements2.size(); i++) {
			productTemplateElementService.addProductTemplateElement(elements2.get(i));
		}
		
		//create ProductTemplate with elements array (Product = Bicycle)
		productTemplateService.addProductTemplate(new ProductTemplate(2, 2, "bicycle", elements2));
		
		List<DisplayTemplateElement> elements3 = Arrays.asList(
				new DisplayTemplateFloatField(21, productTemplateElementService.getByName("bicycle_price"),new LinearLayoutParams("LinearLayout"), 3, "price", "FloatField", 22, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(22, productTemplateElementService.getByName("bicycle_name"), new LinearLayoutParams("LinearLayout"), 3, "bicycle_name", "TextField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(23, productTemplateElementService.getByName("bicycle_model_name"), new LinearLayoutParams("LinearLayout"), 3, "model_name", "TextField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateFloatField(24, productTemplateElementService.getByName("weight"), new LinearLayoutParams("LinearLayout"), 3, "weight", "FloatField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(25, productTemplateElementService.getByName("fork"), new LinearLayoutParams("LinearLayout"), 3, "fork", "TextField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(26, productTemplateElementService.getByName("rotors"), new LinearLayoutParams("LinearLayout"), 3, "rotors", "TextField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateFloatField(27, productTemplateElementService.getByName("rating"), new LinearLayoutParams("LinearLayout"), 3, "rating", "FloatField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(28, productTemplateElementService.getByName("bicycle_color"), new LinearLayoutParams("LinearLayout"), 3, "color", "TextField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(29, productTemplateElementService.getByName("wheel_size"), new LinearLayoutParams("LinearLayout"), 3, "wheel_size", "TextField", 16, "sans-serif-ligh", 22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateImageList(30, productTemplateElementService.getByName("bicycle_images"), new LinearLayoutParams("LinearLayout"), 2, "images", "ImageList", 200, 300, 22, "sans-serif-ligh", "#42acdd")
		);
		
		for(int i = 0; i < elements3.size(); i++) {
			displayTemplateElementService.addDisplayTemplateElement(elements3.get(i));
		}
		
		displayTemplateService.addDisplayTemplate(new DisplayTemplate(2, "bicycle", 2, elements3, layouts));
		
		List<BookmarkTemplateElement> bookmarkTemplateElements2 = Arrays.asList(
				new BookmarkTemplateElement(1, "bicycle", "bicycleName"),
				new BookmarkTemplateElement(1, "bicycle", "model modelName")
		);
		
		for(int i = 0; i < bookmarkTemplateElements2.size(); i++) {
			bookmarkTemplateElementService.addBookmarkTemplateElement(bookmarkTemplateElements2.get(i));
		}
		
		bookmarkTemplateService.addBookmarkTemplate(new BookmarkTemplate(2, "bicycle", 2, bookmarkTemplateElements2));
		
		//add a Vendor (Car Simoo!!!)
		List<Address> addresses3 = new ArrayList<>();
		addresses3.add(new Address("Romania, Miercurea Ciuc, strada Petofi Sandor nr. 35"));
		vendorService.addVendor(new Vendor(3, "Simo skytrans_vo", "simoauto", "http://localhost:8083", "3.png", addresses3));	
		
		List<ProductTemplateElement> elements4 = Arrays.asList(
				new ProductTemplateElement(32, "simo_car_price", "number", "", "price"),
				new ProductTemplateElement(33, "simo_car_brand", "string", "", "brand name"),
				new ProductTemplateElement(34, "simo_car_type", "string", "", "type name"),
				new ProductTemplateElement(35, "simo_car_emission_class", "string", "", "emissionClass name"),
				new ProductTemplateElement(36, "simo_car_number_of_seats", "number", "", "numberOfSeats"),
				new ProductTemplateElement(37, "simo_car_color", "string", "", "color"),
				new ProductTemplateElement(38, "simo_car_kilometers", "number", "", "kilometers"),
				new ProductTemplateElement(39, "simo_car_description", "string", "", "description"),
				new ProductTemplateElement(40, "simo_car_images", "array", "", "images file"),
				new ProductTemplateElement(41, "simo_car_facilities", "string", "", "facilities"),
				new ProductTemplateElement(42, "simo_car_motorization", "string", "", "motorization"),
				new ProductTemplateElement(43, "simo_car_vintage", "string", "", "vintage"),
				new ProductTemplateElement(44, "simo_car_battery", "string", "", "battery"),
				new ProductTemplateElement(45, "simo_car_spare_wheel", "string", "", "spareWheel"),
				new ProductTemplateElement(46, "simo_car_wheel_size", "string", "", "wheelSize"),
				new ProductTemplateElement(47, "simo_car_body", "string", "", "body"),
				new ProductTemplateElement(48, "simo_car_remark", "string", "", "remark")
				);
		
		for(int i = 0; i < elements4.size(); i++) {
			productTemplateElementService.addProductTemplateElement(elements4.get(i));
		}
		
		//create ProductTemplate with elements array (Product = Bicycle)
		productTemplateService.addProductTemplate(new ProductTemplate(3, 3, "car", elements4));
		
		List<DisplayTemplateElement> elements5 = Arrays.asList(
				new DisplayTemplateNumberField(32, productTemplateElementService.getByName("simo_car_price"),new LinearLayoutParams("LinearLayout"), 4, "Price (�)", "NumberField", 24, "sans-serif-ligh",22, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(33, productTemplateElementService.getByName("simo_car_brand"), new LinearLayoutParams("LinearLayout"), 4, "Brand", "TextField", 16, "sans-serif-ligh",16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(34, productTemplateElementService.getByName("simo_car_type"), new LinearLayoutParams("LinearLayout"), 4, "Type", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(35, productTemplateElementService.getByName("simo_car_emission_class"), new LinearLayoutParams("LinearLayout"), 4, "Emission class", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateNumberField(36, productTemplateElementService.getByName("simo_car_number_of_seats"), new LinearLayoutParams("LinearLayout"), 4, "Number of seats", "NumberField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(37, productTemplateElementService.getByName("simo_car_color"), new LinearLayoutParams("LinearLayout"), 4, "Color", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateNumberField(38, productTemplateElementService.getByName("simo_car_kilometers"), new LinearLayoutParams("LinearLayout"), 4, "Kilometers", "NumberField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateWebDisplay(39, productTemplateElementService.getByName("simo_car_description"), new LinearLayoutParams("LinearLayout"), 5, "Description", "WebDisplay", 16, "sans-serif-medium", "#42acdd"),
				new DisplayTemplateImageList(40, productTemplateElementService.getByName("simo_car_images"), new LinearLayoutParams("LinearLayout"), 2, "Images", "ImageList", 300, 400, 16, "sans-serif-medium", "#42acdd"),
				new DisplayTemplateTextField(41, productTemplateElementService.getByName("simo_car_facilities"),new LinearLayoutParams("LinearLayout"), 5, "Facilities", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(42, productTemplateElementService.getByName("simo_car_motorization"), new LinearLayoutParams("LinearLayout"), 4, "Motorization", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(43, productTemplateElementService.getByName("simo_car_vintage"), new LinearLayoutParams("LinearLayout"), 4, "Vintage", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(44, productTemplateElementService.getByName("simo_car_battery"), new LinearLayoutParams("LinearLayout"), 4, "Battery", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(45, productTemplateElementService.getByName("simo_car_spare_wheel"), new LinearLayoutParams("LinearLayout"), 4, "Spare wheel", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(46, productTemplateElementService.getByName("simo_car_wheel_size"), new LinearLayoutParams("LinearLayout"), 5, "Wheel size", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateTextField(47, productTemplateElementService.getByName("simo_car_body"), new LinearLayoutParams("LinearLayout"), 5, "Body", "TextField", 16, "sans-serif-ligh", 16, "sans-serif-medium", "#42acdd", "#000000"),
				new DisplayTemplateWebDisplay(48, productTemplateElementService.getByName("simo_car_remark"), new LinearLayoutParams("LinearLayout"), 5, "Remark", "WebDisplay", 16, "sans-serif-medium", "#42acdd")
		);
		
		for(int i = 0; i < elements5.size(); i++) {
			displayTemplateElementService.addDisplayTemplateElement(elements5.get(i));
		}
		
		List<Layout> layouts1 = Arrays.asList(
				new GridLayout("GridLayout", 1, 0, new LinearLayoutParams("LinearLayout"), 2, 1),
				new LinearLayout("LinearLayout", 2, 1, new GridLayoutParams("GridLayout", 0, 0), "horizontal"),
				new LinearLayout("LinearLayout", 3, 1, new GridLayoutParams("GridLayout", 1, 0), "horizontal"),
				new LinearLayout("LinearLayout", 4, 3, new LinearLayoutParams("LinearLayout"), "vertical"),
				new LinearLayout("LinearLayout", 5, 3, new LinearLayoutParams("LinearLayout"), "vertical")
		);
		
		displayTemplateService.addDisplayTemplate(new DisplayTemplate(3, "car", 3, elements5, layouts1));


	}
}
