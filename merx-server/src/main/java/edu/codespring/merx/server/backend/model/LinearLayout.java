package edu.codespring.merx.server.backend.model;

public class LinearLayout extends Layout{
	
	public LinearLayout() {
		super();
	}

	public LinearLayout(String name, Integer id, Integer parentLayoutId, LayoutParams parentLayoutParams, String mode) {
		super(name, id, parentLayoutId, parentLayoutParams, mode);
	}
	
}
