package edu.codespring.merx.server.backend.model;

public abstract class DisplayInfoElement {
	
	private String name;
	private String type;
	private Integer parentLayoutId;
	private LayoutParams parentLayoutParams;
	
	public DisplayInfoElement() {
		
	}

	public DisplayInfoElement(String name, String type, Integer parentLayoutId, LayoutParams parentLayoutParams) {
		super();
		this.name = name;
		this.type = type;
		this.parentLayoutId = parentLayoutId;
		this.parentLayoutParams = parentLayoutParams;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getParentLayoutId() {
		return parentLayoutId;
	}

	public void setParentLayoutId(Integer parentLayoutId) {
		this.parentLayoutId = parentLayoutId;
	}

	public LayoutParams getParentLayoutParams() {
		return parentLayoutParams;
	}

	public void setParentLayoutParams(LayoutParams parentLayoutParams) {
		this.parentLayoutParams = parentLayoutParams;
	}
	
}
