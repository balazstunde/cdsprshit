package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.BookmarkTemplate;

@Repository
public interface BookmarkTemplateRepository extends MongoRepository<BookmarkTemplate, Integer>{

	public BookmarkTemplate findByVendorIdAndName(Integer id, String name);
}
