package edu.codespring.merx.server.backend.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "BookmarkTemplates")
public class BookmarkTemplate {
	
	@Id
	private Integer id;
	private String name;
	private Integer vendorId;
	private List<BookmarkTemplateElement> elements;
	
	public BookmarkTemplate() {
		
	}
	
	public BookmarkTemplate(Integer id, String name, Integer vendorId, List<BookmarkTemplateElement> elements) {
		super();
		this.id = id;
		this.name = name;
		this.vendorId = vendorId;
		this.elements = elements;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public List<BookmarkTemplateElement> getElements() {
		return elements;
	}

	public void setElements(List<BookmarkTemplateElement> elements) {
		this.elements = elements;
	}
	
	
}
