package edu.codespring.merx.server.backend.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import edu.codespring.merx.server.service.VendorService;


@Controller
public class QRCodeGeneratorController {
	
	@Autowired
	private VendorService vendorService;
	
	@RequestMapping(value="vendor/{vendorId}/{product}/{productId}/{bookmark}/qr/{width}/{height}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getImage(@PathVariable Integer vendorId, @PathVariable String product, @PathVariable Integer productId, @PathVariable String bookmark, @PathVariable Integer width, @PathVariable Integer height) {
		String url = "{Vendor: {\"id\":" + vendorId + ",\"name\":\"" + vendorService.getVendor(vendorId).getName() + "\"}, Product: {\"id\": \""+ productId + "\", \"name\": \"" + bookmark + "\", \"type\" : \"" + product + "\", \"image_url\":\"\"}}";
		System.out.println(url);
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = null;
		try {
			bitMatrix = qrCodeWriter.encode(url, BarcodeFormat.QR_CODE, width, height);
		} catch (WriterException e) {
			e.printStackTrace();
		}
		
		BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();

        try {
			ImageIO.write(image, "png", bao);
		} catch (IOException e) {
			e.printStackTrace();
		}

        return bao.toByteArray();
	}
}
