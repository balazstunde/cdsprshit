package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;

@Repository
public interface DisplayTemplateNumberFieldRepository extends MongoRepository<DisplayTemplateNumberField, Integer>{

}
