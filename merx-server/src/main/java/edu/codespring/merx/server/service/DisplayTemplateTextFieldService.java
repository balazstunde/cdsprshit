package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.DisplayTemplateTextField;
import edu.codespring.merx.server.backend.repository.DisplayTemplateTextFieldRepository;

@Service
public class DisplayTemplateTextFieldService {
	@Autowired
	private DisplayTemplateTextFieldRepository displayTemplateTextFieldRepository;
	
	public DisplayTemplateTextField getDisplayTemplateTextField(int id) {
		return displayTemplateTextFieldRepository.findById(id).get();
	}
	
	public DisplayTemplateTextField addDisplayTemplateTextField(DisplayTemplateTextField displayTemplateTextField) {
		return displayTemplateTextFieldRepository.save(displayTemplateTextField);
	}
	
	public List<DisplayTemplateTextField> getAllDisplayTemplateTextFields() {
		List<DisplayTemplateTextField> displayTemplateTextFields = new ArrayList<>();
		displayTemplateTextFieldRepository.findAll().forEach(displayTemplateTextFields::add);
		
		return displayTemplateTextFields;
	}
}
