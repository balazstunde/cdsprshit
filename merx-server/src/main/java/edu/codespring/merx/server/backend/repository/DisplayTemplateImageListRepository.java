package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.DisplayTemplateImageList;

@Repository
public interface DisplayTemplateImageListRepository extends MongoRepository<DisplayTemplateImageList, Integer>{

}
