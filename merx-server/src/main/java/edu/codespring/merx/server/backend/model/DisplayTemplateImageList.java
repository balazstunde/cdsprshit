package edu.codespring.merx.server.backend.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DisplayTemplateImageLists")
public class DisplayTemplateImageList extends DisplayTemplateElement{
	
	public DisplayTemplateImageList() {
		
	}

	public DisplayTemplateImageList(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type, Integer width,
			Integer height, Integer keyFontSize, String keyFontFamily, String keyColor) {
		super(id, productTemplateElement, parentLayoutParams, parentLayoutId, name, type, width, height, keyFontSize, keyFontFamily, keyColor);
	}

	
}
