package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.MerxUser;
import edu.codespring.merx.server.backend.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	public MerxUser getUser(String username) {
		return userRepository.findByUsername(username);
	}
	
	public MerxUser addUser(MerxUser user) {
		return userRepository.save(user);
	}
	
	public List<MerxUser> getAllUsers() {
		List<MerxUser> users = new ArrayList<>();
		userRepository.findAll().forEach(users::add);
		
		return users;
	}
	
	public MerxUser getByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	public void deleteUser(MerxUser user) {
		userRepository.delete(user);
	}
}
