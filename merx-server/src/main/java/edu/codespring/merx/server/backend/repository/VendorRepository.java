package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.Vendor;

@Repository
public interface VendorRepository extends MongoRepository<Vendor, Integer>{
	Vendor findByUsername(String username);
}
