package edu.codespring.merx.server.backend.model;

public class LayoutParams {
	
	private String parentLayoutName;
	private int row;
	private int column;
	
	public LayoutParams() {
		
	}

	public LayoutParams(String parentLayoutName) {
		super();
		this.parentLayoutName = parentLayoutName;
	}
	
	public LayoutParams(String parentLayoutName, Integer row, Integer column) {
		super();
		this.parentLayoutName = parentLayoutName;
		this.row = row;
		this.column = column;
	}

	public String getParentLayoutName() {
		return parentLayoutName;
	}

	public void setParentLayoutName(String parentLayoutName) {
		this.parentLayoutName = parentLayoutName;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}
	
}
