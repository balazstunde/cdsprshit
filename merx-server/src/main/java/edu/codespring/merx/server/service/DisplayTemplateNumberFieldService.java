package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;
import edu.codespring.merx.server.backend.repository.DisplayTemplateNumberFieldRepository;

@Service
public class DisplayTemplateNumberFieldService {
	@Autowired
	private DisplayTemplateNumberFieldRepository displayTemplateNumberFieldRepository;
	
	public DisplayTemplateNumberField getDisplayTemplateNumberField(int id) {
		return displayTemplateNumberFieldRepository.findById(id).get();
	}
	
	public DisplayTemplateNumberField addDisplayTemplateNumberField(DisplayTemplateNumberField displayTemplateNumberField) {
		return displayTemplateNumberFieldRepository.save(displayTemplateNumberField);
	}
	
	public List<DisplayTemplateNumberField> getAllDisplayTemplateNumberFields() {
		List<DisplayTemplateNumberField> displayTemplateNumberFields = new ArrayList<>();
		displayTemplateNumberFieldRepository.findAll().forEach(displayTemplateNumberFields::add);
		
		return displayTemplateNumberFields;
	}
}
