package edu.codespring.merx.server.backend.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DisplayTemplateNumberFields")
public class DisplayTemplateNumberField extends DisplayTemplateElement{
	
	public DisplayTemplateNumberField() {
		
	}

	public DisplayTemplateNumberField(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type, Integer valueFontSize,
			String valueFontFamily, Integer keyFontSize, String keyFontFamily, String keyColor, String valueColor) {
		super(id, productTemplateElement, parentLayoutParams, parentLayoutId, name, type, valueFontSize, valueFontFamily, keyFontSize, keyFontFamily, keyColor, valueColor);

	}
}
