package edu.codespring.merx.server.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DisplayTemplateElements")
public class DisplayTemplateElement {

	@Id
	private Integer id;
	private ProductTemplateElement productTemplateElement;
	private LayoutParams parentLayoutParams;
	
	private Integer parentLayoutId;
	private String name;
	private String type;
	private Integer valueFontSize;
	private String valueFontFamily;
	private Integer keyFontSize;
	private String keyFontFamily;
	private String keyColor;
	private String valueColor;
	private Integer width;
	private Integer height;
	
	public DisplayTemplateElement() {
		
	}

	public DisplayTemplateElement(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type) {
		super();
		this.id = id;
		this.productTemplateElement = productTemplateElement;
		this.parentLayoutParams = parentLayoutParams;
		this.parentLayoutId = parentLayoutId;
		this.name = name;
		this.type = type;
	}
	
	//booleanfield
	public DisplayTemplateElement(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type, Integer keyFontSize, String keyFontFamily, String keyColor) {
		super();
		this.id = id;
		this.productTemplateElement = productTemplateElement;
		this.parentLayoutParams = parentLayoutParams;
		this.parentLayoutId = parentLayoutId;
		this.name = name;
		this.type = type;
	}
	
	//floatfield
	public DisplayTemplateElement(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type, Integer valueFontSize, String valueFontFamily, Integer keyFontSize, String keyFontFamily, String keyColor, String valueColor) {
		super();
		this.id = id;
		this.productTemplateElement = productTemplateElement;
		this.parentLayoutParams = parentLayoutParams;
		this.parentLayoutId = parentLayoutId;
		this.name = name;
		this.type = type;
		this.valueFontSize=valueFontSize;
		this.valueFontFamily=valueFontFamily;
		this.keyFontSize=keyFontSize;
		this.keyFontFamily=keyFontFamily;
		this.keyColor=keyColor;
		this.valueColor=valueColor;
	}
	
	//imagefield
	public DisplayTemplateElement(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type,
			Integer width, Integer height, Integer keyFontSize, String keyFontFamily, String keyColor) {
		super();
		this.id = id;
		this.productTemplateElement = productTemplateElement;
		this.parentLayoutParams = parentLayoutParams;
		this.parentLayoutId = parentLayoutId;
		this.name = name;
		this.type = type;
		this.width=width;
		this.height=height;
		this.keyFontSize=keyFontSize;
		this.keyFontFamily=keyFontFamily;
		this.keyColor=keyColor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public ProductTemplateElement getProductTemplateElement() {
		return productTemplateElement;
	}

	public void setProductTemplateElement(ProductTemplateElement productTemplateElement) {
		this.productTemplateElement = productTemplateElement;
	}

	public Integer getParentLayoutId() {
		return parentLayoutId;
	}

	public void setParentLayoutId(Integer parentLayoutId) {
		this.parentLayoutId = parentLayoutId;
	}

	public LayoutParams getParentLayoutParams() {
		return parentLayoutParams;
	}

	public void setParentLayoutParams(LayoutParams parentLayoutParams) {
		this.parentLayoutParams = parentLayoutParams;
	}

	public Integer getValueFontSize() {
		return valueFontSize;
	}

	public void setValueFontSize(Integer valueFontSize) {
		this.valueFontSize = valueFontSize;
	}

	public String getValueFontFamily() {
		return valueFontFamily;
	}

	public void setValueFontFamily(String valueFontFamily) {
		this.valueFontFamily = valueFontFamily;
	}

	public Integer getKeyFontSize() {
		return keyFontSize;
	}

	public void setKeyFontSize(Integer keyFontSize) {
		this.keyFontSize = keyFontSize;
	}

	public String getKeyFontFamily() {
		return keyFontFamily;
	}

	public void setKeyFontFamily(String keyFontFamily) {
		this.keyFontFamily = keyFontFamily;
	}

	public String getKeyColor() {
		return keyColor;
	}

	public void setKeyColor(String keyColor) {
		this.keyColor = keyColor;
	}

	public String getValueColor() {
		return valueColor;
	}

	public void setValueColor(String valueColor) {
		this.valueColor = valueColor;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

}
