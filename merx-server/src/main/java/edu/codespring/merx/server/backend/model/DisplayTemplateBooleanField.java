package edu.codespring.merx.server.backend.model;

public class DisplayTemplateBooleanField extends DisplayTemplateElement{

	public DisplayTemplateBooleanField() {
		
	}
	
	public DisplayTemplateBooleanField(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type, Integer keyFontSize,
			String keyFontFamily, String keyColor) {
		super(id, productTemplateElement, parentLayoutParams, parentLayoutId, name, type, keyFontSize, keyFontFamily, keyColor);
	}

}
