package edu.codespring.merx.server.backend.model;

public enum UserRole {
	ROLE_USER, ROLE_ADMIN
}
