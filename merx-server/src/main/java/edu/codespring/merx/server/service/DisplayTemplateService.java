package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.DisplayTemplate;
import edu.codespring.merx.server.backend.repository.DisplayTemplateRepository;

@Service
public class DisplayTemplateService {
	
	final static Logger logger = LoggerFactory.getLogger(DisplayTemplateService.class);
	
	@Autowired
	private DisplayTemplateRepository displayTemplateRepository;
	
	public DisplayTemplate getDisplayTemplate(int id) {
		try {
			return displayTemplateRepository.findById(id).get();
		}catch(DataAccessException ex) {
			logger.error("Exception in get display template.");
			throw new ServiceException("Exception in get display template.", ex);
		}
		
	}
	
	public DisplayTemplate addDisplayTemplate(DisplayTemplate displayTemplate) {
		try {
			return displayTemplateRepository.save(displayTemplate);
		}catch(DataAccessException ex) {
			logger.error("Exception in add display template.");
			throw new ServiceException("Exception in add display template.", ex);
		}
		
	}
	
	public List<DisplayTemplate> getAllDisplayTemplates() {
		try {
			List<DisplayTemplate> displayTemplates = new ArrayList<>();
			displayTemplateRepository.findAll().forEach(displayTemplates::add);
			
			return displayTemplates;
		}catch(DataAccessException ex) {
			logger.error("Exception in get all display template.");
			throw new ServiceException("Exception in get all display template.", ex);
		}
	}
	
	public DisplayTemplate getByName(String name) {
		try {
			return displayTemplateRepository.findByName(name);
		}catch(DataAccessException ex) {
			logger.error("Exception in get by name display template.");
			throw new ServiceException("Exception in get by name display template.", ex);
		}
	}
	
	public DisplayTemplate getByVendorIdAndName(Integer id, String name) {
		try {
			return displayTemplateRepository.findByVendorIdAndName(id, name);
		}catch(DataAccessException ex) {
			logger.error("Exception in get by vendor id and name display template.");
			throw new ServiceException("Exception in get by vendor id and name display template.", ex);
		}
	}
	
	public List<DisplayTemplate> getByVendorId(Integer id) {
		try {
			return displayTemplateRepository.findByVendorId(id);
		}catch(DataAccessException ex) {
			logger.error("Exception in get by vendor id display template.");
			throw new ServiceException("Exception in get by vendor id display template.", ex);
		}
	}
	
	public void deleteTemplate(DisplayTemplate displayTemplate) {
		try {
			displayTemplateRepository.delete(displayTemplate);
		}catch(DataAccessException ex) {
			logger.error("Exception in delete display template.");
			throw new ServiceException("Exception in delete display template.", ex);
		}
	}
}
