package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.MerxUser;

@Repository
public interface UserRepository extends MongoRepository<MerxUser, Long>{
	MerxUser findByUsername(String username);
}
