package edu.codespring.merx.server.backend.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import edu.codespring.merx.server.backend.model.BookmarkTemplate;
import edu.codespring.merx.server.backend.model.BookmarkTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayInfo;
import edu.codespring.merx.server.backend.model.DisplayInfoBooleanElement;
import edu.codespring.merx.server.backend.model.DisplayInfoElement;
import edu.codespring.merx.server.backend.model.DisplayInfoFloatElement;
import edu.codespring.merx.server.backend.model.DisplayInfoImageElement;
import edu.codespring.merx.server.backend.model.DisplayInfoImageListElement;
import edu.codespring.merx.server.backend.model.DisplayInfoNumberElement;
import edu.codespring.merx.server.backend.model.DisplayInfoTextElement;
import edu.codespring.merx.server.backend.model.DisplayInfoWebDisplayElement;
import edu.codespring.merx.server.backend.model.DisplayTemplate;
import edu.codespring.merx.server.backend.model.DisplayTemplateBooleanField;
import edu.codespring.merx.server.backend.model.DisplayTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayTemplateFloatField;
import edu.codespring.merx.server.backend.model.DisplayTemplateImageField;
import edu.codespring.merx.server.backend.model.DisplayTemplateImageList;
import edu.codespring.merx.server.backend.model.DisplayTemplateNumberField;
import edu.codespring.merx.server.backend.model.DisplayTemplateTextField;
import edu.codespring.merx.server.backend.model.DisplayTemplateWebDisplay;
import edu.codespring.merx.server.backend.model.Layout;
import edu.codespring.merx.server.backend.model.ProductTemplate;
import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.service.BookmarkTemplateService;
import edu.codespring.merx.server.service.DisplayTemplateService;
import edu.codespring.merx.server.service.NextSequenceService;
import edu.codespring.merx.server.service.ProductTemplateService;
import edu.codespring.merx.server.service.ServiceException;
import edu.codespring.merx.server.service.VendorService;

@RestController
public class TemplateController {
	
	final static Logger logger = LoggerFactory.getLogger(TemplateController.class);
	
	@Autowired
	private VendorService vendorService;
	@Autowired
	private DisplayTemplateService displayTemplateService;
	@Autowired
	private ProductTemplateService productTemplateService;
	@Autowired
	private BookmarkTemplateService bookMarkTemplateService;
	@Autowired
	private NextSequenceService nextSequenceService;
	
	@RequestMapping(value = "vendor/{vendorId}/product-templates/", method = RequestMethod.POST)
	public ProductTemplate addTemplate(@RequestBody ProductTemplate productTemplate) {
		try {
			//set id for productTemplate
			productTemplate.setId(nextSequenceService.getNextSequence("productTemplateSequences"));
			
			//set id for productTemplateElements
			List<ProductTemplateElement> productTemplateElements = productTemplate.getElements();
			for(ProductTemplateElement productTemplateElement: productTemplateElements) {
				productTemplateElement.setId(nextSequenceService.getNextSequence("productTemplateELementSequences"));
			}
			
			return productTemplateService.addProductTemplate(productTemplate);
		}catch(ServiceException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "vendor/{vendorId}/display-templates/", method = RequestMethod.POST)
	public DisplayTemplate addDisplayTemplate(@RequestBody DisplayTemplate displayTemplate) {
		
		try {
			//set id for displayTemplate
			displayTemplate.setId(nextSequenceService.getNextSequence("displayTemplateSequences"));
			
			//set id for displayTemplateElements
			List<DisplayTemplateElement> displayTemplateElements = displayTemplate.getElements();
			for(DisplayTemplateElement displayTemplateElement: displayTemplateElements) {
				displayTemplateElement.setId(nextSequenceService.getNextSequence("displayTemplateELementSequences"));
			}
			
			//set id for layouts
			/*List<Layout> layouts = displayTemplate.getLayoutElements();
			for(Layout layout: layouts) {
				layout.setId(nextSequenceService.getNextSequence("layoutSequences"));
			}*/
			
			return displayTemplateService.addDisplayTemplate(displayTemplate);
		}catch(ServiceException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "vendor/{vendorId}/bookmark-templates/", method = RequestMethod.POST)
	public BookmarkTemplate addBookMarkTemplate(@RequestBody BookmarkTemplate bookmarkTemplate) {
		
		try {
			//set id for bookmarkTemplate
			bookmarkTemplate.setId(nextSequenceService.getNextSequence("bookmarkTemplateSequences"));
			
			//set id for bookmarkTemplateElements
			List<BookmarkTemplateElement> bookmarkTemplateElements = bookmarkTemplate.getElements();
			for(BookmarkTemplateElement bookmarkTemplateElement: bookmarkTemplateElements) {
				bookmarkTemplateElement.setId(nextSequenceService.getNextSequence("bookmarkTemplateElementSequence"));
			}
			
			return bookMarkTemplateService.addBookmarkTemplate(bookmarkTemplate);
		}catch(ServiceException ex) {
			logger.error(ex.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "vendor/{vendorId}/templates", method = RequestMethod.GET)
	public List<DisplayTemplate> listDisplayTemplate(@PathVariable Integer vendorId){
		return displayTemplateService.getByVendorId(vendorId);
	}
	
	@RequestMapping(value = "vendor/{vendorId}/display-template/{type}", method = RequestMethod.GET)
	public DisplayTemplate displayTemplateByType(@PathVariable Integer vendorId, @PathVariable String type){
		return displayTemplateService.getByVendorIdAndName(vendorId, type);
	}
	
	@RequestMapping(value = "vendor/{vendorId}/product-template/{type}", method = RequestMethod.GET)
	public ProductTemplate productTemplateByType(@PathVariable Integer vendorId, @PathVariable String type){
		return productTemplateService.getByVendorIdAndName(vendorId, type);
	}
	
	@RequestMapping(value = "vendor/{vendorId}/bookmark-template/{type}", method = RequestMethod.GET)
	public BookmarkTemplate bookmarkTemplateByType(@PathVariable Integer vendorId, @PathVariable String type){
		return bookMarkTemplateService.getByVendorIdAndName(vendorId, type);
	}
	
	@RequestMapping(value = "vendor/{vendorId}/display-template/{type}", method = RequestMethod.DELETE)
	public void deleteDisplayTemplateByType(@PathVariable Integer vendorId, @PathVariable String type){
		System.out.println(displayTemplateService.getByVendorIdAndName(vendorId, type));
		displayTemplateService.deleteTemplate(displayTemplateService.getByVendorIdAndName(vendorId, type));
	}
	
	@RequestMapping(value = "vendor/{vendorId}/product-template/{type}", method = RequestMethod.DELETE)
	public void deleteProductTemplateByType(@PathVariable Integer vendorId, @PathVariable String type){
		productTemplateService.deleteTemplate(productTemplateService.getByVendorIdAndName(vendorId, type));
	}
	
	@RequestMapping(value = "vendor/{vendorId}/bookmark-template/{type}", method = RequestMethod.DELETE)
	public void deleteBookmarkTemplateByType(@PathVariable Integer vendorId, @PathVariable String type){
		bookMarkTemplateService.deleteTemplate(bookMarkTemplateService.getByVendorIdAndName(vendorId, type));
	}
	
	@RequestMapping(value = "vendor/{vendorId}/{product}/{productId}", method = RequestMethod.GET)
	public DisplayInfo getProduct(@PathVariable int vendorId, @PathVariable String product, @PathVariable int productId){
		RestTemplate restTemplate = new RestTemplate();
		 
        //find vendor by id from url
		Vendor vendor = null;
		DisplayTemplate displayTemplate = null;
		try {
			vendor = vendorService.getVendor(vendorId);
			displayTemplate = displayTemplateService.getByVendorIdAndName(vendorId, product);
		}catch(ServiceException e) {
			logger.error(e.getMessage());
			return null;
		}
		
		//send the REST request by vendor url + productId
        String result = restTemplate.getForObject(vendor.getUrl() + "/" + product + "/" + productId, String.class);
        
        //create JSONObject from String
        JSONParser parser = new JSONParser();
        JSONObject myProduct = null;
        
        try {
        	myProduct = (JSONObject) parser.parse(result);
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}
        
        for (Object key : myProduct.keySet()) {
            //based on you key types
            String keyStr = (String)key;
            Object keyvalue = myProduct.get(keyStr);

            //Print key and value
            System.out.println("key: "+ keyStr + " value: " + keyvalue);

        }
        
        //list of displayInfoElements
        List<DisplayInfoElement> displayInfoElements = new ArrayList<DisplayInfoElement>();
        
        //iterate displayTemplate`s elements list
        List<DisplayTemplateElement> displayTemplateElements = displayTemplate.getElements();
        
        for(int i = 0; i < displayTemplateElements.size(); i++) {
        	System.out.println("ProductField:");
        	DisplayTemplateElement displayTemplateElement = displayTemplateElements.get(i);		//i-th displayTemplateElement
        	System.out.println(displayTemplateElement.getProductTemplateElement().getSource());		//i-th displayTemplateElement`s ProductTemplateElement`s source (database)
        	System.out.println(myProduct.get(displayTemplateElement.getProductTemplateElement().getSource()));	//myProduct (ProductInfo) corresponding field by name
        	
    		if(displayTemplateElement.getType().equals("NumberField")) {
        		DisplayTemplateNumberField displayTemplateNumberField = (DisplayTemplateNumberField) displayTemplateElements.get(i);
        		Number value = splitObject(myProduct, displayTemplateElement.getProductTemplateElement().getSource(), Number.class);
        		System.out.println("value: " + value);
        		//if the value is null don't create a display element for it
        		if(value != null) {
	           		displayInfoElements.add(new DisplayInfoNumberElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateElement.getParentLayoutId(), displayTemplateElement.getParentLayoutParams(), value, displayTemplateNumberField.getKeyFontSize(), displayTemplateNumberField.getKeyFontFamily(), displayTemplateNumberField.getValueFontSize(), displayTemplateNumberField.getValueFontFamily(), displayTemplateNumberField.getKeyColor(), displayTemplateNumberField.getValueColor()));
        		}else {
        			System.out.println("It was a null field.");
        		}
         	}else {
        		if(displayTemplateElement.getType().equals("TextField") ) {
        			DisplayTemplateTextField displayTemplateTextField = (DisplayTemplateTextField) displayTemplateElements.get(i);
            		String value = splitObject(myProduct, displayTemplateElement.getProductTemplateElement().getSource(),String.class);
            		System.out.println("value: " + value);
            		//if the value is null or an empty string don't create a display element for it
            		if(value != null && !value.equals("")) {
            			displayInfoElements.add(new DisplayInfoTextElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateElement.getParentLayoutId(), displayTemplateElement.getParentLayoutParams(), value, displayTemplateTextField.getKeyFontSize(), displayTemplateTextField.getKeyFontFamily(), displayTemplateTextField.getValueFontSize(), displayTemplateTextField.getValueFontFamily(), displayTemplateTextField.getKeyColor(), displayTemplateTextField.getValueColor()));
            		}else {
            			System.out.println("It was a null field.");
            		}
        		}else {
        			if(displayTemplateElement.getType().equals("ImageField")) {
        				DisplayTemplateImageField displayTemplateImageField = (DisplayTemplateImageField) displayTemplateElements.get(i);
                		String value = splitObject(myProduct, displayTemplateElement.getProductTemplateElement().getSource(), String.class);
                		System.out.println("value: " + value);
                		//if the value is null or an empty string don't create a display element for it
                		if(value != null && !value.equals(" ")) {
                			displayInfoElements.add(new DisplayInfoImageElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateImageField.getParentLayoutId(), displayTemplateImageField.getParentLayoutParams(), displayTemplateImageField.getWidth(), displayTemplateImageField.getHeight(), displayTemplateImageField.getKeyFontSize(), displayTemplateImageField.getKeyFontFamily(), value, displayTemplateImageField.getKeyColor()));
					    }else {
                			System.out.println("It was a null field.");
                		}
							
        			}else {
        				if(displayTemplateElement.getType().equals("ImageList")) {
        					DisplayTemplateImageList displayTemplateImageList = (DisplayTemplateImageList) displayTemplateElements.get(i);
        					String[] constraints = displayTemplateElement.getProductTemplateElement().getSource().split("\\s+");
        					JSONArray images = (JSONArray) myProduct.get(constraints[0]);
                    		List<String> values = new ArrayList<>();
                    		
                    		for(int j=0; j<images.size(); j++) {
                    			JSONObject imagePath = (JSONObject)images.get(j);
								values.add("vendor/"+ vendorId + "/" + product + "/" + productId + "/" + (String)imagePath.get(constraints[1]));
								
                    		}
                    		System.out.println("value: " + values);
                    		//if the values is an empty list don't create a display element for it
                    		if(!values.isEmpty()) {
                    			displayInfoElements.add(new DisplayInfoImageListElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateImageList.getParentLayoutId(), displayTemplateImageList.getParentLayoutParams(), displayTemplateImageList.getWidth(), displayTemplateImageList.getHeight(), displayTemplateImageList.getKeyFontSize(), displayTemplateImageList.getKeyFontFamily(), values, displayTemplateImageList.getKeyColor()));
                    		}else {
                    			System.out.println("It was an empty array.");
                    		}
         				}else {
        					if(displayTemplateElement.getType().equals("FloatField")) {
        		        		DisplayTemplateFloatField displayTemplateFloatField = (DisplayTemplateFloatField) displayTemplateElements.get(i);
        		        		Number value = splitObject(myProduct, displayTemplateElement.getProductTemplateElement().getSource(), Number.class);
        		        		System.out.println("value: " + value);
        		        		//if the value is null don't create a display element for it
        		        		if(value != null) {
        		        			displayInfoElements.add(new DisplayInfoFloatElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateElement.getParentLayoutId(), displayTemplateElement.getParentLayoutParams(), value, displayTemplateFloatField.getKeyFontSize(), displayTemplateFloatField.getKeyFontFamily(), displayTemplateFloatField.getKeyFontSize(), displayTemplateFloatField.getKeyFontFamily(), displayTemplateFloatField.getKeyColor(), displayTemplateFloatField.getValueColor()));
        		        		}else {
        		        			System.out.println("It was a null field.");
        		        		}
        		        	}else {
        		        		if(displayTemplateElement.getType().equals("BooleanField")) {
        		        			DisplayTemplateBooleanField displayTemplateBooleanField = (DisplayTemplateBooleanField) displayTemplateElements.get(i);
            		        		Boolean value = splitObject(myProduct, displayTemplateElement.getProductTemplateElement().getSource(), Boolean.class);
            		        		System.out.println("value: " + value);
            		        		//if the value is null don't create a display element for it
            		        		if(value != null) {
            		        			displayInfoElements.add(new DisplayInfoBooleanElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateElement.getParentLayoutId(), displayTemplateElement.getParentLayoutParams(), value, displayTemplateBooleanField.getKeyFontSize(), displayTemplateBooleanField.getKeyFontFamily(), displayTemplateBooleanField.getKeyColor()));
            		        		}else {
            		        			System.out.println("It was a null field.");
            		        		}
            		        	}else {
            		        		if(displayTemplateElement.getType().equals("WebDisplay")) {
            		        			DisplayTemplateWebDisplay displayTemplateWebDisplay = (DisplayTemplateWebDisplay) displayTemplateElements.get(i);
            		            		String value = splitObject(myProduct, displayTemplateElement.getProductTemplateElement().getSource(),String.class);
            		            		System.out.println("value: " + value);
            		            		//if the value is null or an empty string don't create a display element for it
            		            		if(value != null && !value.equals("")) {
            		            			displayInfoElements.add(new DisplayInfoWebDisplayElement(displayTemplateElement.getName(), displayTemplateElement.getType(), displayTemplateElement.getParentLayoutId(), displayTemplateElement.getParentLayoutParams(), value, displayTemplateWebDisplay.getKeyFontSize(), displayTemplateWebDisplay.getKeyFontFamily(), displayTemplateWebDisplay.getKeyColor()));
            		            		}else {
            		            			System.out.println("It was a null field.");
            		            		}
            		        		}else {
            		        			System.out.println("Error!");
            		        		}
            		        	}
        		        		
        		        	}
        				}
        			}
        		}
        	}
        }
        
        DisplayInfo displayInfo = new DisplayInfo(displayTemplate.getId(), displayTemplate.getName(), displayInfoElements, displayTemplate.getLayoutElements());
        
        return displayInfo;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T splitObject(JSONObject jsonObject, String str, Class<T> clazz) {
		String[] splited = str.split("\\s+");
		for(int i = 0; i < splited.length - 1; i++) {
			if(jsonObject.containsKey(splited[i])) {
				jsonObject = getByKey(jsonObject, (splited[i]));
			}else {
				return null;
			}
		}
		if(jsonObject.containsKey(splited[splited.length-1])) {
			return (T) jsonObject.get(splited[splited.length-1]);
		}else {
			return null;
		}
	}
	
	public JSONObject getByKey(JSONObject jsonObject, String key) {
		return (JSONObject) jsonObject.get(key);
	}
	
}
