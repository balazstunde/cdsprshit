package edu.codespring.merx.server.backend.model;

public class DisplayInfoWebDisplayElement extends DisplayInfoElement {

	private String value;
	private Number keyFontSize;
	private String keyFontFamily;
	private String keyColor;
	
	public DisplayInfoWebDisplayElement() {
		super();
	}
	
	public DisplayInfoWebDisplayElement(String name, String type, Integer parentLayoutId,
			LayoutParams parentLayoutParams, String value, Number keyFontSize, String keyFontFamily, String keyColor) {
		super(name, type, parentLayoutId, parentLayoutParams);
		this.value = value;
		this.keyFontSize = keyFontSize;
		this.keyFontFamily = keyFontFamily;
		this.keyColor = keyColor;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Number getKeyFontSize() {
		return keyFontSize;
	}

	public void setKeyFontSize(Number keyFontSize) {
		this.keyFontSize = keyFontSize;
	}

	public String getKeyFontFamily() {
		return keyFontFamily;
	}

	public void setKeyFontFamily(String keyFontFamily) {
		this.keyFontFamily = keyFontFamily;
	}

	public String getKeyColor() {
		return keyColor;
	}

	public void setKeyColor(String keyColor) {
		this.keyColor = keyColor;
	}
	
}
