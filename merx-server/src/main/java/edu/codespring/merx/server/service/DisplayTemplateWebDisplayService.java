package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.DisplayTemplateWebDisplay;
import edu.codespring.merx.server.backend.repository.DisplayTemplateWebDisplayRepository;

@Service
public class DisplayTemplateWebDisplayService {

	@Autowired
	private DisplayTemplateWebDisplayRepository displayTemplateWebDisplayRepository;
	
	public DisplayTemplateWebDisplay getDisplayTemplateWebDisplay(int id) {
		return displayTemplateWebDisplayRepository.findById(id).get();
	}
	
	public DisplayTemplateWebDisplay addDisplayTemplateWebDisplay(DisplayTemplateWebDisplay displayTemplateWebDisplay) {
		return displayTemplateWebDisplayRepository.save(displayTemplateWebDisplay);
	}
	
	public List<DisplayTemplateWebDisplay> getAllDisplayTemplateWebDisplays() {
		List<DisplayTemplateWebDisplay> displayTemplateWebDisplays = new ArrayList<>();
		displayTemplateWebDisplayRepository.findAll().forEach(displayTemplateWebDisplays::add);
		
		return displayTemplateWebDisplays;
	}
}
