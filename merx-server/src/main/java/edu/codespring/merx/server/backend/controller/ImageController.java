package edu.codespring.merx.server.backend.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.backend.repository.VendorRepository;

@Controller
public class ImageController {
	
	final static Logger logger = LoggerFactory.getLogger(ImageController.class);
	
	@Autowired
	private VendorRepository vendorRepository;
	
	//image with folder {images}
	@RequestMapping(value="vendor/{vendorId}/{product}/{productId}/{images}/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getImage(@PathVariable Integer vendorId, @PathVariable String product, @PathVariable Integer productId, @PathVariable String images, @PathVariable String imageId) {
		
		RestTemplate restTemplate = new RestTemplate();
		
		Vendor vendor = vendorRepository.findById(vendorId).get();
	    ResponseEntity<byte[]> responseEntity = restTemplate.getForEntity(vendor.getUrl()+ "/" + product + "/" + productId + "/" + images + "/" + imageId, byte[].class);
	    logger.info(vendor.getUrl()+ "/" + product + "/" + productId + "/" + images + "/" + imageId);
	    return responseEntity;
	}
	
	//image without folder (like Simo) add /images to url to be general
	@RequestMapping(value="vendor/{vendorId}/{product}/{productId}/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getImage1(@PathVariable Integer vendorId, @PathVariable String product, @PathVariable Integer productId, @PathVariable String imageId) {
		
		RestTemplate restTemplate = new RestTemplate();
		
		Vendor vendor = vendorRepository.findById(vendorId).get();
	    ResponseEntity<byte[]> responseEntity = restTemplate.getForEntity(vendor.getUrl()+ "/" + product + "/" + productId + "/images/" + imageId, byte[].class);
	    logger.info(vendor.getUrl()+ "/" + product + "/" + productId + "/images/" + imageId);
	    return responseEntity;
	}
	
	//image with folder {images}
	@RequestMapping(value="vendorimages/{imageId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public ResponseEntity<byte[]> getVendorImage(@PathVariable String imageId) {
		
		File serverFile = new File("C:/vendorimages/" + imageId);
	    byte[] bytes = null;
		try {
			bytes = Files.readAllBytes(serverFile.toPath());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
	}
	
	@RequestMapping(value = "/vendors/{vendorId}/file", method = RequestMethod.POST)
	@ResponseBody
	public String saveImage(@RequestParam MultipartFile file, @PathVariable Integer vendorId) {
		FileOutputStream imageOutFile = null;
		try {
			imageOutFile = new FileOutputStream("C:/vendorimages/" + vendorId);
		} catch (FileNotFoundException e1) {
			logger.error(e1.getMessage());
		}
		try {
			imageOutFile.write(file.getBytes());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return vendorId + ".jpg";
	}
}
