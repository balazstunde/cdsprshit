package edu.codespring.merx.server.backend.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Vendors")
public class Vendor {
	
	@Id
	private Integer id;
	private String name;
	private String username;
	private String url;
	private String image;
	private List<Address> addresses;
	
	public Vendor() {
		
	}
	
	public Vendor(Integer id, String name, String username, String url, String image, List<Address> addresses) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.url = url;
		this.image = image;
		this.addresses = addresses;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Address> getAddresses() {
		return addresses;
	}
	
	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
