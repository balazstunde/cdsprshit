package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.backend.repository.VendorRepository;

@Service
public class VendorService {
	
	@Autowired
	private VendorRepository vendorRepository;
	
	public Vendor getVendor(int id) {
		return vendorRepository.findById(id).get();
	}
	
	public Vendor addVendor(Vendor vendor) {
		return vendorRepository.save(vendor);
	}
	
	public List<Vendor> getAllVendors() {
		List<Vendor> vendors = new ArrayList<>();
		vendorRepository.findAll().forEach(vendors::add);
		
		return vendors;
	}
	
	public void deleteVendor(Vendor vendor) {
		vendorRepository.delete(vendor);
	}

	public Vendor getByUsername(String username) {
		return vendorRepository.findByUsername(username);
	}
}
