package edu.codespring.merx.server.backend.model;

public class DisplayInfoTextElement extends DisplayInfoElement {
	
	private String value;
	private Number keyFontSize;
	private String keyFontFamily;
	private Number valueFontSize;
	private String valueFontFamily;
	private String keyColor;
	private String valueColor;
	
	public DisplayInfoTextElement() {
		
	}

	public DisplayInfoTextElement(String name, String type, Integer parentLayoutId, LayoutParams parentLayoutParams,
			String value, Number keyFontSize, String keyFontFamily, Number valueFontSize, String valueFontFamily,
			String keyColor, String valueColor) {
		super(name, type, parentLayoutId, parentLayoutParams);
		this.value = value;
		this.keyFontSize = keyFontSize;
		this.keyFontFamily = keyFontFamily;
		this.valueFontSize = valueFontSize;
		this.valueFontFamily = valueFontFamily;
		this.keyColor = keyColor;
		this.valueColor = valueColor;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Number getKeyFontSize() {
		return keyFontSize;
	}

	public void setKeyFontSize(Number keyFontSize) {
		this.keyFontSize = keyFontSize;
	}

	public String getKeyFontFamily() {
		return keyFontFamily;
	}

	public void setKeyFontFamily(String keyFontFamily) {
		this.keyFontFamily = keyFontFamily;
	}

	public Number getValueFontSize() {
		return valueFontSize;
	}

	public void setValueFontSize(Number valueFontSize) {
		this.valueFontSize = valueFontSize;
	}

	public String getValueFontFamily() {
		return valueFontFamily;
	}

	public void setValueFontFamily(String valueFontFamily) {
		this.valueFontFamily = valueFontFamily;
	}

	public String getKeyColor() {
		return keyColor;
	}

	public void setKeyColor(String keyColor) {
		this.keyColor = keyColor;
	}

	public String getValueColor() {
		return valueColor;
	}

	public void setValueColor(String valueColor) {
		this.valueColor = valueColor;
	}

}
