package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.ProductTemplateElement;
import edu.codespring.merx.server.backend.repository.ProductTemplateElementRepository;

@Service
public class ProductTemplateElementService {
	@Autowired
	private ProductTemplateElementRepository productTemplateElementRepository;
	
	public ProductTemplateElement getProductTemplateElement(int id) {
		return productTemplateElementRepository.findById(id).get();
	}
	
	public ProductTemplateElement addProductTemplateElement(ProductTemplateElement productTemplateElement) {
		return productTemplateElementRepository.save(productTemplateElement);
	}
	
	public List<ProductTemplateElement> getAllProductTemplateElements() {
		List<ProductTemplateElement> productTemplateElements = new ArrayList<>();
		productTemplateElementRepository.findAll().forEach(productTemplateElements::add);
		
		return productTemplateElements;
	}
	
	public ProductTemplateElement getByName(String name) {
		return productTemplateElementRepository.findByName(name);
	}
}
