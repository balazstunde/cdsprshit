package edu.codespring.merx.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.BookmarkTemplateElement;
import edu.codespring.merx.server.backend.repository.BookmarkTemplateElementRepository;

@Service
public class BookmarkTemplateElementService {

	@Autowired
	private BookmarkTemplateElementRepository bookmarkTemplateElementRepository;
	
	public BookmarkTemplateElement getBookmarkTemplateElement(int id) {
		return bookmarkTemplateElementRepository.findById(id).get();
	}
	
	public BookmarkTemplateElement addBookmarkTemplateElement(BookmarkTemplateElement bookmarkTemplateElement) {
		return bookmarkTemplateElementRepository.save(bookmarkTemplateElement);
	}
}
