package edu.codespring.merx.server.backend.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "BookmarkTemplateElements")
public class BookmarkTemplateElement {

	@Id
	private Integer id;
	private String name;
	private String source;
	
	public BookmarkTemplateElement() {
		
	}

	public BookmarkTemplateElement(Integer id, String name, String source) {
		super();
		this.id = id;
		this.name = name;
		this.source = source;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
