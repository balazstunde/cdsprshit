package edu.codespring.merx.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.BookmarkTemplate;
import edu.codespring.merx.server.backend.repository.BookmarkTemplateRepository;

@Service
public class BookmarkTemplateService {
	
	final static Logger logger = LoggerFactory.getLogger(BookmarkTemplateService.class);
	
	@Autowired
	private BookmarkTemplateRepository bookmarkTemplateRepository;
	
	public BookmarkTemplate getBookmarkTemplate(int id) {
		try {
			return bookmarkTemplateRepository.findById(id).get();
		}catch(DataAccessException ex) {
			logger.error("Exception in get bookmark template.");
			throw new ServiceException("Exception in get bookmark template.", ex);
		}
		
	}
	
	public BookmarkTemplate addBookmarkTemplate(BookmarkTemplate bookmarkTemplate) {
		try {
			return bookmarkTemplateRepository.save(bookmarkTemplate);
		}catch(DataAccessException ex) {
			logger.error("Exception in add bookmark template.");
			throw new ServiceException("Exception in add bookmark template.", ex);
		}
	}
	
	public BookmarkTemplate getByVendorIdAndName(Integer id, String name) {
		try {
			return bookmarkTemplateRepository.findByVendorIdAndName(id, name);
		}catch(DataAccessException ex) {
			logger.error("Exception in get by vendor id and name bookmark template.");
			throw new ServiceException("Exception in get by vendor id and name bookmark template.", ex);
		}
	}
	
	public void deleteTemplate(BookmarkTemplate bookmarkTemplate) {
		try {
			bookmarkTemplateRepository.delete(bookmarkTemplate);
		}catch(DataAccessException ex) {
			logger.error("Exception in get by vendor id and name bookmark template.");
			throw new ServiceException("Exception in get by vendor id and name bookmark template.", ex);
		}
	}
}
