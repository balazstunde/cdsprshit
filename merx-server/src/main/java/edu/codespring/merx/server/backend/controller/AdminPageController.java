package edu.codespring.merx.server.backend.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import edu.codespring.merx.server.backend.model.BookmarkTemplate;
import edu.codespring.merx.server.backend.model.BookmarkTemplateElement;
import edu.codespring.merx.server.backend.model.DisplayTemplate;
import edu.codespring.merx.server.backend.model.ProductBookmark;
import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.backend.model.VendorBookmark;
import edu.codespring.merx.server.service.BookmarkTemplateService;
import edu.codespring.merx.server.service.DisplayTemplateService;
import edu.codespring.merx.server.service.VendorService;
import org.json.simple.JSONArray;

@Controller
public class AdminPageController {
	@Autowired
	private VendorService vendorService;
	@Autowired
	private DisplayTemplateService displayTemplateService;
	@Autowired
	private BookmarkTemplateService bookmarkTemplateService;
	
	@RequestMapping("/")
	@ResponseBody
	public String hello() {
		return "Hello!";
	}

	@RequestMapping("/vendors")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<VendorBookmark> getVendors(HttpServletRequest req) {
		
		List<Vendor>allVendors= vendorService.getAllVendors();
		List<VendorBookmark> vendorBookmarks = new LinkedList<>();
		for(Vendor vendor : allVendors) {
			VendorBookmark vendorBookmark = new VendorBookmark(vendor.getId(),vendor.getName(), vendor.getImage());
			vendorBookmarks.add(vendorBookmark);
		}
		return vendorBookmarks;
	}
	
	@RequestMapping("/vendors/{vendorId}/products")
	@ResponseBody
	//public List<ProductBookmark> getProducts( @PathVariable int vendorId, HttpServletResponse response, HttpServletRequest request){
	public List<ProductBookmark> getProducts( @PathVariable int vendorId){
			
		List<ProductBookmark> allProductBookmarks = new LinkedList<>();
		Vendor vendor = vendorService.getVendor(vendorId);
		//BookmarkTemplate bookmarkTemplate = bookmarkTemplateService.getByVendorIdAndName(vendorId, name);
		List<DisplayTemplate> allTemplates = displayTemplateService.getAllDisplayTemplates();
		RestTemplate restTemplate = new RestTemplate();
		List<String> result = new ArrayList<String>();
		List<String> resultName = new ArrayList<String>();
		String productName="";
		
		//kivalasztom a templatek kozul a jelenlegi vendor templatejeit es a termeknek megfelelo objektumokat lekerem, pl.: locahlhost:8081/car
		for(DisplayTemplate template : allTemplates){
			if(template.getVendorId().equals(vendorId)) {
				System.out.println(template.getName());
				result.add(restTemplate.getForObject(vendor.getUrl() + "/" + template.getName(), String.class));
				resultName.add(template.getName());	//a neve is legyen meg egy masik tombben
				//productName=template.getName();
			}
		}
		
		 JSONParser parser = new JSONParser();
	     JSONArray myProduct = null;
	     JSONObject productObject = null;
	     
	     for(int i = 0; i < result.size(); i++) {
	    	 productName = resultName.get(i);
	    	 try {
	 			myProduct = (JSONArray) parser.parse(result.get(i));
	 			System.out.println("myProduct: " + myProduct);
	 	     } catch (ParseException e) {
	 				e.printStackTrace();
	 	     }
	 		     
	 		 for(int element = 0; element < myProduct.size(); ++element) {
	 			try {
	 				productObject = (JSONObject) parser.parse(myProduct.get(element).toString());
	 				System.out.println("Element " + element);
	 				System.out.println("ProductObject " + myProduct.get(element).toString());
	 			} catch (ParseException e) {
	 				e.printStackTrace();
	 			}		   

	 			System.out.println("BookmarkTemplate: " + vendorId + " " + productName);
	 			BookmarkTemplate bookmarkTemplate = bookmarkTemplateService.getByVendorIdAndName(vendorId, productName);
	 			
	 			
	 			String bookmarkString = "";
	 			List<BookmarkTemplateElement> bookmarkTemplateElements = bookmarkTemplate.getElements();
	 			Integer bookmarkElementsSize = bookmarkTemplateElements.size();
	 			System.out.println("bookmarkElementsSize " + bookmarkElementsSize);
	 			for(int j = 0; j < bookmarkElementsSize; j++) {
	 				System.out.println("Name: " + bookmarkTemplateElements.get(j).getSource());
	 				bookmarkString += splitObject(productObject, bookmarkTemplateElements.get(j).getSource(), String.class) + "_";
	 				System.out.println("bookmarkString: " + bookmarkString);
	 			}
	 			bookmarkString = bookmarkString.replaceAll("\\s+","_");
	 			System.out.println("bookmarkString: " + bookmarkString);
	 			
	 			allProductBookmarks.add(new ProductBookmark(splitObject(productObject, "id", Long.class), bookmarkString, productName, ""));
	 	   	}	  
	     }
		
	    //request.setAttribute("vendorId", vendorId);
		//request.setAttribute("products", allProductBookmarks);
		//return "products";
		return allProductBookmarks;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T splitObject(JSONObject jsonObject, String str, Class<T> clazz) {
		String[] splited = str.split("\\s+");
		for(int i = 0; i < splited.length - 1; i++) {
			if(jsonObject.containsKey(splited[i])) {
				jsonObject = getByKey(jsonObject, (splited[i]));
			}else {
				return (T)"";
			}
		}
		if(jsonObject.containsKey(splited[splited.length-1])) {
			return (T) jsonObject.get(splited[splited.length-1]);
		}else {
			return (T)"";
		}
	}
	
	public JSONObject getByKey(JSONObject jsonObject, String key) {
		return (JSONObject) jsonObject.get(key);
	}
}
