package edu.codespring.merx.server.backend.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DisplayTemplates")
public class DisplayTemplate {

	@Id
	private Integer id;
	private String name;
	private Integer vendorId;
	private List<DisplayTemplateElement> elements;
	private List<Layout> layoutElements;
	
	public DisplayTemplate() {
		
	}

	public DisplayTemplate(Integer id, String name, Integer vendorId, List<DisplayTemplateElement> elements,
			List<Layout> layoutElements) {
		super();
		this.id = id;
		this.name = name;
		this.vendorId = vendorId;
		this.elements = elements;
		this.layoutElements = layoutElements;
	}

	public List<Layout> getLayoutElements() {
		return layoutElements;
	}

	public void setLayoutElements(List<Layout> layoutElements) {
		this.layoutElements = layoutElements;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DisplayTemplateElement> getElements() {
		return elements;
	}

	public void setElements(List<DisplayTemplateElement> elements) {
		this.elements = elements;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}	
	
}
