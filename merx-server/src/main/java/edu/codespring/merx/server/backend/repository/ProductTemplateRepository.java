package edu.codespring.merx.server.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.ProductTemplate;

@Repository
public interface ProductTemplateRepository extends MongoRepository<ProductTemplate, Integer>{
		public ProductTemplate findByName(String name); 
		public ProductTemplate findByVendorIdAndName(Integer id, String name);
}
