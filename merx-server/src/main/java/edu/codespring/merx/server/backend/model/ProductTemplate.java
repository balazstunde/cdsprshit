package edu.codespring.merx.server.backend.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProductTemplates")
public class ProductTemplate {

	@Id
	private Integer id;
	private Integer vendorId;
	private String name;
	private List<ProductTemplateElement> elements;
	
	public ProductTemplate() {
		
	}
	
	public ProductTemplate(Integer id, Integer vendorId, String name, List<ProductTemplateElement> elements) {
		super();
		this.id = id;
		this.vendorId = vendorId;
		this.name = name;
		this.elements = elements;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ProductTemplateElement> getElements() {
		return elements;
	}
	public void setElements(List<ProductTemplateElement> elements) {
		this.elements = elements;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	
}
