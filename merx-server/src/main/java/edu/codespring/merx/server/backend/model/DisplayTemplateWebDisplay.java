package edu.codespring.merx.server.backend.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DisplayTemplateWebDisplays")
public class DisplayTemplateWebDisplay extends DisplayTemplateElement {
	
	public DisplayTemplateWebDisplay() {
		
	}
	
	public DisplayTemplateWebDisplay(Integer id, ProductTemplateElement productTemplateElement,
			LayoutParams parentLayoutParams, Integer parentLayoutId, String name, String type, Integer keyFontSize,
			String keyFontFamily, String keyColor) {
		super(id, productTemplateElement, parentLayoutParams, parentLayoutId, name, type, keyFontSize, keyFontFamily, keyColor);
		
	}
	
}
