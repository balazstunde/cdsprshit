package edu.codespring.merx.server.backend.model;

import java.util.List;
public class DisplayInfoImageListElement extends DisplayInfoElement{

	private Integer width;
	private Integer height;
	private Number keyFontSize;
	private String keyFontFamily;
	private List<String> value;
	private String keyColor;

	public DisplayInfoImageListElement() {
		
	}
	
	public DisplayInfoImageListElement(String name, String type, Integer parentLayoutId, LayoutParams parentLayoutParams,
		Integer width, Integer height, Number keyFontSize, String keyFontFamily, List<String> value, String keyColor) {
	super(name, type, parentLayoutId, parentLayoutParams);
	this.width = width;
	this.height = height;
	this.keyFontSize = keyFontSize;
	this.keyFontFamily = keyFontFamily;
	this.value = value;
	this.keyColor = keyColor;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Number getKeyFontSize() {
		return keyFontSize;
	}

	public void setKeyFontSize(Number keyFontSize) {
		this.keyFontSize = keyFontSize;
	}

	public String getKeyFontFamily() {
		return keyFontFamily;
	}

	public void setKeyFontFamily(String keyFontFamily) {
		this.keyFontFamily = keyFontFamily;
	}

	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}

	public String getKeyColor() {
		return keyColor;
	}

	public void setKeyColor(String keyColor) {
		this.keyColor = keyColor;
	}

}
