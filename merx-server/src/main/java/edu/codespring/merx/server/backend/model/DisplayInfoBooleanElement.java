package edu.codespring.merx.server.backend.model;

public class DisplayInfoBooleanElement extends DisplayInfoElement {
	
	private Boolean value;
	private Number keyFontSize;
	private String keyFontFamily;
	private String keyColor;

	public DisplayInfoBooleanElement() {
		
	}
	
	public DisplayInfoBooleanElement(String name, String type, Integer parentLayoutId, LayoutParams parentLayoutParams,
			Boolean value, Number keyFontSize, String keyFontFamily, String keyColor) {
		super(name, type, parentLayoutId, parentLayoutParams);
		this.value = value;
		this.keyFontSize = keyFontSize;
		this.keyFontFamily = keyFontFamily;
		this.keyColor = keyColor;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

	public Number getKeyFontSize() {
		return keyFontSize;
	}

	public void setKeyFontSize(Number keyFontSize) {
		this.keyFontSize = keyFontSize;
	}

	public String getKeyFontFamily() {
		return keyFontFamily;
	}

	public void setKeyFontFamily(String keyFontFamily) {
		this.keyFontFamily = keyFontFamily;
	}

	public String getKeyColor() {
		return keyColor;
	}

	public void setKeyColor(String keyColor) {
		this.keyColor = keyColor;
	}
	
}
