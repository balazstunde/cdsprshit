package edu.codespring.merx.server.backend.model;

public class GridLayoutParams extends LayoutParams{
	
	public GridLayoutParams() {
		
	}

	public GridLayoutParams(String parentLayoutName, int row, int column) {
		super(parentLayoutName, row, column);
	}

}
