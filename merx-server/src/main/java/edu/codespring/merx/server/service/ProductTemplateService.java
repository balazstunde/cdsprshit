package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.ProductTemplate;
import edu.codespring.merx.server.backend.repository.ProductTemplateRepository;

@Service
public class ProductTemplateService {
	
	final static Logger logger = LoggerFactory.getLogger(ProductTemplateService.class);
	
	@Autowired
	private ProductTemplateRepository productTemplateRepository;
	
	public ProductTemplate getProductTemplate(int id) {
		try {
			return productTemplateRepository.findById(id).get();
		}catch(DataAccessException ex) {
			logger.error("Exception in get product template.");
			throw new ServiceException("Exception in get product template.", ex);
		}
	}
	
	public ProductTemplate addProductTemplate(ProductTemplate productTemplate) {
		try {
			return productTemplateRepository.save(productTemplate);
		}catch(DataAccessException ex) {
			logger.error("Exception in add product template.");
			throw new ServiceException("Exception in add product template.", ex);
		}
	}
	
	public List<ProductTemplate> getAllProductTemplates() {
		try {
			List<ProductTemplate> productTemplates = new ArrayList<>();
			productTemplateRepository.findAll().forEach(productTemplates::add);
			
			return productTemplates;
		}catch(DataAccessException ex) {
			logger.error("Exception in get all product template.");
			throw new ServiceException("Exception in get all product template.", ex);
		}
	}
	
	public ProductTemplate getByVendorIdAndName(Integer id, String name) {
		try {
			return productTemplateRepository.findByVendorIdAndName(id, name);
		}catch(DataAccessException ex) {
			logger.error("Exception in get by vendor id and name product template.");
			throw new ServiceException("Exception in get by vendor id and name product template.", ex);
		}
	}
	
	public void deleteTemplate(ProductTemplate productTemplate) {
		try {
			productTemplateRepository.delete(productTemplate);
		}catch(DataAccessException ex) {
			logger.error("Exception in delete product template.");
			throw new ServiceException("Exception in delete product template.", ex);
		}
	}
}
