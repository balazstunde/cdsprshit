package edu.codespring.merx.server.backend.security;

import static edu.codespring.merx.server.backend.security.SecurityConstants.HEADER_STRING;
import static edu.codespring.merx.server.backend.security.SecurityConstants.SECRET;
import static edu.codespring.merx.server.backend.security.SecurityConstants.TOKEN_PREFIX;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import edu.codespring.merx.server.backend.model.MerxUser;
import edu.codespring.merx.server.service.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


public class JWTAuthorizationFilter extends BasicAuthenticationFilter  {
	
	private UserService userService;

	public JWTAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            String user = JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                    .build()
                    .verify(token.replace(TOKEN_PREFIX, ""))
                    .getSubject();

            if(userService==null){
                ServletContext servletContext = request.getServletContext();
                WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
                userService = webApplicationContext.getBean(UserService.class);
            }
            MerxUser merxUser = userService.getByUsername(user);
            
            if(merxUser != null) {
            	Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
    			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(merxUser.getRole().toString());
    			grantedAuthorities.add(grantedAuthority);
                
                if (user != null) {
                    return new UsernamePasswordAuthenticationToken(user, null, grantedAuthorities);
                }
            }
        }
        return null;
    }
}
