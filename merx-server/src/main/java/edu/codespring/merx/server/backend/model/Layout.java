package edu.codespring.merx.server.backend.model;

public class Layout {
	
	private Integer id;
	private String name;
	private Integer parentLayoutId;
	private LayoutParams parentLayoutParams;
	private String mode;
	private Integer row;
	private Integer column;
	
	public Layout() {
		
	}

	public Layout(String name, Integer id, Integer parentLayoutId, LayoutParams parentLayoutParams) {
		super();
		this.name = name;
		this.id = id;
		this.parentLayoutId = parentLayoutId;
		this.parentLayoutParams = parentLayoutParams;
	}
	
	public Layout(String name, Integer id, Integer parentLayoutId, LayoutParams parentLayoutParams, Integer row, Integer column) {
		super();
		this.name = name;
		this.id = id;
		this.parentLayoutId = parentLayoutId;
		this.parentLayoutParams = parentLayoutParams;
		this.row = row;
		this.column = column;
	}
	
	public Layout(String name, Integer id, Integer parentLayoutId, LayoutParams parentLayoutParams, String mode) {
		super();
		this.name = name;
		this.id = id;
		this.parentLayoutId = parentLayoutId;
		this.parentLayoutParams = parentLayoutParams;
		this.mode = mode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentLayoutId() {
		return parentLayoutId;
	}

	public void setParentLayoutId(Integer parentLayoutId) {
		this.parentLayoutId = parentLayoutId;
	}

	public LayoutParams getParentLayoutParams() {
		return parentLayoutParams;
	}

	public void setParentLayoutParams(LayoutParams parentLayoutParams) {
		this.parentLayoutParams = parentLayoutParams;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}
	
}
