package edu.codespring.merx.server.backend.model;

public class ProductBookmark {
	private long id;
	private String name;
	private String type;
	private String imageURL;
	
	public ProductBookmark() {
		
	}
	
	public ProductBookmark(long id, String name, String type, String imageURL) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.imageURL = imageURL;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	
	

}
