package edu.codespring.merx.server.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.codespring.merx.server.backend.model.MerxUser;
import edu.codespring.merx.server.backend.model.Vendor;
import edu.codespring.merx.server.service.NextSequenceService;
import edu.codespring.merx.server.backend.model.UserRole;
import edu.codespring.merx.server.service.UserService;
import edu.codespring.merx.server.service.VendorService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private VendorService vendorService;
	
	@Autowired
	private NextSequenceService nextSequenceService;
	
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	@ResponseBody
	public List<MerxUser> getAllUsers(){
		return userService.getAllUsers();
	}
	
	@RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
	@ResponseBody
	public UserRole getUser(@PathVariable String username){
		return userService.getByUsername(username).getRole();
	}

    @PostMapping("/users/sign-up")
	public String signUp(@RequestBody MerxUser user) {
		
		if (userService.getUser(user.getUsername()) != null) {
			return "sameEmail";
		}
		
		if (user.getPassword().isEmpty() || user.getPassword().length() < 6) {
			return "Password is not given!";
		}

		System.out.println(user.getRole());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userService.addUser(user);
		
		//for a signed up user create a vendor with username (relation between user and vendor)
		Vendor vendor = new Vendor();
		vendor.setUsername(user.getUsername());
		vendor.setId(nextSequenceService.getNextSequence("customSequences"));
		
		vendorService.addVendor(vendor);
		
		return "Succes";
	}
    
    @PostMapping("/vendors")
	public Vendor addVendor(@RequestBody Vendor vendor) {
		
		if (vendor == null) {
			return null;
		}
		
		vendor.setId(nextSequenceService.getNextSequence("customSequences"));
		return vendorService.addVendor(vendor);
		
	}
    
    @DeleteMapping("vendors/{vendorId}")
    public void deleteVendor(@PathVariable Integer vendorId) {
    	//delete a user (by username) and the related vendor??
    	//String username = vendorService.getVendor(vendorId).getUsername();
    	//MerxUser merxUser = userService.getByUsername(username);
    	//userService.deleteUser(merxUser);
    	Vendor vendor = vendorService.getVendor(vendorId);
    	vendorService.deleteVendor(vendor);
    }
    
    @DeleteMapping("users/{username}")
    public void deleteUser(@PathVariable String username) {
    	userService.deleteUser(userService.getByUsername(username));
    }
    
    @PutMapping("vendors/{vendorId}")
    public Vendor replaceVendor(@RequestBody Vendor vendor, @PathVariable Integer vendorId) {
    	Vendor dbVendor = vendorService.getVendor(vendorId);
    	if(dbVendor != null) {
    		// if the vendor with vendorId exists just replace
    		dbVendor.setImage(vendor.getImage());
    		dbVendor.setAddresses(vendor.getAddresses());
    		dbVendor.setName(vendor.getName());
    		dbVendor.setUrl(vendor.getUrl());
    		return vendorService.addVendor(dbVendor);
    	}else { 
    		//another add a new entity
    		vendor.setId(vendorId);
    		return vendorService.addVendor(vendor);
    	}
    }
    
    @PutMapping("/users")
    public MerxUser replaceUser(@RequestBody MerxUser user) {
    	MerxUser merxUser = userService.getUser(user.getUsername());
    	merxUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    	return userService.addUser(merxUser);
    }
    
}
