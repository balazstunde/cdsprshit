package edu.codespring.merx.server.backend.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import edu.codespring.merx.server.backend.model.DisplayTemplate;

@Repository
public interface DisplayTemplateRepository extends MongoRepository<DisplayTemplate, Integer>{
		public DisplayTemplate findByName(String name); 
		public DisplayTemplate findByVendorIdAndName(Integer id, String name);
		public List<DisplayTemplate> findByVendorId(Integer id);
}
