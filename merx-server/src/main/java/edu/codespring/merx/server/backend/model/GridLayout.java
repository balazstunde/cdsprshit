package edu.codespring.merx.server.backend.model;

public class GridLayout extends Layout {
	
	public GridLayout() {
		
	}

	public GridLayout(String name, Integer id, Integer parentLayoutId, LayoutParams parentLayoutParams, Integer row,
			Integer column) {
		super(name, id, parentLayoutId, parentLayoutParams, row, column);
	}
	
}
