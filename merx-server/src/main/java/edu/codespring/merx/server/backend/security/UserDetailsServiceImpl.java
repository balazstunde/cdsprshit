package edu.codespring.merx.server.backend.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.service.UserService;
import edu.codespring.merx.server.backend.model.MerxUser;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		MerxUser merxUser = userService.getByUsername(username);
		if (merxUser == null) {
			throw new UsernameNotFoundException(username);
		}

		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(merxUser.getRole().toString());
		grantedAuthorities.add(grantedAuthority);


		return new User(merxUser.getUsername(), merxUser.getPassword(), grantedAuthorities);
	}

}
