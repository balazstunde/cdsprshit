package edu.codespring.merx.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.codespring.merx.server.backend.model.DisplayTemplateImageField;
import edu.codespring.merx.server.backend.repository.DisplayTemplateImageFieldRepository;

@Service
public class DisplayTemplateImageFieldService {
	@Autowired
	private DisplayTemplateImageFieldRepository displayTemplateImageFieldRepository;
	
	public DisplayTemplateImageField getDisplayTemplateImageField(int id) {
		return displayTemplateImageFieldRepository.findById(id).get();
	}
	
	public DisplayTemplateImageField addDisplayTemplateImageField(DisplayTemplateImageField displayTemplateImageField) {
		return displayTemplateImageFieldRepository.save(displayTemplateImageField);
	}
	
	public List<DisplayTemplateImageField> getAllDisplayTemplateImageFields() {
		List<DisplayTemplateImageField> displayTemplateImageFields = new ArrayList<>();
		displayTemplateImageFieldRepository.findAll().forEach(displayTemplateImageFields::add);
		
		return displayTemplateImageFields;
	}
}
