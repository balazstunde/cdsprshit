<!DOCTYPE html> 
<%@ page contentType="text/html;charset=UTF-8" language="java" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>

<html> 
<head> 
<meta charset="ISO-8859-1"> 
<title>Vendors</title> 
</head> 
<body> 
	 <h1>Vendors</h1>
	<table>
		  <c:forEach items="${vendors}" var="vendor">
		    <tr>
		      <c:set var="uri" value="http://localhost:8080/vendor/${vendor.id}/products" />
		      <td><a href="${uri}")>${vendor.name}</a></td>
		      <td><img src = "http://localhost:8080/vendorimages/${vendor.imageUrl}" height = 80 width = 110></td>
		    </tr>
		  </c:forEach>
	</table>
</body>