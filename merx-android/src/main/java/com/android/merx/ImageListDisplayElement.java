package com.android.merx;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.squareup.picasso.Picasso;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ImageListDisplayElement extends DisplayElement implements Serializable {

    private List<String> value;
    private Integer width;
    private Integer height;

    public ImageListDisplayElement(String name, String type, String constraints, Integer layoutId,List<String> value,Integer width,Integer height) {
        super(name, type, constraints, layoutId);
        this.value=value;
        this.height=height;
        this.width=width;
    }

    @Override
    public List<View> displayElement(final Context context) {
        LinearLayout linearLayout=new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        final Map<View,Integer> imageViews= new HashMap<>();

        ImageView imageView;
        for (int i = 0; i < value.size(); i++) {
            imageView = new ImageView(context);
            imageView.setMaxHeight(height);
            imageView.setMaxWidth(width);
            Picasso.with(context)
                    .load(value.get(i))
                    .resize(width, height)
                    .into(imageView);
            imageViews.put(imageView,i);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Intent intent = new Intent(context, ImageSliderActivity.class);
                    intent.putExtra("images", (Serializable) value);
                    Log.e("INDEX",imageViews.get(view)+"");
                    intent.putExtra("index",imageViews.get(view));
                    context.startActivity(intent);
                }
            });

            linearLayout.addView(imageView);
            Log.e("IMAGE CREATED->>>>",value.get(i));
        }

        HorizontalScrollView scrollView = new HorizontalScrollView(context);
        scrollView.addView(linearLayout);

        List<View> result = new LinkedList<View>();
        result.add(scrollView);
        return result;
    }

    @Override
    public List<String> getValue() {
        return this.value;
    }
}
