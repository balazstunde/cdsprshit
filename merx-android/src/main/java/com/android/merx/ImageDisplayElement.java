package com.android.merx;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ImageDisplayElement extends DisplayElement implements Serializable {
    private String value;

    public ImageDisplayElement(String name, String type, String constraints, String value,Integer layoutId) {
        super(name, type, constraints,layoutId);
        this.value = value;
    }

    @Override
    public List<View> displayElement(Context context) {
        List<View> imageViews = new LinkedList<View>();
        ImageView imageView = new ImageView(context);
        imageView.setMaxHeight(100);
        imageView.setMaxWidth(100);
        Picasso.with(context)
                .load(value)
                .resize(100,100)
                .into(imageView);
        imageViews.add(imageView);
        return imageViews;
    }

    public String getValue() {
        return value;
    }
}