package com.android.merx;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.zxing.Result;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import static android.Manifest.permission.CAMERA;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView mScannerView;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private Button jsonButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     //   jsonButton = (Button) findViewById(R.id.json_button);

        mScannerView = findViewById(R.id.cameraView);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                Toast.makeText(getApplicationContext(), "Permission already granted", Toast.LENGTH_LONG).show();
            } else {
                requestPermission();
            }
        }

     /*   jsonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("SEVERE", "CLICKED");
                SharedPreferences prefs = MainActivity.this.getSharedPreferences(
                        "com.android.merx", Context.MODE_PRIVATE);
                Log.e("api_url DI OListener", prefs.getString("api_url", "Error!"));
                final Intent intent = new Intent(MainActivity.this, ShowProductActivity.class);
                DisplayInfo displayInfo = new DisplayInfo();
                intent.putExtra("views", (Serializable) displayInfo.getElements(MainActivity.this));
                intent.putExtra("layouts", (Serializable)displayInfo.getLayoutElements(MainActivity.this));
                intent.putExtra("url", "vendor/3/car/10");
                intent.putExtra("bookmark",(Serializable) displayInfo.getBookmark());
                displayInfo.jsonParser(MainActivity.this, intent);

            }
        });*/
        ImageView bookMark = findViewById(R.id.toBeBookmarked);

        bookMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,BookmarkActivity.class);
                startActivity(intent);
            }
        });

        ImageView hamburger = findViewById(R.id.hamburger);

        hamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ConfigurationActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean checkPermission() {
        return ( ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA ) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted){
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if(mScannerView == null) {
                    mScannerView = new ZXingScannerView(this);
                    setContentView(mScannerView);
                }
                mScannerView.setResultHandler(this);
                mScannerView.startCamera(camId);
            } else {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScannerView.stopCamera();
        mScannerView = null;
    }

    @Override
    public void handleResult(Result rawResult) {

        final String result = rawResult.getText();
        Log.e("QRCodeScanner", rawResult.getText());

        final Intent intent = new Intent(MainActivity.this, ShowProductActivity.class);
        DisplayInfo displayInfo = new DisplayInfo();
        try {
            JSONObject obj = new JSONObject(rawResult.getText());
            String url="vendor/";
            JSONObject vendor = obj.getJSONObject("Vendor");
            url+=vendor.getString("id")+"/";
            JSONObject product = obj.getJSONObject("Product");
            url+=product.getString("type")+"/";
            url+=product.getString("id");
            final SharedPreferences prefs = MainActivity.this.getSharedPreferences(
                    "com.android.merx", Context.MODE_PRIVATE);
            Bookmark bookmark = new Bookmark();
            bookmark.setName(product.getString("name"));
            bookmark.setVendor(vendor.getString("name"));
            bookmark.setImageUrl(product.getString("image_url"));
            bookmark.setProductUrl(prefs.getString("api_url", "http://10.0.2.2:8080")+"/"+url);
            intent.putExtra("views", (Serializable) displayInfo.getElements(MainActivity.this));
            intent.putExtra("layouts", (Serializable)displayInfo.getLayoutElements(MainActivity.this));
            intent.putExtra("url", url);
            intent.putExtra("bookmark",(Serializable) bookmark);
            Log.e("URL>>>>>>>>>>>>>>",bookmark.getProductUrl());
            displayInfo.jsonParser(MainActivity.this, intent);
        } catch (JSONException e) {
            Toast.makeText(MainActivity.this,"THE QR CODE IS INCORRECT",Toast.LENGTH_LONG).show();
            e.printStackTrace();
            onResume();
        }
        catch (NullPointerException e){
            Toast.makeText(MainActivity.this,"THE SERVER IS OFFLINE",Toast.LENGTH_LONG).show();
            onResume();
        }

    }

}