package com.android.merx;

import android.content.Context;
import android.view.View;

import java.io.Serializable;
import java.util.List;

public class BooleanDisplayElement  extends DisplayElement implements Serializable {
    public BooleanDisplayElement(String name, String type, String constraints, Integer layoutId) {
        super(name, type, constraints, layoutId);
    }

    @Override
    public List<View> displayElement(Context context) {
        return null;
    }

    @Override
    public Object getValue() {
        return null;
    }
}
