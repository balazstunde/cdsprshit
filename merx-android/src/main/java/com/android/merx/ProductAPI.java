package com.android.merx;

import com.google.gson.JsonElement;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ProductAPI {

    @GET()
    Call<JsonElement> getProducts(@Url String url);
}