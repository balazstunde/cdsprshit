package com.android.merx;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class NumberDisplayElement extends DisplayElement implements Serializable {

    private Integer value;
    private Integer keyFontSize;
    private String keyFontFamily;
    private String keyColor;
    private Integer valueFontSize;
    private String valueFontFamily;
    private String valueColor;

    public NumberDisplayElement(String name, String type, String constraints,Integer value,Integer keyFontSize,String keyFontFamily,Integer layoutId,String keyColor,String valueFontFamily,Integer valueFontSize,String valueColor) {
        super(name, type, constraints,layoutId);
        this.value=value;
        this.keyFontFamily=keyFontFamily;
        this.keyFontSize=keyFontSize;
        this.keyColor=keyColor;
        this.valueColor=valueColor;
        this.valueFontFamily=valueFontFamily;
        this.valueFontSize=valueFontSize;
    }

    @Override
    public List<View> displayElement(Context context) {
        List<View> textViews = new LinkedList<View>();
        TextView nameView = new TextView(context);
        nameView.setTextSize(keyFontSize);
        nameView.setTypeface(Typeface.create(keyFontFamily,Typeface.NORMAL));
        nameView.setText(getName());
        nameView.setTextColor(Color.parseColor(keyColor));
        textViews.add(nameView);

        TextView valueView = new TextView(context);
        valueView.setTextSize(valueFontSize);
        valueView.setTypeface(Typeface.create(valueFontFamily,Typeface.NORMAL));
        valueView.setText(String.valueOf(value));
        valueView.setTextColor(Color.parseColor(valueColor));
        textViews.add(valueView);

        return textViews;
    }

    @Override
    public Integer getValue() {
        return value;
    }
}
