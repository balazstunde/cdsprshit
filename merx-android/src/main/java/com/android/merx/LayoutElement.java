package com.android.merx;

import android.content.Context;
import android.view.ViewGroup;

import java.io.Serializable;

public abstract class LayoutElement implements Serializable {
    private String name;
    private Integer id;
    private Integer parentID;
    private ViewGroup.LayoutParams params;

    public LayoutElement(String name, Integer id, Integer parentID) {
        this.name = name;
        this.id = id;
        this.parentID = parentID;
    }

    abstract ViewGroup displayElement(Context context);
    Integer getId(){return id;};
    Integer getParentID(){return parentID;};
    public ViewGroup.LayoutParams getLayoutParams(){
        return params;
    }
    public void setLayoutParams(ViewGroup.LayoutParams params){
        this.params=params;
    }
}
