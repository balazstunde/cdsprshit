package com.android.merx;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class WebDisplayElement extends DisplayElement implements Serializable {
    private String value;
    private Integer fontSize;
    private String fontFamily;
    private String color;

    public WebDisplayElement(String name, String type, String constraints,String value,Integer fontSize,String fontFamily,Integer layoutId,String color) {
        super(name, type, constraints,layoutId);
        this.value=value;
        this.fontFamily=fontFamily;
        this.fontSize=fontSize;
        this.color=color;
    }

    @Override
    public List<View> displayElement(Context context) {
        List<View> textViews = new LinkedList<View>();
        TextView nameView = new TextView(context);
        nameView.setTextSize(fontSize);
        nameView.setTypeface(Typeface.create(fontFamily,Typeface.NORMAL));
        nameView.setText(getName());
        nameView.setTextColor(Color.parseColor(color));
        textViews.add(nameView);

        TextView valueView = new TextView(context);
        valueView.setTextSize(fontSize);
        valueView.setFontFeatureSettings(fontFamily);
        valueView.setText(Html.fromHtml(value));
        textViews.add(valueView);
        return textViews;
    }

    @Override
    public String getValue() {
        return value;
    }
}
