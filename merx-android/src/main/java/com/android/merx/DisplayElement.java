package com.android.merx;

import android.content.Context;
import android.view.View;

import java.io.Serializable;
import java.util.List;

public abstract class DisplayElement implements Serializable{
    private String name;
    private String type;
    private String constraints;
    private Integer layoutId;

    public DisplayElement(String name, String type, String constraints,Integer layoutId) {
        this.name = name;
        this.type = type;
        this.constraints = constraints;
        this.layoutId=layoutId;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getConstraints() {
        return constraints;
    }

    public abstract List<View> displayElement(Context context);

    public Integer getLayoutId() {
        return layoutId;
    }

    public abstract Object getValue();
}
