package com.android.merx;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "bookmarks_db";

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Bookmark.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTIS "+ Bookmark.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insertBookmark(Bookmark bookmark){
        SQLiteDatabase db = this.getWritableDatabase();
        Log.e("CREATE STATEMENT>>>>>", Bookmark.CREATE_TABLE);
        ContentValues bookmarkValue = new ContentValues();
        bookmarkValue.put(Bookmark.IMAGEURL_COLUMN,bookmark.getImageUrl());
        bookmarkValue.put(Bookmark.COLUMN_VENDOR,bookmark.getVendor());
        bookmarkValue.put(Bookmark.COLUMN_NAME,bookmark.getName());
        bookmarkValue.put(Bookmark.COLUMN_PRODUCTURL,bookmark.getProductUrl());

        db.insert(Bookmark.TABLE_NAME,null,bookmarkValue);
        db.close();
        System.out.println("INSERTED "+bookmarkValue.toString()+"   FROM DATABASEHELPER");
        return true;
    }

    public List<Bookmark> getAllBookmarks(){
        List<Bookmark> bookmarks = new LinkedList<>();

        String selectQuery = "SELECT * FROM "+ Bookmark.TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){
            do{
                Bookmark bookmark = new Bookmark();
                bookmark.setId(cursor.getLong(cursor.getColumnIndex(Bookmark.COLUMN_ID)));
                bookmark.setName(cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_NAME)));
                bookmark.setImageUrl(cursor.getString(cursor.getColumnIndex(Bookmark.IMAGEURL_COLUMN)));
                bookmark.setVendor(cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_VENDOR)));
                bookmark.setProductUrl(cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_PRODUCTURL)));

                bookmarks.add(bookmark);
            } while(cursor.moveToNext());
        }

        db.close();

        return bookmarks;
    }

    public void deleteBookmark(Bookmark bookmark){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Bookmark.TABLE_NAME,Bookmark.COLUMN_ID+" = ?",new String[]{String.valueOf(bookmark.getId())});
        db.close();
    }
}
