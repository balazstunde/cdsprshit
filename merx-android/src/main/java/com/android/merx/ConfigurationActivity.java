package com.android.merx;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

public class ConfigurationActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 100;
    private Button submit;
    private EditText apiUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        submit = (Button) findViewById(R.id.submit);
        apiUrl = (EditText) findViewById(R.id.apiUrl);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = ConfigurationActivity.this.getSharedPreferences(
                        "com.android.merx", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                Log.e("api_url_shared_prefs", prefs.getString("api_url", "Error!"));
                editor.putString("api_url", apiUrl.getText().toString());
                editor.commit();
                Log.e("api_url_shared_prefs", prefs.getString("api_url", "Error!"));
                apiUrl.setText("");
            }
        });

        ImageView backIcon = findViewById(R.id.backIcon);
        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfigurationActivity.super.onBackPressed();
            }
        });

    }



    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        SharedPreferences prefs = ConfigurationActivity.this.getSharedPreferences(
                "com.android.merx", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.emulator:
                if (checked)
                    editor.putString("api_url", "http://10.10.9.26:8080");
                    editor.apply();
                    break;
            case R.id.phone:
                if (checked)
                    editor.putString("api_url", "http://10.10.9.99:8080");
                    editor.commit();
                    break;
        }
    }
}
