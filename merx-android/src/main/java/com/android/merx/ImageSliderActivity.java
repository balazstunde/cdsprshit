package com.android.merx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageSliderActivity extends AppCompatActivity {
    private ViewFlipper viewFlipper;
    private float initialX;
    private int size;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider);

        Intent intent = getIntent();
        final List<String> imagesUrl = (List<String>) intent.getSerializableExtra("images");
        final int index = intent.getIntExtra("index",0);
        viewFlipper = new ViewFlipper(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        viewFlipper.setLayoutParams(layoutParams);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(false);

        viewFlipper.setInAnimation(this, android.R.anim.fade_in);
        viewFlipper.setOutAnimation(this, android.R.anim.fade_out);

        FrameLayout.LayoutParams params =
                new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(90, 90, 90, 90);
        params.gravity = Gravity.CENTER;
        this.size=imagesUrl.size();
        for(int i=0;i<imagesUrl.size();i++){
            Log.e("IMAGEVIEW",imagesUrl.get(i));
            ImageView imageView = new ImageView(this);
            Picasso.with(this)
                    .load(imagesUrl.get(i))
                    .into(imageView);
            viewFlipper.addView(imageView);
            Log.e("MEGHIV","ITT");
        }
        viewFlipper.setDisplayedChild(index);

        this.addContentView(viewFlipper,layoutParams);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchevent) {
        switch (touchevent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = touchevent.getX();
                if (initialX > finalX) {
                    if (viewFlipper.getDisplayedChild() == size-1)
                        break;
                    Log.e("NOW:::",viewFlipper.getDisplayedChild()+"");
                    viewFlipper.showNext();
                } else {
                    if (viewFlipper.getDisplayedChild() == 0)
                        break;
                    viewFlipper.showPrevious();
                }
                break;
        }
        return false;
    }
}
