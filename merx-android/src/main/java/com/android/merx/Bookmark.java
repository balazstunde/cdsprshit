package com.android.merx;

import java.io.Serializable;

public class Bookmark implements Serializable{
    public static final String TABLE_NAME = "Bookmark";
    public static final String COLUMN_NAME = "Name";
    public static final String COLUMN_VENDOR = "Vendor";
    public static final String IMAGEURL_COLUMN = "ImageURL";
    public static final String COLUMN_ID = "Id";
    public static final String COLUMN_PRODUCTURL = "ProductURL";

    private Long id;
    private String name;
    private String vendor;
    private String imageUrl;
    private String productUrl;

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +"(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_NAME + " TEXT,"+ COLUMN_VENDOR + " TEXT," + IMAGEURL_COLUMN + " TEXT," +COLUMN_PRODUCTURL+" TEXT" +")";

    public Bookmark(){

    }

    public Bookmark(Long id, String name, String vendor, String imageUrl) {
        this.id = id;
        this.name = name;
        this.vendor = vendor;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

}
