package com.android.merx;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.JsonElement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplayInfo implements Serializable {
    private List<DisplayElement> elements;
    private List<LayoutElement> layoutElements;
    private Bookmark bookmark;

    public DisplayInfo() {
        this.elements = new LinkedList<DisplayElement>();
        this.layoutElements = new LinkedList<LayoutElement>();
        this.bookmark=new Bookmark();
    }

    public void jsonParser(final Context context, final Intent intent) {
        Log.e("BarCode URL ", intent.getStringExtra("url"));
        final SharedPreferences prefs = context.getSharedPreferences(
                "com.android.merx", Context.MODE_PRIVATE);
        String BASE_URL = prefs.getString("api_url", "http://10.0.2.2:8080");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL + "/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProductAPI productAPI = retrofit.create(ProductAPI.class);
        Log.e("IP",BASE_URL);
        Log.e("URL: ",intent.getStringExtra("url"));
        Call<JsonElement> call = productAPI.getProducts(intent.getStringExtra("url"));
        Log.e("Rest call", "rest call ok");
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                JsonElement products = response.body();

                try {
                    String jsonString = products.toString();
                    JSONObject obj = null;
                    Log.e("JSON STRING", jsonString);
                    try {
                        obj = new JSONObject(jsonString);
                        String type;
                        String name;
                        String value;
                        String constraints;
                        Integer layoutId;
                        JSONArray product = obj.getJSONArray("elements");
                        for (int j = 0; j < product.length(); j++) {
                            JSONObject element = product.getJSONObject(j);
                            type = element.getString("type");
                            name = element.getString("name");
                            value = element.getString("value");
                            layoutId = element.getInt("parentLayoutId");
                            constraints = "blablabla";
                            if (type.equalsIgnoreCase("textField")) {
                                int fontSize = Integer.valueOf(element.getString("keyFontSize"));
                                String fontFamily = element.getString("keyFontFamily");
                                TextDisplayElement textDisplayElement = new TextDisplayElement(name, type, constraints, value, fontSize, fontFamily, layoutId,element.getString("keyColor"),element.getString("valueFontFamily"),element.getInt("valueFontSize"),element.getString("valueColor"));
                                elements.add(textDisplayElement);
                            } else if (type.equalsIgnoreCase("imageField")) {
                                ImageDisplayElement imageDisplayElement = new ImageDisplayElement(name, type, constraints, value, layoutId);
                                elements.add(imageDisplayElement);
                            } else if (type.equalsIgnoreCase("numberfield")) {
                                int fontSize = Integer.valueOf(element.getString("keyFontSize"));
                                String fontFamily = element.getString("keyFontFamily");
                                NumberDisplayElement numberDisplayElement = new NumberDisplayElement(name, type, constraints, Integer.valueOf(value), fontSize, fontFamily, layoutId,element.getString("keyColor"),element.getString("valueFontFamily"),element.getInt("valueFontSize"),element.getString("valueColor"));
                                elements.add(numberDisplayElement);
                            } else if (type.equalsIgnoreCase("floatfield")) {
                                int fontSize = Integer.valueOf(element.getString("keyFontSize"));
                                String fontFamily = element.getString("keyFontFamily");
                                FloatDisplayElement floatDisplayElement = new FloatDisplayElement(name, type, constraints, Float.valueOf(value), fontSize, fontFamily, layoutId,element.getString("keyColor"),element.getString("valueFontFamily"),element.getInt("valueFontSize"),element.getString("valueColor"));
                                elements.add(floatDisplayElement);
                            } else if (type.equalsIgnoreCase("imagelist")) {
                                Integer width = element.getInt("width");
                                Integer height = element.getInt("height");
                                JSONArray values = element.getJSONArray("value");
                                List<String> imageUrls = new LinkedList<>();
                                for (int i = 0; i < values.length(); i++) {
                                    imageUrls.add(prefs.getString("api_url", "http://10.0.2.2:8080") + "/" + values.get(i).toString());
                                }
                                ImageListDisplayElement imageListDisplayElement = new ImageListDisplayElement(name, type, constraints, layoutId, imageUrls, width, height);
                                elements.add(imageListDisplayElement);
                            } else if (type.equalsIgnoreCase("webdisplay")){
                                int fontSize = Integer.valueOf(element.getString("keyFontSize"));
                                String fontFamily = element.getString("keyFontFamily");
                                WebDisplayElement webDisplayElement = new WebDisplayElement(name, type, constraints, value, fontSize, fontFamily, layoutId,element.getString("keyColor"));
                                elements.add(webDisplayElement);
                            }
                        }
                        JSONArray layouts = obj.getJSONArray("layoutElements");
                        String layoutName;
                        Integer id;
                        Integer parentId;
                        Map<Integer, String> parentIds = new HashMap<Integer, String>();
                        for (int i = 0; i < layouts.length(); i++) {
                            JSONObject layout = layouts.getJSONObject(i);
                            layoutName = layout.getString("name");
                            id = layout.getInt("id");
                            parentId = layout.getInt("parentLayoutId");
                            if (layoutName.equalsIgnoreCase("linearlayout")) {
                                String mode = layout.getString("mode");
                                LinearLayoutElement linearLayoutElement = new LinearLayoutElement(layoutName, id, parentId, mode);
                                if (!parentId.equals(0)) {
                                    parentIds.put(id, "linearlayout");
                                    Log.e("ENTERED","ENTERED"+id);
                                    JSONObject params = layout.getJSONObject("parentLayoutParams");
                                    for (Map.Entry j : parentIds.entrySet()) {
                                        System.out.println(">>>>>>>>>>>>>>>>>>>>> "+id+" parentID: "+parentId+" paranetId to be matched: "+j.getKey());
                                        if (j.getKey().equals(parentId)) {
                                            if (j.getValue().toString().equalsIgnoreCase("Gridlayout")) {
                                                System.out.println("LINEAR LAYOUT"+id+" :: "+ params.getString("column")+" :: "+params.getString("row"));
                                                linearLayoutElement.setParentLayoutParams("column", params.getString("column"));
                                                linearLayoutElement.setParentLayoutParams("row", params.getString("row"));

                                            } else if (j.getValue().toString().equalsIgnoreCase("linearlayout")) {
                                             //  linearLayoutElement.setParentLayoutParams();
                                            }
                                        }
                                    }
                                } else {
                                    parentIds.put(id, "linearlayout");
                                }
                                layoutElements.add(linearLayoutElement);
                            } else if (layoutName.equalsIgnoreCase("gridlayout")) {
                                Integer row = layout.getInt("row");
                                Integer column = layout.getInt("column");
                                GridLayoutElement gridLayoutElement = new GridLayoutElement(layoutName, id, parentId, row, column);
                                if (!parentId.equals(0)) {
                                    parentIds.put(id, "gridlayout");
                                    JSONObject params = layout.getJSONObject("parentLayoutParams");
                                    for (Map.Entry j : parentIds.entrySet()) {
                                        if (j.getKey().equals(parentId)) {
                                            if (j.getValue().toString().equalsIgnoreCase("Gridlayout")) {
                                                gridLayoutElement.setParentLayoutParams("column", params.getString("column"));
                                                gridLayoutElement.setParentLayoutParams("row", params.getString("row"));
                                            } else if (j.getValue().toString().equalsIgnoreCase("linearlayout")) {
                                                // to be continued

                                            }
                                        }
                                    }
                                } else {
                                    parentIds.put(id, "gridlayout");
                                }
                                layoutElements.add(gridLayoutElement);
                            }
                        }
                        context.startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("HERE","JSON FORMAT ERROR");
                    }
                } catch (NullPointerException e){
                    Toast.makeText(context, "SORRY THE REQUESTED SERVER IS OFFLINE", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("ERROR", t.getMessage());
            }
        });
    }

    public List<DisplayElement> getElements(Context context) {
        return elements;
    }

    public List<LayoutElement> getLayoutElements(Context context) {
        return layoutElements;
    }

    public Bookmark getBookmark() {return bookmark;}
}
