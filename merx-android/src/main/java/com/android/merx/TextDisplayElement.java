package com.android.merx;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.zxing.common.StringUtils;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static java.lang.String.format;

public class TextDisplayElement extends DisplayElement implements Serializable {
    private String value;
    private Integer keyFontSize;
    private String keyFontFamily;
    private String keyColor;
    private Integer valueFontSize;
    private String valueFontFamily;
    private String valueColor;

    public TextDisplayElement(String name, String type, String constraints,String value,Integer keyFontSize,String keyFontFamily,Integer layoutId,String keyColor,String valueFontFamily,Integer valueFontSize,String valueColor) {
        super(name, type, constraints,layoutId);
        this.value=value;
        this.keyFontFamily=keyFontFamily;
        this.keyFontSize=keyFontSize;
        this.keyColor=keyColor;
        this.valueColor=valueColor;
        this.valueFontFamily=valueFontFamily;
        this.valueFontSize=valueFontSize;
    }

    @Override
    public List<View> displayElement(Context context) {
        List<View> textViews = new LinkedList<View>();
        TextView nameView = new TextView(context);

        nameView.setTextSize(keyFontSize);
        nameView.setText(getName());
        nameView.setTextColor(Color.parseColor(keyColor));
        nameView.setTypeface(Typeface.create(keyFontFamily,Typeface.NORMAL));
        textViews.add(nameView);

        TextView valueView = new TextView(context);
        valueView.setTextSize(valueFontSize);
        valueView.setFontFeatureSettings(valueFontFamily);
        valueView.setText(value);
        valueView.setTextColor(Color.parseColor(valueColor));
        valueView.setTypeface(Typeface.create(valueFontFamily,Typeface.NORMAL));
        valueView.setSingleLine(false);
        valueView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        textViews.add(valueView);
        return textViews;
    }

    @Override
    public String getValue() {
        return value;
    }
}
