package com.android.merx;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);

        ImageView backButton = findViewById(R.id.backIcon);

        final Intent intent = getIntent();
        final List<LayoutElement> layoutElements = (List<LayoutElement>) intent.getSerializableExtra("layouts");
        final ScrollView scrollView = (ScrollView) findViewById(R.id.scroll);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowProductActivity.super.onBackPressed();
            }
        });

        final ImageView toBeBookmarked = findViewById(R.id.toBeBookmarked);


        DatabaseHelper db = new DatabaseHelper(ShowProductActivity.this);
        List<Bookmark>allBooksmarks=db.getAllBookmarks();
        Bookmark bookmark = new Bookmark();
        bookmark = (Bookmark) intent.getSerializableExtra("bookmark");

        for (int i = 0; i < allBooksmarks.size(); i++) {
           if (bookmark.getProductUrl().equals(allBooksmarks.get(i).getProductUrl())) {
                toBeBookmarked.setImageResource(R.drawable.ic_heart_red);
              }
        }




        toBeBookmarked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHelper db = new DatabaseHelper(ShowProductActivity.this);
                List<Bookmark>allBooksmarks=db.getAllBookmarks();
                Bookmark bookmark = new Bookmark();
                bookmark = (Bookmark) intent.getSerializableExtra("bookmark");
                try {
                    for (int i = 0; i < allBooksmarks.size(); i++) {
                        if (bookmark.getProductUrl().equals(allBooksmarks.get(i).getProductUrl())) {
                            Toast.makeText(getApplicationContext(), "THIS PRODUCT IS ALREADY IN YOUR BOOKMARKS LIST", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    toBeBookmarked.setImageResource(R.drawable.ic_heart_red);
                    db.insertBookmark(bookmark);
                    Log.e("CLICKED BOOKMARK", bookmark.getProductUrl());
                }
                catch (NullPointerException e){
                    Toast.makeText(getApplicationContext(), "THIS PRODUCT IS ALREADY IN YOUR BOOKMARKS LIST", Toast.LENGTH_LONG).show();
                }
            }
        });

        Map<Integer,ViewGroup> layouts = new HashMap<>();
        Map<Integer,Integer> layoutId = new HashMap<>();
        Map<Integer,ViewGroup.LayoutParams> layoutParams=new HashMap<>();

        for (LayoutElement layoutElement : layoutElements) {
            layouts.put(layoutElement.getId(),layoutElement.displayElement(ShowProductActivity.this));
            layoutParams.put(layoutElement.getId(),layoutElement.getLayoutParams());
            layoutId.put(layoutElement.getId(),layoutElement.getParentID());

            // Add parent layout
            if (layoutElement.getParentID().equals(0))
                scrollView.addView(layouts.get(layoutElement.getId()));
        }


        // Create layout hierarchy
        for (Map.Entry entry : layouts.entrySet()) {
            Integer id = (Integer) entry.getKey ();
            ViewGroup layout = (ViewGroup) entry.getValue ();
            ViewGroup.LayoutParams params = layoutParams.get (id);

            Integer parentId = layoutId.get (id);
            if (!parentId.equals(0)) {
                ViewGroup parentLayout = (ViewGroup) layouts.get(parentId);
                parentLayout.addView(layout);
            }
        }

        final LinearLayout imagesLayout = new LinearLayout(this);
        imagesLayout.setOrientation(LinearLayout.HORIZONTAL);

        final List<DisplayElement> elements = (List<DisplayElement>) intent.getSerializableExtra("views");

        for (DisplayElement element : elements) {
            List<View> tempListOfViews=element.displayElement(ShowProductActivity.this);

            for (View view : tempListOfViews)
                layouts.get(element.getLayoutId()).addView(view);
        }
    }
}