package com.android.merx;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.util.HashMap;
import java.util.Map;

public class ScrollViewElement extends LayoutElement {
    private Map<String,String> parentLayoutParams;

    public Map<String, String> getParentLayoutParams() {
        return parentLayoutParams;
    }

    public void setParentLayoutParams(String name,String value) {
        parentLayoutParams.put(name,value);
    }

    public ScrollViewElement(String name, Integer id, Integer parentID) {
        super(name, id, parentID);
        parentLayoutParams = new HashMap<>();
    }

    @Override
    ViewGroup displayElement(Context context) {
        ScrollView scrollView = new ScrollView(context);

        return scrollView;
    }
}
