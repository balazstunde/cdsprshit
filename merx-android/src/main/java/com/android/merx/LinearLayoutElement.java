package com.android.merx;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class LinearLayoutElement extends LayoutElement implements Serializable {
    private String mode;
    private Map<String,String> parentLayoutParams;


    public LinearLayoutElement(String name,Integer id,Integer parentID,String mode){
        super(name,id,parentID);
        this.mode=mode;
        parentLayoutParams = new HashMap<>();
    }


    public Map<String, String> getParentLayoutParams() {
        return parentLayoutParams;
    }

    public void setParentLayoutParams(String name,String value) {
        parentLayoutParams.put(name,value);
    }

    @Override
    ViewGroup displayElement(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(10,10,10,10);
        if(mode.equalsIgnoreCase("horizontal")) {
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        }
        else{
            linearLayout.setOrientation(LinearLayout.VERTICAL);
        }
        if(!parentLayoutParams.isEmpty()) {
            if(parentLayoutParams.containsKey("column") && parentLayoutParams.containsKey("row")) {
                GridLayout.Spec row=GridLayout.spec(0);
                GridLayout.Spec column=GridLayout.spec(0);
                Log.e("PARENT-->","GRID");
                for(Map.Entry k:parentLayoutParams.entrySet()) {
                    if(k.getKey().equals("row")){
                        row=GridLayout.spec(Integer.valueOf(k.getValue().toString()));
                    }
                    else{
                        column=GridLayout.spec(Integer.valueOf(k.getValue().toString()));
                    }
                    ViewGroup.LayoutParams params;
                    params = new GridLayout.LayoutParams(row,column);
                    params.height=GridLayout.LayoutParams.WRAP_CONTENT;
                    params.width=GridLayout.LayoutParams.MATCH_PARENT;
                    linearLayout.setLayoutParams(params);
                    Log.e("PARAMS:",params.toString());
                    setLayoutParams(params);
                }
            }
        }
        return linearLayout;
    }
}
