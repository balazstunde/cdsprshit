package com.android.merx;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.GridLayout;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class GridLayoutElement extends LayoutElement implements Serializable{
    private Integer rowNumber;
    private Integer columnNumber;
    private Map<String,String> parentLayoutParams;

    public GridLayoutElement(String name, Integer id, Integer parentID,Integer rowNumber,Integer columnNumber) {
        super(name, id, parentID);
        this.rowNumber=rowNumber;
        this.columnNumber=columnNumber;
        parentLayoutParams=new HashMap<>();
    }

    public Map<String, String> getParentLayoutParams() {
        return parentLayoutParams;
    }

    public void setParentLayoutParams(String name, String value) {
        parentLayoutParams.put(name,value);
    }

    @Override
    ViewGroup displayElement(Context context) {
        GridLayout gridLayout = new GridLayout(context);
        gridLayout.setColumnCount(columnNumber);
        gridLayout.setRowCount(rowNumber);
        if(!parentLayoutParams.isEmpty()) {
            if(parentLayoutParams.containsKey("column") && parentLayoutParams.containsKey("row")) {
                GridLayout.Spec row=GridLayout.spec(0);
                GridLayout.Spec column=GridLayout.spec(0);
                Log.e("PARENT-->","GRID");
                for(Map.Entry k:parentLayoutParams.entrySet()) {
                    if(k.getKey().equals("row")){
                        row=GridLayout.spec(Integer.valueOf(k.getValue().toString()));
                    }
                    else{
                        column=GridLayout.spec(Integer.valueOf(k.getValue().toString()));
                    }
                    ViewGroup.LayoutParams params;
                    params = new GridLayout.LayoutParams(row,column);
                    params.height=GridLayout.LayoutParams.WRAP_CONTENT;
                    params.width=GridLayout.LayoutParams.MATCH_PARENT;
                    gridLayout.setLayoutParams(params);
                    Log.e("PARAMS:",params.toString());
                    setLayoutParams(params);
                }
            }
        }
        return gridLayout;
    }
}
