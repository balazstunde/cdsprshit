package com.android.merx;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class BookmarkActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);


    }
    @Override
    protected void onResume()
    {
        super.onResume();
        final SwipeMenuListView listView = (SwipeMenuListView) findViewById(R.id.bookmarkList);
        final DatabaseHelper databaseHelper = new DatabaseHelper(this);

        final List<Bookmark> allBookmarks=databaseHelper.getAllBookmarks();
        final List<String> bookmarksForList = new LinkedList<>();

        for (int i = 0; i < allBookmarks.size(); i++) {
            System.out.println("ALL BOOKMARKS: "+allBookmarks.get(i).getImageUrl()+" "+allBookmarks.get(i).getName()+" "+allBookmarks.get(i).getVendor()+" "+allBookmarks.get(i).getId()+" "+allBookmarks.get(i).getProductUrl());
            bookmarksForList.add(allBookmarks.get(i).getImageUrl()+" "+allBookmarks.get(i).getName()+" "+allBookmarks.get(i).getVendor());
        }

/*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,bookmarksForList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                final TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTextColor(Color.BLACK);
                return view;
            }
        };
*/
        //listView.setAdapter(adapter);
        final List<String> names = new LinkedList<>();
        final List<String> infos = new LinkedList<>();
        final List<String> images = new LinkedList<>();

        for (int i = 0; i < allBookmarks.size(); i++) {
            names.add(allBookmarks.get(i).getName());
            infos.add(allBookmarks.get(i).getVendor());
            images.add(allBookmarks.get(i).getImageUrl());
        }

        CustomListAdapter customListAdapter = new CustomListAdapter(this,names,infos,images);
        listView.setAdapter(customListAdapter);

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());

                deleteItem.setBackground(new ColorDrawable(Color.rgb(164, 135,
                        135)));
                deleteItem.setWidth(170);
                deleteItem.setIcon(R.drawable.ic_delete_black);
                menu.addMenuItem(deleteItem);

            }
        };

        listView.setMenuCreator(creator);

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        databaseHelper.deleteBookmark(allBookmarks.get(position));
                        Log.e("SWIPED", allBookmarks.get(position).getName()+" "+allBookmarks.get(position).getVendor()+" "+allBookmarks.get(position).getId()+allBookmarks.get(position).getProductUrl()+" "+allBookmarks.get(position).getId()+" DELETED");
                        names.remove(position);
                        infos.remove(position);
                        images.remove(position);
                        allBookmarks.remove(position);
                        listView.invalidateViews();
                        break;
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Intent intent = new Intent(BookmarkActivity.this, ShowProductActivity.class);
                DisplayInfo displayInfo = new DisplayInfo();

                intent.putExtra("views", (Serializable) displayInfo.getElements(BookmarkActivity.this));
                intent.putExtra("layouts", (Serializable)displayInfo.getLayoutElements(BookmarkActivity.this));
                intent.putExtra("url", allBookmarks.get(i).getProductUrl());
                intent.putExtra("bookmark",(Serializable) allBookmarks.get(i));
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+allBookmarks.get(i).getProductUrl());
                displayInfo.jsonParser(BookmarkActivity.this, intent);
            }
        });

        ImageView backIcon = findViewById(R.id.backIcon);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BookmarkActivity.super.onBackPressed();
            }
        });
    }
}
