package com.android.merx;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomListAdapter extends ArrayAdapter {

    private final Activity context;
    private final List<String> imageIDarray;
    private final List<String> nameArray;
    private final List<String> infoArray;

    public CustomListAdapter(Activity context, List<String> nameArrayParam, List<String> infoArrayParam, List<String> imageIDArrayParam){

        super(context,R.layout.bookmarklistview_row , nameArrayParam);
        this.context=context;
        this.imageIDarray = imageIDArrayParam;
        this.nameArray = nameArrayParam;
        this.infoArray = infoArrayParam;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.bookmarklistview_row, null,true);


        TextView nameTextField = (TextView) rowView.findViewById(R.id.textView4);
        TextView infoTextField = (TextView) rowView.findViewById(R.id.textView3);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageViewID);

        nameTextField.setText(nameArray.get(position));
        infoTextField.setText(infoArray.get(position));
        imageView.setImageResource(R.drawable.ic_car_black);

        return rowView;

    };
}
