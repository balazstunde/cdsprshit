import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vendor-menu',
  templateUrl: './vendor-menu.component.html',
  styleUrls: ['./vendor-menu.component.css']
})
export class VendorMenuComponent implements OnInit {

  route: string;
  constructor() { }

  ngOnInit() {
    // hardcoded vendorID (after login we will know the vendorId)
    this.route = '/vendor/2/products/';
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
