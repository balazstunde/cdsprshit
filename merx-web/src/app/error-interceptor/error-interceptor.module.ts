import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})

@Injectable()
export class ErrorInterceptorModule implements HttpInterceptor {

    constructor(private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 403) {
                // auto logout if 403 response returned from api
                localStorage.removeItem('currentUser');
                this.router.navigateByUrl('/home');
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}
