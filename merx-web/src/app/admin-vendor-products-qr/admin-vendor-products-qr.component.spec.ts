import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminVendorProductsQRComponent } from './admin-vendor-products-qr.component';

describe('AdminVendorProductsQRComponent', () => {
  let component: AdminVendorProductsQRComponent;
  let fixture: ComponentFixture<AdminVendorProductsQRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminVendorProductsQRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminVendorProductsQRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
