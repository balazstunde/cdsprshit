import OlMap from 'ol/Map';
import OlVectorSource from 'ol/source/Vector';
import OlVectorLayer from 'ol/layer/Vector';
import OlView from 'ol/View';
import OlFeature from 'ol/Feature';
import OlPoint from 'ol/geom/Point';
import OlXyzSource from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import {Icon, Style} from 'ol/style.js';
import {fromLonLat} from 'ol/proj';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-vendor',
  templateUrl: './map-vendor.component.html',
  styleUrls: ['./map-vendor.component.css']
})
export class MapVendorComponent implements OnInit {

  map: OlMap;
  vectorSource: OlVectorSource;
  vectorLayer: OlVectorLayer;
  vectorSourceOwn: OlVectorSource;
  vectorLayerOwn: OlVectorLayer;
  xyzSource: OlXyzSource;
  tileLayer: OlTileLayer;
  view: OlView;
  markerList: OlFeature[] = [];
  markerListOwn: OlFeature[] = [];

  constructor() { }

  ngOnInit() {
        /*Hardcode example*/
        const listOfCoord = [];
        const list = [23.588356, 46.76768];
        const list2 = [23.6236353, 46.7712101];
        const list3 = [23.589060, 46.770725];
        listOfCoord.push(list);
        listOfCoord.push(list2);
        listOfCoord.push(list3);

        const listOfCoordOwn = [];
        const listown = [23.590803, 46.769355];
        const listown2 = [23.589762, 46.768977];
        const listown3 = [23.590647, 46.768150];
        listOfCoordOwn.push(listown);
        listOfCoordOwn.push(listown2);
        listOfCoordOwn.push(listown3);

        /* Feature and vector */
        for (const key in listOfCoord) {
            if (listOfCoord.hasOwnProperty(key)) {
                const element = listOfCoord[key];
                const markerTemp = new OlFeature({
                    geometry: new OlPoint(fromLonLat(element))
                });
                markerTemp.setStyle(new Style({
                    image: new Icon(({
                    color: '#000000',
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v3.20.1/examples/data/dot.png'
                    }))
                }));
                this.markerList.push(markerTemp);
            }
        }
        for (const key in listOfCoordOwn) {
            if (listOfCoordOwn.hasOwnProperty(key)) {
                const element = listOfCoordOwn[key];
                const markerTempOwn = new OlFeature({
                    geometry: new OlPoint(fromLonLat(element))
                });
                markerTempOwn.setStyle(new Style({
                    image: new Icon(({
                    color: '#ff0000',
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v3.20.1/examples/data/dot.png'
                    }))
                }));
                this.markerListOwn.push(markerTempOwn);
            }
        }

        this.vectorSource = new OlVectorSource({
            features: this.markerList
        });

        this.vectorLayer = new OlVectorLayer({
            source: this.vectorSource
        });

        this.vectorSourceOwn = new OlVectorSource({
            features: this.markerListOwn
        });

        this.vectorLayerOwn = new OlVectorLayer({
            source: this.vectorSourceOwn
        });

        /* XYZ */
        this.xyzSource = new OlXyzSource({
            url: 'http://tile.osm.org/{z}/{x}/{y}.png'
        });

        this.tileLayer = new OlTileLayer({
            source: this.xyzSource
        });

        /* View and map */
        this.view = new OlView({
            center: fromLonLat([23.588356, 46.76768]),
            zoom: 14
        });

        this.map = new OlMap({
            target: 'map',
            /* Add layers */
            layers: [this.tileLayer, this.vectorLayer, this.vectorLayerOwn],
            view: this.view
        });
    }
}
