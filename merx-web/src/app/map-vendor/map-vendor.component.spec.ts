import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapVendorComponent } from './map-vendor.component';

describe('MapVendorComponent', () => {
  let component: MapVendorComponent;
  let fixture: ComponentFixture<MapVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
