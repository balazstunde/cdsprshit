import { Component, OnInit, OnDestroy } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})

export class ListProductsComponent implements OnInit, OnDestroy {
  id: number;
  products = [] ;
  httpstring : string;
  role : string;
  private sub: any;

  constructor(private route: ActivatedRoute, private http: HttpClient) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      //this.role = params['role'];
       // In a real app: dispatch action to load the details here.
    });
    console.log(this.id);
    this.httpstring = "http://localhost:8080/vendors/" + this.id + "/products"
    this.http.get(this.httpstring)
    .subscribe(
       data => {
        const temp = [];
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            const element = data[key];
            console.log(element);
            console.log(element.name);
            console.log(element.id);
            console.log(element.type);
            this.products.push({"name" : element.name, "route": element.id + "/qr/" + element.type});
          }
        }
        //console.log(temp);
        //this.products = temp;
        console.log(this.products);
        }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}