import { CommonModule } from '@angular/common';
import {Injectable, NgModule} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})


@Injectable()
export class InterceptorModule implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      request = request.clone({
        setHeaders: {
          Authorization: currentUser
        }
      });
    }

    return next.handle(request);
  }
}