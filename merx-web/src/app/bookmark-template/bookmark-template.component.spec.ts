import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookmarkTemplateComponent } from './bookmark-template.component';

describe('BookmarkTemplateComponent', () => {
  let component: BookmarkTemplateComponent;
  let fixture: ComponentFixture<BookmarkTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookmarkTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookmarkTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
