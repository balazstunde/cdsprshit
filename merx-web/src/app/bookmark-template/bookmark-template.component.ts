import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-bookmark-template',
  templateUrl: './bookmark-template.component.html',
  styleUrls: ['./bookmark-template.component.css']
})
export class BookmarkTemplateComponent implements OnInit {
    public bookmarkForm: FormGroup;
    public data: string;
    constructor(private _fb: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
      this.bookmarkForm = this._fb.group({
          vendorId: 4,
          name: ['', ] ,
          elements: this._fb.array([
              this.initproducts(),
          ])
      });

  }

    initproducts() {
        return this._fb.group({
            name: [''],
            source: [''],
        });
    }

    addBookmarkTemplateElement() {
        const control = <FormArray>this.bookmarkForm.controls['elements'];
        control.push(this.initproducts());
    }

    removeBookmarkTemplateElement(i: number) {
        const control = <FormArray>this.bookmarkForm.controls['elements'];
        control.removeAt(i);
    }

    onSubmit(f: NgForm) {
        console.log(f.value);
        this.data = f.value;
    }

    sendRequest(f: NgForm){
        this.data = f.value;
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json'
        })
        };
        this.http.post("http://localhost:8080/vendor/4/bookmark-templates/", this.data, httpOptions)
        .subscribe();
      }
    
}
