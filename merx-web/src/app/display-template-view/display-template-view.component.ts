import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Option } from './Option';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TreeviewItem, TreeviewConfig,
  TreeviewEventParser, OrderDownlineTreeviewEventParser, DownlineTreeviewItem} from 'ngx-treeview';

@Component({
  selector: 'app-display-template-view',
  templateUrl: './display-template-view.component.html',
  styleUrls: ['./display-template-view.component.css'],
  providers: [
    { provide: TreeviewEventParser, useClass: OrderDownlineTreeviewEventParser }
]
})
export class DisplayTemplateViewComponent implements OnInit {
  public success;
  public index;
  public myForm: FormGroup;
  public layoutForm: FormGroup;
  public myElementsArray = [];
  public layouts = [];
  public values = [];
  public checkedItem;
  public checkedItemName;
  public layoutForTheRequestObject=[];
  public flagForRequest;
  constructor(private _fb: FormBuilder, private http: HttpClient, private router: Router) { }

  config = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: true,
    decoupleChildFromParent: true,
    maxHeight: 400
  });

  selectedOptions: Option;
  selectedLayoutOptions: Option;
  options = [
   new Option( 1, 'string' ),
   new Option( 2, 'number' ),
   new Option( 3, 'boolean' ),
   new Option( 4, 'array' )
  ];

  types = [
   new Option( 1, 'TextField' ),
   new Option( 2, 'BooleanField' ),
   new Option( 3, 'ImageField' ),
   new Option( 4, 'NumberField'),
   new Option( 5, 'FloatField')
  ];

  layoutElements = [
    new Option( 1, 'GridLayout' ),
    new Option( 2, 'LinearLayout' ),
   ];

  parentLayouts = [
  new Option( 1, 'GridLayout' ),
  new Option( 2, 'LinearLayout' ),
  ];

  layoutTag = '';
  onChangeL(event) {
    this.layoutTag=event.target.value;
  }

  parentLayoutTag = '';
  onChangePL(event) {
    this.parentLayoutTag=event.target.value;
  }

  ngOnInit() {
    this.index = 0;
    this.flagForRequest = 0;
    this.layoutForm = this._fb.group({
      id: [''],
      name:[''],
      row: [''],
      column: [''],
      mode: [''],
      parentLayoutId:[''],
      parentLayoutParams:
        this._fb.group({
          parentLayoutName:[''],
          row:[''],
          column:['']
        })
    });
    this.myForm = this._fb.group({
        name: ['', ] ,
        vendorId: 4,
        elements: this._fb.array([
          this.initElements(),
      ])
    });
    this.selectedOptions = this.options[1];
    this.selectedLayoutOptions = this.layoutElements[1];
  }

  initElements() {
    return this._fb.group({
        parentLayoutId:[''],
        parentLayoutParams: 
          this._fb.group({
            parentLayoutName:[''],
            row:[''],
            column:['']
          }),
        name: [''],
        type: [''],
        keyFontFamily: [''],
        valueFontFamily:[''],
        keyFontSize:[''],
        valueFontSize:[''],
        keyColor:[''],
        valueColor:[''],
        width:[''],
        height:['']
    });
  }

  onSubmit(f: NgForm) {
      if(this.flagForRequest == 0){
        this.myForm.value['elements'][this.myForm.value['elements'].length-2]['parentLayoutId'] = this.checkedItem;
        this.myForm.value['elements'][this.myForm.value['elements'].length-2]['parentLayoutParams']['parentLayoutName'] = this.checkedItemName;
        this.myElementsArray.push(this.myForm.value['elements'][this.myForm.value['elements'].length-2]);
      }
      if(this.flagForRequest == 1){
        this.flagForRequest = 0;
      }
  }

  onSubmitLayout(f: NgForm) {
    this.index = this.index + 1;
    this.layoutForm.value['id'] = this.index;

    var layoutItem = new TreeviewItem({
      text: String(this.layoutForm.value['name']),
      value: this.layoutForm.value['id'],
      checked: false
    });

    var layoutForRequestItem = {};
    if(this.layoutForm.value['name'] == 'GridLayout')  {
      layoutForRequestItem['id'] = this.layoutForm.value['id'];
      layoutForRequestItem['name'] = this.layoutForm.value['name'];
      layoutForRequestItem['row'] = this.layoutForm.value['row'];
      layoutForRequestItem['column'] = this.layoutForm.value['column'];
    }else{
      layoutForRequestItem['id'] = this.layoutForm.value['id'];
      layoutForRequestItem['name'] = this.layoutForm.value['name'];
      layoutForRequestItem['mode'] = this.layoutForm.value['mode'];
    }
    
    if(this.checkedItem == undefined){
      this.layoutForm.value['parentLayoutId'] = 0;
      this.layoutForm.value['parentLayoutParams']['parentLayoutName'] = 'LinearLayout';
      layoutForRequestItem['parentLayoutId'] = 0;
      layoutForRequestItem['parentLayoutParams'] = {};
      layoutForRequestItem['parentLayoutParams']['parentLayoutName'] = 'LinearLayout';
      this.layouts.push(layoutItem);
    }else{
      this.layoutForm.value['parentLayoutId'] = this.checkedItem;
      this.layoutForm.value['parentLayoutParams']['parentLayoutName'] = this.checkedItemName;
      layoutForRequestItem['parentLayoutId'] = this.checkedItem;
      layoutForRequestItem['parentLayoutParams'] = {};
      layoutForRequestItem['parentLayoutParams']['parentLayoutName'] = this.checkedItemName;
      this.recursiveTreeViewIterator(this.layouts, this.checkedItem, layoutItem);
      this.checkedItem = undefined;
    }
    if(this.layoutForm.value['name'] == 'GridLayout'){
      delete this.layoutForm.value['mode'];
    }else{
      delete this.layoutForm.value['row'];
      delete this.layoutForm.value['column'];
    }
    if(this.checkedItemName == 'LinearLayout' || this.layoutForm.value['parentLayoutParams']['parentLayoutName'] == 'LinearLayout'){
      delete this.layoutForm.value['parentLayoutParams']['row'];
      delete this.layoutForm.value['parentLayoutParams']['column'];
    }else{
      layoutForRequestItem['parentLayoutParams']['row'] = this.layoutForm.value['parentLayoutParams']['row'];
      layoutForRequestItem['parentLayoutParams']['column'] = this.layoutForm.value['parentLayoutParams']['column'];
    }

    this.layoutForTheRequestObject.push(layoutForRequestItem);
  }
  
  recursiveTreeViewIterator(treeView, parentLayoutId, newTreeViewItem){
    for(var i=0; i < treeView.length; i++){
      for (var l in treeView[i]){
        if(l == "text" || l == "value" || l == "internalChildren")
        if(l == "value" && treeView[i][l] == parentLayoutId){
          if(treeView[i]['children'] == undefined){
            treeView[i]["children"]=[newTreeViewItem];
          }else{
            treeView[i]["children"].push(newTreeViewItem);
          }
        }
        if (l == "internalChildren"){
          this.recursiveTreeViewIterator(treeView[i][l], parentLayoutId, newTreeViewItem);
        }
      }
    }
  }

  tag = '';
  onChange(event) {
    this.tag=event.target.value;
  }

  addDisplayTemplateElement() {
    const control = <FormArray>this.myForm.controls['elements'];
    control.push(this.initElements());
  }

  clean(obj) {
    //delete empty fields
    for (var propName in obj) { 
      if (obj[propName] === undefined || obj[propName] == "") {
        delete obj[propName];
      }
    }
    //delete empty fields from elements
    for(var i = 0; i < obj['elements'].length; i++){
      for (let key in obj['elements'][i]) {
        if(obj['elements'][i][key] == "" || obj['elements'][i][key] == undefined){
          delete obj['elements'][i][key];
        }
      }
      //delete empty fields from elements -> parentLayoutParams 
      for (let key in obj['elements'][i]['parentLayoutParams']) {
        if(obj['elements'][i]['parentLayoutParams'][key] == "" || obj['elements'][i]['parentLayoutParams'][key] ==  undefined){
          delete obj['elements'][i]['parentLayoutParams'][key];
        }
      }
    }
  }

  sendRequest(f: NgForm){
    this.flagForRequest = 1;
    this.myForm.value['elements'][this.myForm.value['elements'].length-1]['parentLayoutId'] = this.checkedItem;
    this.myForm.value['elements'][this.myForm.value['elements'].length-1]['parentLayoutParams']['parentLayoutName'] = this.checkedItemName;
    
    this.myElementsArray.push(this.myForm.value['elements'][this.myForm.value['elements'].length-1]);
    var data = {};
    data['layoutElements'] = this.layoutForTheRequestObject;
    data['elements'] = this.myElementsArray;
    data['name'] = this.myForm.value['name'];
    data['vendorId'] = this.myForm.value['vendorId'];
    this.clean(data);
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
    })
    };
    this.http.post('http://localhost:8080/vendor/4/display-templates/', data, httpOptions)
    .subscribe(
      data => {
        this.success = "Success!";
        this.asyncTimer().then(() => this.success = '');
      }
    );
  }

  async asyncTimer() {
    await this.sleep(3000);
  }

  sleep(ms = 0) {
      return new Promise(r => setTimeout(r, ms));
  }

  onFilterChange(value: string) {
  }

  onSelectedChange(downlineItems: DownlineTreeviewItem[]) {
    this.recursiveTreeViewIteratorFindTheCheckedValueEvenIfItIsAParent(this.layouts);
  }

  recursiveTreeViewIteratorFindTheCheckedValueEvenIfItIsAParent(treeView){
    for(var i=0; i < treeView.length; i++){
      for (var l in treeView[i]){
        if(l == 'checked' && treeView[i]['checked']==true){
          this.checkedItem = treeView[i]['value'];
          this.checkedItemName = treeView[i]['text'];
          //treeView[i]['checked'] = false;
          //this.asyncTimer().then(() => treeView[i]['checked'] = false);
        }else{
          if (l == "internalChildren"){
            this.recursiveTreeViewIteratorFindTheCheckedValueEvenIfItIsAParent(treeView[i][l]);
          }
        }
      }
    }
  }
}
