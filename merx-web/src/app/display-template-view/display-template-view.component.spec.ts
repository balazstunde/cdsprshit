import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayTemplateViewComponent } from './display-template-view.component';

describe('DisplayTemplateViewComponent', () => {
  let component: DisplayTemplateViewComponent;
  let fixture: ComponentFixture<DisplayTemplateViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayTemplateViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayTemplateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
