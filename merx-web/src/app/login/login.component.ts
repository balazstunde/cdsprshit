import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

    constructor(private router: Router, private http: HttpClient) { }

    loginForm = new FormGroup({
        username: new FormControl(''),
        password: new FormControl(''),
    });

    onSubmit() {
        const url = 'http://localhost:8080/login';
        const options = {
            headers: {'Content-Type': 'application/json'},
            observe: 'response' as 'response'
        };

        const body = {
            username: this.loginForm.value.username,
            password: this.loginForm.value.password
        };

        this.http.post(url, body, options)
        .subscribe(response => {
            localStorage.setItem('currentUser', JSON.stringify(response.headers.get('Authorization')));
            const urlRole = 'http://localhost:8080/users/' + this.loginForm.value.username;
            const headers = {'Authorization' : localStorage.getItem('currentUser'), headers: {'Content-Type': 'application/json'} };
            // Sending get request to get the user's role
            this.http.get(urlRole, headers).subscribe(role => {
                if ( role === 'ROLE_ADMIN' ) {
                    this.router.navigateByUrl('/admin'); } else {
                    if (role === 'ROLE_USER') {
                        this.router.navigateByUrl('/vendor'); }
                }
            });
        });
    }
}
