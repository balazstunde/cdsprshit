import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {ColorPickerModule} from 'ngx-color-picker';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { ListProductsComponent } from './list-products/list-products.component';
import { LoginComponent } from './login/login.component';
import { ListVendorsComponent } from './list-vendors/list-vendors.component';
import { ProductTemplateViewComponent } from './product-template-view/product-template-view.component';
import { DisplayTemplateViewComponent } from './display-template-view/display-template-view.component';
import { RouterModule, Routes} from "@angular/router";
import { ShowQRCodeComponent } from './show-qrcode/show-qrcode.component';
import { BookmarkTemplateComponent } from './bookmark-template/bookmark-template.component';
import { MapComponent } from './map/map.component';
import { LandingPageMenuComponent } from './landing-page-menu/landing-page-menu.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { VendorHomeComponent } from './vendor-home/vendor-home.component';
import { VendorListProductsComponent } from './vendor-list-products/vendor-list-products.component';
import { VendorNewProductComponent } from './vendor-new-product/vendor-new-product.component';
import { AdminListVendorsComponent } from './admin-list-vendors/admin-list-vendors.component';
import { AdminNewVendorComponent } from './admin-new-vendor/admin-new-vendor.component';
import { VendorEditTemplateComponent } from './vendor-edit-template/vendor-edit-template.component';
import { VendorConfigComponent } from './vendor-config/vendor-config.component';
import { AdminVendorProductsComponent } from './admin-vendor-products/admin-vendor-products.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { VendorMenuComponent } from './vendor-menu/vendor-menu.component';
import { AdminVendorProductsQRComponent } from './admin-vendor-products-qr/admin-vendor-products-qr.component';
import { VendorProductsQRComponent } from './vendor-products-qr/vendor-products-qr.component';
import {InterceptorModule} from './interceptor/interceptor.module';
import { NewVendorComponent } from './new-vendor/new-vendor.component';
import { ListProductTemplatesComponent } from './list-product-templates/list-product-templates.component';
import { AdminHelpComponent } from './admin-help/admin-help.component';
import { VendorHelpComponent } from './vendor-help/vendor-help.component';
import {ErrorInterceptorModule} from "./error-interceptor/error-interceptor.module";
import { MapVendorComponent } from './map-vendor/map-vendor.component';
import { TreeviewModule } from 'ngx-treeview';


@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    ListProductsComponent,
	  LoginComponent,
    ListVendorsComponent,	
    ProductTemplateViewComponent,
    DisplayTemplateViewComponent,
    MapComponent,
    LandingPageMenuComponent,
    LandingPageComponent,
    ShowQRCodeComponent,
    BookmarkTemplateComponent,
    AdminHomeComponent,
    VendorHomeComponent,
    AdminListVendorsComponent,
    VendorListProductsComponent,
    VendorNewProductComponent,
    VendorEditTemplateComponent,
    VendorConfigComponent,
    AdminNewVendorComponent,
    AdminVendorProductsComponent,
    AdminMenuComponent,
    VendorMenuComponent,
    AdminVendorProductsQRComponent,
    VendorProductsQRComponent,
    NewVendorComponent,
    ListProductTemplatesComponent,
    AdminHelpComponent,
    VendorHelpComponent,
    MapVendorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ColorPickerModule,
    RouterModule.forRoot([
      {path: 'admin/help' , component: AdminHelpComponent},
      {path:"admin", component: AdminHomeComponent},
      {path:"admin/vendors/:id/products", component: AdminVendorProductsComponent},
      {path:"admin/vendors/:vendorId/products/:productId/qr/:type", component: AdminVendorProductsQRComponent},
      {path:"admin/vendors", component: AdminListVendorsComponent},
      {path:"admin/vendors/new-vendor", component: AdminNewVendorComponent},
      {path: 'vendor/help' , component: VendorHelpComponent},
      {path:"vendor", component: VendorHomeComponent},
      {path:"vendor/products/:id/:role", component: VendorListProductsComponent},
      {path:"vendor/:id/products", component: VendorListProductsComponent},
      {path:"vendor/:vendorId/products/:productId/qr/:type", component: VendorProductsQRComponent},
      {path:"vendor/:id/products/new-product", component: VendorNewProductComponent},
      {path:"vendor/:id/products/edit", component: VendorEditTemplateComponent},
      {path:"vendor/settings", component:VendorConfigComponent},
      {path:"home", component: LandingPageComponent},
      {path: '', redirectTo: '/home', pathMatch: 'full'}
    ]),
    TreeviewModule.forRoot()
  ],
  providers: [{
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorModule,
      multi: true
  },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorModule, multi: true }],
  bootstrap: [AppComponent]
})

export class AppModule { }
