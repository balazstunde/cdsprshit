import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorHelpComponent } from './vendor-help.component';

describe('VendorHelpComponent', () => {
  let component: VendorHelpComponent;
  let fixture: ComponentFixture<VendorHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorHelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
