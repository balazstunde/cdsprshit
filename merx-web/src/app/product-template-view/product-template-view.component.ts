import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Option } from './options';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-product-template-view',
  templateUrl: './product-template-view.component.html',
  styleUrls: ['./product-template-view.component.css']
})
export class ProductTemplateViewComponent implements OnInit {
  public myForm: FormGroup;
  public data: string;
  constructor(private _fb: FormBuilder, private http: HttpClient) { }

  selectedOptions: Option;
  options = [
   new Option( 1, 'string' ),
   new Option( 2, 'number' ),
   new Option( 3, 'boolean' ),
   new Option( 4, 'array' ),
  ];


  ngOnInit() {
    this.myForm = this._fb.group({
        vendorId: 4,
        name: ['', ] ,
        elements: this._fb.array([
            this.initproducts(),
        ])
    });
    this.selectedOptions = this.options[1];
  }

  initproducts() {
    return this._fb.group({
        name: [''],
        type: [''],
        source: [''],
    });
  }

  addLanguage() {
    const control = <FormArray>this.myForm.controls['elements'];
    control.push(this.initproducts());
  }

  removeLanguage(i: number) {
    const control = <FormArray>this.myForm.controls['elements'];
    control.removeAt(i);
  }

  onSubmit(f: NgForm) {
    this.data = f.value;
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
  }

  sendRequest(f: NgForm){
    this.data = f.value;
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
    })
    };
    this.http.post("http://localhost:8080/vendor/4/product-templates/", this.data, httpOptions)
    .subscribe();
  }

}
