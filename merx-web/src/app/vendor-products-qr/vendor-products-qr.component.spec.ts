import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorProductsQRComponent } from './vendor-products-qr.component';

describe('VendorProductsQRComponent', () => {
  let component: VendorProductsQRComponent;
  let fixture: ComponentFixture<VendorProductsQRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorProductsQRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorProductsQRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
