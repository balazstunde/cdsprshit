import OlMap from 'ol/Map';
import OlVectorSource from 'ol/source/Vector';
import OlVectorLayer from 'ol/layer/Vector';
import OlView from 'ol/View';
import OlFeature from 'ol/Feature';
import OlPoint from 'ol/geom/Point';
import OlXyzSource from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import {Icon, Style} from 'ol/style.js';
import {fromLonLat} from 'ol/proj';
import { OnInit, Component } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

    map: OlMap;
    vectorSource: OlVectorSource;
    vectorLayer: OlVectorLayer;
    xyzSource: OlXyzSource;
    tileLayer: OlTileLayer;
    view: OlView;
    markerList: OlFeature[] = [];

    ngOnInit() {

        /*Hardcode example*/
        const listOfCoord = [];
        const list = [23.588356, 46.76768];
        const list2 = [23.6236353, 46.7712101];
        const list3 = [23.589060, 46.770725];
        const list4 = [23.590803, 46.769355];
        const list5 = [23.589762, 46.768977];
        const list6 = [23.590647, 46.768150];

        listOfCoord.push(list);
        listOfCoord.push(list2);
        listOfCoord.push(list3);
        listOfCoord.push(list4);
        listOfCoord.push(list5);
        listOfCoord.push(list6);

        /* Feature and vector */
        for (const key in listOfCoord) {
            if (listOfCoord.hasOwnProperty(key)) {
                const element = listOfCoord[key];
                const markerTemp = new OlFeature({
                    geometry: new OlPoint(fromLonLat(element))
                });
                markerTemp.setStyle(new Style({
                    image: new Icon(({
                    color: '#000000',
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v3.20.1/examples/data/dot.png'
                    }))
                }));
                this.markerList.push(markerTemp);
            }
        }

        this.vectorSource = new OlVectorSource({
            features: this.markerList
        });

        this.vectorLayer = new OlVectorLayer({
            source: this.vectorSource
        });

        /* XYZ */
        this.xyzSource = new OlXyzSource({
            url: 'http://tile.osm.org/{z}/{x}/{y}.png'
        });

        this.tileLayer = new OlTileLayer({
            source: this.xyzSource
        });

        /* View and map */
        this.view = new OlView({
            center: fromLonLat([23.588356, 46.76768]),
            zoom: 14
        });

        this.map = new OlMap({
            target: 'map',
            /* Add layers */
            layers: [this.tileLayer, this.vectorLayer],
            view: this.view
        });
    }
}
