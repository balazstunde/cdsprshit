import { Component } from '@angular/core';
import { ImageService } from './image.service';
import {HttpClient} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-qrcode',
  templateUrl: './show-qrcode.component.html',
  styleUrls: ['./show-qrcode.component.css'],
  providers:	[ImageService]
})
export class ShowQRCodeComponent {
  name = 'Angular 6';
  vendorId: number;
  productId: number;
  private sub: any;
  imgUrl: string;
  type: string; 

  imageToShow: any;
  isImageLoading: boolean;

  constructor(private imageService: ImageService, private route: ActivatedRoute, private http: HttpClient) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.vendorId = +params['vendorId']; // (+) converts string 'id' to a number
      this.productId = +params['productId']; 
      this.type = params['type']; 
      // here the bicycle is hardcoded
      this.imgUrl = 'http://localhost:8080/vendor/' + this.vendorId + "/" + this.type + "/" + this.productId + "/test/qr/480/480";
      // In a real app: dispatch action to load the details here.
   });

      this.isImageLoading = true;
      this.imageService.getImage(this.imgUrl).subscribe(data => {
      this.createImageFromBlob(data);
      this.isImageLoading = false;
      }, error => {
        this.isImageLoading = false;
        console.log(error);
      });

  }

  createImageFromBlob(image: Blob) {
   let reader = new FileReader();
   reader.addEventListener("load", () => {
      this.imageToShow = reader.result;
   }, false);

   if (image) {
      reader.readAsDataURL(image);
   }
  }
}
