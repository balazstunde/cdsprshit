import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowQRCodeComponent } from './show-qrcode.component';

describe('ShowQRCodeComponent', () => {
  let component: ShowQRCodeComponent;
  let fixture: ComponentFixture<ShowQRCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowQRCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowQRCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
