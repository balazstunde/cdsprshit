import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorListProductsComponent } from './vendor-list-products.component';

describe('VendorListProductsComponent', () => {
  let component: VendorListProductsComponent;
  let fixture: ComponentFixture<VendorListProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorListProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorListProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
