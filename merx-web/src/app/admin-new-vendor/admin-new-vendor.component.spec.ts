import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNewVendorComponent } from './admin-new-vendor.component';

describe('AdminNewVendorComponent', () => {
  let component: AdminNewVendorComponent;
  let fixture: ComponentFixture<AdminNewVendorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNewVendorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNewVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
