import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductTemplatesComponent } from './list-product-templates.component';

describe('ListProductTemplatesComponent', () => {
  let component: ListProductTemplatesComponent;
  let fixture: ComponentFixture<ListProductTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProductTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
