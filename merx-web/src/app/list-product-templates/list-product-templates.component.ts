import { Component, OnInit, OnDestroy } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-product-templates',
  templateUrl: './list-product-templates.component.html',
  styleUrls: ['./list-product-templates.component.css']
})
export class ListProductTemplatesComponent implements OnInit {

  id: number;
  products = [] ;
  httpString: string;
  role: string;
  private sub: any;

  constructor(private route: ActivatedRoute, private http: HttpClient) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      this.role = params['role'];
    });

    this.httpString = 'http://localhost:8080/vendor/' + this.id + '/templates';
    this.http.get(this.httpString)
    .subscribe(
       data => {
        const temp = [];
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            const element = data[key];
            if (this.role === 'admin') {
              this.products.push({'name' : element.name});
            } else {
              this.products.push({'name' : element.name});
            }
          }
        }
        }
    );
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  deleteTemplates () {
    confirm('Are you sure you want to delete the selected product type and all of its templates?')
  }
}
