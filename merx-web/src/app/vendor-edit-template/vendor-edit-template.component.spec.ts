import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorEditTemplateComponent } from './vendor-edit-template.component';

describe('VendorEditTemplateComponent', () => {
  let component: VendorEditTemplateComponent;
  let fixture: ComponentFixture<VendorEditTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorEditTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorEditTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
