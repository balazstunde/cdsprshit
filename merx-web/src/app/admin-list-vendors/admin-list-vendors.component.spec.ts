import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminListVendorsComponent } from './admin-list-vendors.component';

describe('AdminListVendorsComponent', () => {
  let component: AdminListVendorsComponent;
  let fixture: ComponentFixture<AdminListVendorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminListVendorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminListVendorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
