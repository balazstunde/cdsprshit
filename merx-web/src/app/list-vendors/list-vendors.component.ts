import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { elementStart } from '@angular/core/src/render3';

@Component({
  selector: 'app-list-vendors',
  templateUrl: './list-vendors.component.html',
  styleUrls: ['./list-vendors.component.css']
})

export class ListVendorsComponent implements OnInit {

  constructor(private http: HttpClient) { }

  vendors = [];
  images = [];
  ngOnInit() {

    this.http.get('http://localhost:8080/vendors')
    .subscribe(
       data => {
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            // tslint:disable-next-line:no-shadowed-variable
            const element = data[key];
            console.log(element.name);
            console.log(element.imageUrl);
            this.vendors.push({"name" : element.name, "route": element.id + "/products/" });
          }
        }
      }
    );
  }

  deleteVendor () {
    confirm("Are you sure you want to delete the selected vendor?")
  };
}
